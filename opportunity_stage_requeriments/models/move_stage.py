from odoo import api, models, _, fields
from odoo.exceptions import UserError,ValidationError


class LeadCrm(models.Model):
    _inherit = 'crm.lead'

    @api.depends('stage_id')
    # @api.onchange('stage_id')
    def get_approve_text_for_rfp(self):
        for rec in self:
            if rec.type_new =='rfp':
                if rec.stage_id.id == 10 and rec.is_approved_by_presales != True:



                    print(rec.is_new_approve)
                    if rec.is_new_approve:
                        print(rec.is_new_approve)
                        if len(str(rec.id).split('_')) == 1:
                            my_id = int(str(rec.id).split('_')[0])
                            print('1',my_id)
                        else:
                            my_id = int(str(rec.id).split('_')[1])
                            print('2',my_id)

                        template_id = rec.env.ref(
                            'opportunity_stage_requeriments.template_mail_approve_presales_after_data_correct').id
                        print('rfp')
                        template = rec.env['mail.template'].browse(template_id)
                        template.sudo().send_mail(my_id, force_send=True)

                    rec.current_approve = 'Presales Approval'
                    rec.is_new_approve = True
                    rec.is_close_expecting_close_date = True



                else:
                    rec.current_approve = ' '
                    rec.is_new_approve = False




    next_stage_id = fields.Many2one(comodel_name="crm.stage",string="Next Stage", related="stage_id.next_stage", store=True)
    current_approve = fields.Char(string="Current Approve",readonly=True,store=True)
    current_approve_for_rfp = fields.Char(string="Test Field",compute=get_approve_text_for_rfp,store=True)
    is_new_approve = fields.Boolean(string="",  )


    # @api.depends('stage_id')
    # def automatic_move_stage(self):
    #     for rec in self :
    #         if rec.is_budget and rec.is_frame and rec.is_project and rec.is_solution:
    #             rec.stage_id==rec.next_stage_id
    #         if rec.is_aware and rec.is_engaged and rec.is_dist and rec.is_deal and rec.is_deal_id:
    #             rec.stage_id==rec.next_stage_id
    #         if rec.is_cust_req and rec.is_engaged and rec.is_dist and rec.is_deal and rec.is_deal_id:
    #             rec.stage_id==rec.next_stage_id

    def action_data_correct_in_crm(self):
        for rec in self:
            if rec.is_budget == True and rec.is_frame == True and rec.is_project == True and rec.is_solution == True:
                rec.is_approved_data_from_sales_admin = True
                rec.stage_id = rec.next_stage_id
                rec.is_close_expecting_close_date = True
                rec.current_approve = 'Presales Approval'
                template_id = self.env.ref(
                    'opportunity_stage_requeriments.template_mail_approve_presales_after_data_correct').id
                template = rec.env['mail.template'].browse(template_id)
                template.sudo().send_mail(rec.id, force_send=True)
            else:
                raise ValidationError('You must check all requirements before approve data')

    def write(self, vals):
        res = super(LeadCrm, self).write(vals)
        # if  vals.get('is_budget') == True and  vals.get('is_frame') == True and vals.get('is_project') == True and vals.get('is_solution')==True and vals.get('is_approved_data_from_sales_admin')==True :
        #     self.stage_id = self.next_stage_id
        if vals.get('is_aware') == True and vals.get('is_engaged') == True and vals.get('is_dist') == True and vals.get('is_deal') == True and vals.get('is_deal_id') == True:
            self.stage_id = self.next_stage_id
        if vals.get('is_cust_req') == True and  vals.get('is_propose') == True and vals.get('is_verify') == True and vals.get('is_sheet')==True and vals.get('attachments_ids') == True and vals.get('cost_calc_count') >= 1:
            self.stage_id = self.next_stage_id
        if vals.get('is_cust_engaged') == True :
            self.stage_id = self.next_stage_id
            template_id = self.env.ref('opportunity_stage_requeriments.lead_project_manager_handover_mail_template').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)
        if vals.get('lost_ids') == True :
            self.stage_id.name="Lost"
        return res

    # @api.depends('attachments_ids')
    # def get_won_stage_requirement(self):
    #     attach_won = self.env['ir.attachment'].search(
    #         [('res_model', '=', 'crm.lead'),
    #          ('res_id', '=', self.id), ('stage_attach_id', '=', 'Negotiation')])
    #     if attach_won:
    #         if self.is_done_won_stage or not self.is_done_won_stage:
    #             self.is_done_won_stage=True
    #
    # def _set_open(self):
    #     pass
    #
    # is_done_won_stage = fields.Boolean(string="Done Won", compute=get_won_stage_requirement ,inverse=_set_open)

    # @api.depends('is_done_won_stage','attachments_ids')
    # @api.onchange('is_done_won_stage','attachments_ids')
    # def move_to_won_stage(self):
    #     for rec in self:
    #         proposition_stage_id =self.env['crm.lead'].search(
    #             [('is_done_won_stage', '=', True),('stage_id', '=', 'Negotiation')])
    #         print(proposition_stage_id)
    #         proposition_stage_id.write({'stage_id': rec.next_stage_id.id})
    #

    # @api.depends('stage_id','next_stage_id')
    # @api.onchange('stage_id','next_stage_id')
    # def on_change_stage_id_for_rfb(self):
    #     for rec in self:
    #         if rec.type_new =='rfp':
    #             if rec.stage_id.id == 10 and rec.is_approved_by_presales != True:
    #                 # rec.current_approve = "Presales Approval"
    #                 rec.update({
    #                     'current_approve':"Presales Approval"
    #                 })
    #             # else:
    #             #     rec.current_approve = " "


    is_close_expecting_close_date = fields.Boolean(string="",  )