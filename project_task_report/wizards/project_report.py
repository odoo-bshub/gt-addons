from odoo import fields, models


class CrmReportDaily(models.TransientModel):
    _name = 'daily.report'

    project_id = fields.Many2one('project.project', string='Project', required=True)
    stage_id = fields.Many2one('project.status', string="Mile Stone", required=False)

    def get_task_project(self):
        lst = []
        tasks = self.env['project.task'].search([('project_id', '=',
                                                  self.project_id.id)])
        for rec in tasks:
            lst.append(
                {
                    'name': rec.name,
                    'user_id': rec.user_id.name,
                    'commit': rec.commit,
                    'stage_id': rec.stage_id.name,
                    'details': rec.details,
                    'task_dependency_id': rec.task_dependency_id.name,
                })
        return lst


class ProjectSummery(models.TransientModel):
    _name = 'project.summery'

    project_id = fields.Many2one('project.project', string='Project', required=True)

    def get_task_project_milestone(self):
        lst = []
        dic_task={}
        tasks = self.env['project.task'].search([('project_id', '=',self.project_id.id)])
        # tasks = self.env['project.task.type'].search([('project_ids', 'in',self.project_id.ids)])
        print(tasks)
        for rec in tasks:
            if rec.stage_id.name in dic_task:
            # if rec.name in dic_task:
                dic_task[rec.stage_id.name].append(rec.id)
                # dic_task[rec.name].append()
                # for line in rec.project_ids.task_ids:
                #     if line.stage_id.name==rec.name:
                #         dic_task[rec.name].append(line.name)
            else:
                # dic_task[rec.name] = []
                # for line in rec.project_ids.task_ids:
                #     if line.stage_id.name==rec.name:
                #         dic_task[rec.name] = [line.name]
                dic_task[rec.stage_id.name] = [rec.id]
        print(dic_task)
        return dic_task


class ProgressReportDaily(models.TransientModel):
    _name = 'daily.progress.report'

    project_id = fields.Many2one('project.project', string='Project', required=True)
    task_id = fields.Many2one('project.task', string='Task', required=True)
    date = fields.Date(string="Date", required=True)

    def get_task_progress(self):
        lst = []
        tasks = self.env['project.task'].search([('project_id', '=',
                                                  self.project_id.id),('task_id','=',self.id)])
        for rec in tasks:
            lst.append(
                {
                    'name': rec.name,
                    'user_id': rec.user_id.name,
                    'commit': rec.commit,
                    'stage_id': rec.stage_id.name,
                    'details': rec.details,
                    'task_dependency_id': rec.task_dependency_id.name,
                })
        return lst