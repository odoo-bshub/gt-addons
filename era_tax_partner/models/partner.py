# -*- coding: utf-8 -*-
import qrcode
import base64
from io import BytesIO
from odoo import models, api, fields, _
import binascii
from deep_translator import GoogleTranslator
from textblob import TextBlob


class AccountMoveLineInherit(models.Model):
    _inherit = 'account.move.line'

    tax_amount = fields.Float(string="Tax Amount", compute="_compute_tax_amount")


    @api.depends('tax_ids', 'price_unit','quantity')
    def _compute_tax_amount(self):
        for line in self:
            if line.tax_ids:
                line.tax_amount = line.price_total - line.price_subtotal
            else:
                line.tax_amount = 0.0


class AccountMoveInherit(models.Model):
    _inherit = "account.move"

    @api.onchange('partner_id')
    def _onchange_partner_warning_vat(self):
        if not self.partner_id:
            return
        partner = self.partner_id
        warning = {}
        if partner.company_type == 'company' and not partner.vat:
            title = ("Warning for %s") % partner.name
            message = _("Please add VAT ID for This Partner '%s' !") % (partner.name)
            warning = {
                'title': title,
                'message': message,
            }
        if warning:
            res = {'warning': warning}
            return res

    def _string_to_hex(self, value):
        if value:
            string = str(value)
            string_bytes = string.encode("UTF-8")
            encoded_hex_value = binascii.hexlify(string_bytes)
            hex_value = encoded_hex_value.decode("UTF-8")
            # print("This : "+value +"is Hex: "+ hex_value)
            return hex_value

    def _get_hex(self, tag, length, value):
        if tag and length and value:
            # str(hex(length))
            hex_string = self._string_to_hex(value)
            length = int(len(hex_string)/2)
            # print("LEN", length, " ", "LEN Hex", hex(length))
            conversion_table = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
            hexadecimal = ''
            while (length > 0):
                remainder = length % 16
                hexadecimal = conversion_table[remainder] + hexadecimal
                length = length // 16
            # print(hexadecimal)
            if len(hexadecimal) == 1:
                hexadecimal = "0" + hexadecimal
            return tag + hexadecimal + hex_string

    def get_qr_code_data(self):
        if self.move_type in ('out_invoice', 'out_refund'):
            sellername = str(self.company_id.name)
            seller_vat_no = self.company_id.vat or ''
            if self.partner_id.company_type == 'company':
                customer_name = self.partner_id.name
                customer_vat = self.partner_id.vat
        else:
            sellername = str(self.partner_id.name)
            seller_vat_no = self.partner_id.vat

        seller_hex = self._get_hex("01", "0c", sellername)
        vat_hex = self._get_hex("02", "0f", seller_vat_no)
        # time_stamp = str(self.create_date)
        time_stamp = str(self.invoice_date)
        # print(self.create_date)
        date_hex = self._get_hex("03", "14", time_stamp)
        total_with_vat_hex = self._get_hex("04", "0a", str(round(self.new_custom_total_plus_taxes / self.currency_id.rate, 2)))

        total_vat_hex = self._get_hex("05", "09", str(round(self.new_taxes / self.currency_id.rate, 2)))

        qr_hex = seller_hex + vat_hex + date_hex + total_with_vat_hex + total_vat_hex
        encoded_base64_bytes = base64.b64encode(bytes.fromhex(qr_hex)).decode()

        return encoded_base64_bytes

    qr_code = fields.Binary(string="QR Code", attachment=True, store=True)

    @api.onchange('invoice_line_ids.product_id')
    def generate_qr_code(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(self.get_qr_code_data())
        qr.make(fit=True)
        img = qr.make_image()
        temp = BytesIO()
        img.save(temp, format="PNG")
        qr_image = base64.b64encode(temp.getvalue())
        self.qr_code = qr_image

    @api.depends('partner_id','percentage_custom', 'amount_untaxed', 'amount_by_group','invoice_line_ids','tax_line_ids')
    @api.onchange('partner_id','percentage_custom', 'amount_untaxed', 'amount_by_group','invoice_line_ids','tax_line_ids')
    def get_new_custom_total(self):
        for rec in self:
            if rec.percentage_custom == 0.0:
                if rec.tax_line_ids:
                    #Get taxes from global tax field
                    lst_of_taxes = sum([x.amount/100 for x in rec.tax_line_ids])

                    sum_of_price_subtotal = 0.0
                    for line in rec.invoice_line_ids:
                        sum_of_price_subtotal += line.price_subtotal

                    rec.new_custom_total = sum_of_price_subtotal
                    rec.new_taxes = round(rec.new_custom_total * lst_of_taxes, 2)
                    rec.new_custom_total_plus_taxes = round(rec.new_taxes + rec.new_custom_total, 2)
                else:
                    if rec.invoice_line_ids.mapped('tax_ids'):
                        rec.new_custom_total = rec.amount_untaxed
                        tax_in_lines = 0.0
                        for x in rec.amount_by_group:
                            tax_in_lines = x[1]
                        rec.new_taxes = tax_in_lines
                        rec.new_custom_total_plus_taxes = rec.amount_total

                    else:
                        rec.new_custom_total = rec.amount_untaxed
                        rec.new_taxes = 0.0
                        rec.new_custom_total_plus_taxes = rec.amount_total


            elif rec.percentage_custom > 0.0:

                sum_of_price_subtotal = 0.0
                for line in rec.invoice_line_ids:
                    sum_of_price_subtotal += line.price_subtotal

                if rec.tax_line_ids:
                    # Get taxes from global tax field
                    lst_of_taxes = sum([x.amount / 100 for x in rec.tax_line_ids])

                    rec.new_custom_total = round((sum_of_price_subtotal * rec.percentage_custom) / 100,2)
                    rec.new_taxes = round(rec.new_custom_total * lst_of_taxes, 2)
                    rec.new_custom_total_plus_taxes = round(rec.new_taxes + rec.new_custom_total, 2)

                else:
                    rec.new_custom_total = round((sum_of_price_subtotal * rec.percentage_custom) / 100,2)
                    rec.new_taxes = 0.0
                    rec.new_custom_total_plus_taxes = round(rec.new_taxes + rec.new_custom_total, 2)
            else:
                rec.new_custom_total = 0.0
                rec.new_custom_total_plus_taxes = 0.0
                rec.new_taxes = 0.0

    def _compute_amount_in_word(self):

        for rec in self:
            rec.num_word = str(rec.currency_id.amount_to_text(rec.new_custom_total_plus_taxes)) + ' only'


    @api.onchange('num_word','partner_id')
    @api.depends('num_word','partner_id')
    def amount_to_words_translated(self):
        for rec in self:
            if rec.num_word:
                # self.text_amount_translated = TextBlob(self.num_word).translate(to='ar')
                # print(self.text_amount_translated)
                rec.text_amount_translated = GoogleTranslator(source='auto', target='ar').translate(rec.num_word)
            else:
                rec.text_amount_translated = " "

    text_amount_translated = fields.Text(string="إجمالي المبلغ", required=False,compute="amount_to_words_translated")

    num_word = fields.Char(string="Amount In Words:", compute='_compute_amount_in_word')

    percentage_custom = fields.Float(string="Percentage", )
    new_custom_total = fields.Monetary(string="Custom Total", compute=get_new_custom_total)
    new_taxes = fields.Monetary(string="Custom Taxes", compute=get_new_custom_total)
    new_custom_total_plus_taxes = fields.Monetary(string="Custom Total after taxes", compute=get_new_custom_total)
    percent_id = fields.Many2one(comodel_name="pecent.value", string="Percentage Value",)

class New_Custom_Module_for_Percentage(models.Model):
    _name = 'pecent.value'
    _rec_name = 'name'
    name = fields.Char()



class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    @api.onchange('partner_id')
    def _onchange_partner_warning_vat(self):
        if not self.partner_id:
            return
        partner = self.partner_id
        warning = {}
        if partner.company_type == 'company' and not partner.vat:
            title = ("Warning for %s") % partner.name
            message = _("Please add VAT ID for This Partner '%s' !") % (partner.name)
            warning = {
                'title': title,
                'message': message,
            }
        if warning:
            res = {'warning': warning}
            return res


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    @api.onchange('partner_id')
    def _onchange_partner_warning_vat(self):
        if not self.partner_id:
            return
        partner = self.partner_id
        warning = {}
        if partner.company_type == 'company' and not partner.vat:
            title = ("Warning for %s") % partner.name
            message = _("Please add VAT ID for This Partner '%s' !") % (partner.name)
            warning = {
                'title': title,
                'message': message,
            }
        if warning:
            res = {'warning': warning}
            return res
