from odoo import api, fields, models
from datetime import date, datetime
from odoo.exceptions import ValidationError


class Project_Inheritance(models.Model):
    _inherit = 'project.project'

    def write(self, values):
        # if self.mile_stone_ids:
        if 'mile_stone_ids' in values:
            for li in values['mile_stone_ids']:
                if li[2]:
                    if 'due_date' in li[2] and li[2]['due_date']:
                        if datetime.strptime(li[2]['due_date'], '%Y-%m-%d').date() < self.custom_start_date or \
                                datetime.strptime(li[2]['due_date'], '%Y-%m-%d').date() > self.custom_end_date:
                            raise ValidationError(
                                'Due Date in Milestones tap must be in range of start date and end date of the project ')
        return super(Project_Inheritance, self).write(values)

    @api.model
    def create(self, values):
        res = super(Project_Inheritance, self).create(values)
        states = self.env['project.status'].search([])
        for state in states:
            res.mile_stone_ids.create({
                'project_id': res.id,
                'stage_id': state.id,
            })
        return res

    def get_remaining_days(self):
        for rec in self:
            if rec.custom_end_date:
                today = date.today()
                differ = rec.custom_end_date - today

            return differ.days

    def get_fisical_date_in_pm_tool_report(self):
        for rec in self:
            lst_date = []
            if rec.custom_start_date.strftime("%y") == rec.custom_end_date.strftime("%y"):
                lst_date.append(rec.custom_start_date.strftime("%Y"))
            else:
                lst_date.append(rec.custom_start_date.strftime("%y"))
                lst_date.append(rec.custom_end_date.strftime("%y"))
            return lst_date

    def get_sale_total_by_analytic_account(self):
        for rec in self:
            sale_total = self.env['sale.order'].search([('analytic_account_id', '=', self.analytic_account_id.id)],
                                                       limit=1).amount_total
            return sale_total

    def get_cost_total_by_project_id_by_summary(self):
        for rec in self:
            sum_lst = []
            dolar_rate = self.env['res.currency'].search([('name', '=', 'USD')]).rate
            summary_table = self.env['summary.summary'].search([('project_id', '=', self.id)]).summary_line_ids
            for summary in summary_table:
                sum_lst.append(summary.cost_dollar)

            total_sum = sum(sum_lst)
            by_rate = total_sum * dolar_rate

            return round(by_rate, 2)

    def get_code_of_analytic_account(self):
        for rec in self:
            analytic_account_code = self.env['account.analytic.account'].search([('name', '=', self.name)]).code
            return analytic_account_code

    def get_ho_task_creation_date(self):
        for rec in self:
            if rec.opportunity_id:
                taskks = self.env['project.task'].search([('opportunity_id', '=', self.opportunity_id.id)])
                for task in taskks:
                    if task.name.startswith("HandOver"):
                        return task.create_date.date()

    def get_purchase_lines_from_journal_item_by_analytic_account(self):
        for rec in self:
            dic_purchase_lines = {}
            all_purchase_lines = self.env['purchase.order.line'].search(
                [('account_analytic_id', '=', self.analytic_account_id.id)])
            for purchase in all_purchase_lines:

                lst_taxes_in_purchase = [p.amount / 100 for p in purchase.taxes_id]
                lst_of_total = []
                total_multiply = purchase.price_subtotal
                for tax in lst_taxes_in_purchase:
                    mul = total_multiply * tax
                    lst_of_total.append(mul)

                dic_purchase_lines[purchase.order_id.id] = {
                    'supplier_purchase': purchase.order_id.partner_id.name,
                    'vendor_purchase': purchase.product_id.brand_id.name,
                    'category_purchase': purchase.product_id.categ_id.name,
                    'po_id_purchase': purchase.order_id.name,
                    'value_sar_purchase': purchase.price_subtotal + sum(lst_of_total),
                    'invoice_value_move': False,
                    'remaining_supplier': False,
                    'state_in_vendor_bill': False,
                }

                if purchase.order_id.state == 'draft':
                    dic_purchase_lines[purchase.order_id.id]['status_purchase'] = 'Planned'

                elif purchase.order_id.state in ['sent', 'to approve']:
                    dic_purchase_lines[purchase.order_id.id]['status_purchase'] = 'In Progress'

                elif purchase.order_id.state == 'purchase':
                    if purchase.order_id.picking_count > 0:
                        stock_picking_lines = self.env['stock.picking'].search(
                            [('origin', '=', purchase.order_id.name)]).move_line_ids_without_package
                        for line in stock_picking_lines:
                            if line.product_id == purchase.product_id:
                                if line.state == 'done':
                                    dic_purchase_lines[purchase.order_id.id]['status_purchase'] = 'Delivered'
                                else:
                                    dic_purchase_lines[purchase.order_id.id]['status_purchase'] = 'Issued'
                    else:
                        dic_purchase_lines[purchase.order_id.id]['status_purchase'] = 'Issued'

            all_move_lines = self.env['account.move.line'].search(
                [('analytic_account_id', '=', self.analytic_account_id.id), ('journal_id.type', '=', 'purchase')])
            for move in all_move_lines:

                # get total multiply of taxes with amount
                lst_taxes = [x.amount / 100 for x in move.tax_ids]
                lst_taxes_in_account_move_line = []
                total = move.amount_currency
                for x in lst_taxes:
                    multiply = total * x
                    lst_taxes_in_account_move_line.append(multiply)

                if move.move_id.custom_purchase_id.id in dic_purchase_lines:

                    dic_purchase_lines[move.move_id.custom_purchase_id.id][
                        'invoice_value_move'] = move.amount_currency + sum(lst_taxes_in_account_move_line)
                    dic_purchase_lines[move.move_id.custom_purchase_id.id]['remaining_supplier'] = \
                        dic_purchase_lines[move.move_id.custom_purchase_id.id]['value_sar_purchase'] - \
                        dic_purchase_lines[move.move_id.custom_purchase_id.id]['invoice_value_move']
                    if move.move_id.state == 'draft':
                        dic_purchase_lines[move.move_id.custom_purchase_id.id]['state_in_vendor_bill'] = 'Under Process'
                    elif move.move_id.state == 'posted':
                        if move.move_id.amount_residual != move.move_id.amount_total:
                            dic_purchase_lines[move.move_id.custom_purchase_id.id]['state_in_vendor_bill'] = 'Paid'
                        else:
                            dic_purchase_lines[move.move_id.custom_purchase_id.id]['state_in_vendor_bill'] = 'Hold'

            lst_amount = []
            expenses = self.env['hr.expense'].search([('analytic_account_id', '=', self.analytic_account_id.id)])
            # get total of ad_hoc
            for expens in expenses:
                lst_amount.append(expens.total_amount)
            ad_hoc_total = sum(lst_amount)

            # get actual cost and actual paid
            lst_actual_cost = []
            lst_actual_paid = []
            for item in dic_purchase_lines:
                lst_actual_cost.append(dic_purchase_lines[item]['value_sar_purchase'])
                lst_actual_paid.append(dic_purchase_lines[item]['invoice_value_move'])

            actual_cost = sum(lst_actual_cost) + ad_hoc_total
            actual_paid = sum(lst_actual_paid) + ad_hoc_total

            return dic_purchase_lines, ad_hoc_total, actual_cost, actual_paid

    # @api.depends('name')
    # def get_collection_o2m_field(self):
    #     for rec in self:
    #         rec.collection_ids = False
    #         lst = []
    #         invoices_lines = self.env['account.move.line'].search(
    #             [('analytic_account_id', '=', rec.analytic_account_id.id)])
    #         for invoice_line in invoices_lines:
    #             lst.append(invoice_line.move_id)
    #         not_duplicated_invoices = set(lst)
    #
    #         for invoice in not_duplicated_invoices:
    #             if rec.sale_order_amount or rec.sale_order_amount != 0.0:
    #                 if invoice.state != 'posted':
    #                     rec.collection_ids = [(0, 0, {
    #                         'project_id': rec.id,
    #                         'invoice': invoice.id,
    #                         'invoice_value': invoice.amount_total,
    #                         'percentage': invoice.amount_total / rec.sale_order_amount or 0.0,
    #                         'date_state': 'plan',
    #                         'bill_date': invoice.invoice_date,
    #                         'collection_state': 'plan',
    #                     })]
    #                 else:
    #                     rec.collection_ids = [(0, 0, {
    #                         'project_id': rec.id,
    #                         'invoice': invoice.id,
    #                         'invoice_value': invoice.amount_total,
    #                         'percentage': invoice.amount_total / rec.sale_order_amount or 0.0,
    #                         'date_state': 'issue',
    #                         'bill_date': invoice.invoice_date,
    #                         'collection_state': 'issue',
    #                     })]
    #             if not rec.sale_order_amount or rec.sale_order_amount == 0.0:
    #                 if invoice.state != 'posted':
    #                     rec.collection_ids = [(0, 0, {
    #                         'project_id': rec.id,
    #                         'invoice': invoice.id,
    #                         'invoice_value': invoice.amount_total,
    #                         'percentage': 0.0,
    #                         'date_state': 'plan',
    #                         'bill_date': invoice.invoice_date,
    #                         'collection_state': 'plan',
    #                     })]
    #                 else:
    #                     rec.collection_ids = [(0, 0, {
    #                         'project_id': rec.id,
    #                         'invoice': invoice.id,
    #                         'invoice_value': invoice.amount_total,
    #                         'percentage': 0.0,
    #                         'date_state': 'issue',
    #                         'bill_date': invoice.invoice_date,
    #                         'collection_state': 'issue',
    #                     })]

    custom_start_date = fields.Date(string="Custom Start Date", required=False)
    custom_end_date = fields.Date(string="End Date", required=False)
    re_duration = fields.Char(string="Re Duration", )
    project_type_id = fields.Many2one(comodel_name="project.type", string="Project Type", )
    sale_order_amount = fields.Float(string="Sale Order Amount", readonly=True)
    collection_ids = fields.One2many(comodel_name="project.collection", inverse_name="project_id", store=True,)
    mile_stone_ids = fields.One2many(comodel_name="project.milestone", inverse_name="project_id", string="Milestones", )
    risk_ids = fields.One2many(comodel_name="project.risk", inverse_name="project_id", string="Risks", )


class Analytic_Account_INHERIT(models.Model):
    _inherit = 'account.analytic.account'

    code = fields.Char(string='Reference', index=True, tracking=True,readonly=True)


class Project_Type(models.Model):
    _name = 'project.type'
    _rec_name = 'name'

    name = fields.Char(string="Type", )


class Collection_tab_in_project(models.Model):
    _name = 'project.collection'

    project_id = fields.Many2one(comodel_name="project.project", readonly=True)

    invoice = fields.Many2one(comodel_name="account.move", string="Invoice ID", ondelete='cascade', readonly=False)
    invoice_value = fields.Float(string="Invoice Value", )
    percentage = fields.Float(string="Percentage", )
    date_state = fields.Selection(string="Date(Issue/Plan)", selection=[('plan', 'Planned'), ('issue', 'Issued'), ],
                                  default='plan', readonly=False)
    bill_date = fields.Date(string="Date", readonly=False)
    collection_state = fields.Selection(string="Collection Status",
                                        selection=[('plan', 'Planned'), ('issue', 'Issued'), ('collect', 'Collected')],
                                        default='plan', readonly=False)
    dependency = fields.Char(string=" Last Remark/Dependencies ", )


class MileStones_tab(models.Model):
    _name = 'project.milestone'

    project_id = fields.Many2one(comodel_name="project.project", string="project ID", )

    stage_id = fields.Many2one(comodel_name="project.status", string="Major Milestones", )
    due_date = fields.Date(string="Due Date", )
    per_of_completion = fields.Float(string="% of Completion", )
    comment = fields.Text(string="Comments", )


class Risk(models.Model):
    _name = 'project.risk'

    project_id = fields.Many2one(comodel_name="project.project", string="Project ID", )

    issue = fields.Text(string="Issue/Description", )
    open_date = fields.Date(string="Open Date", )
    agreed_action = fields.Text(string="Agreed Action", )
    state = fields.Selection(string="Status",
                             selection=[('Open', 'Open'), ('Closed', 'Closed'), ('Under Process', 'Under Process')],
                             default='Open', )
