from odoo import api, fields, models

class Product_Product_Inherit(models.Model):
    _inherit = 'product.product'

    brand_id = fields.Many2one(comodel_name="brand.config", string="Brand",related='product_tmpl_id.brand_id',readonly=False,required=True)

class Product_Template_Inherit(models.Model):
    _inherit = 'product.template'

    brand_id = fields.Many2one(comodel_name="brand.config", string="Brand",required=True,)
