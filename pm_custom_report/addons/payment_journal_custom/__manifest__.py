# -*- encoding: utf-8 -*-
{
	"name": "Payment Journal BsHub",
	"version": "14.0",
	"author": "Ahmed Hegazy , BsHub.",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'account',
		'account_accountant',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to add new payment Journal for account. 
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/payment.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
