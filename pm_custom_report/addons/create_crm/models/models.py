from odoo import api, fields, models, _
from odoo.exceptions import UserError

class CreateCrm(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def default_get(self, fields_list):
        if self.env.user.has_group('create_crm.group_create_crm'):
            res = super(CreateCrm, self).default_get(fields_list)
            return res
        else:
            raise UserError(_("Sorry, you don't have privileges to create "
                              "Opportunity."))

    @api.model
    def create(self, vals):
        if self.env.user.has_group('create_crm.group_create_crm'):
            res = super(CreateCrm, self).create(vals)
            return res
        else:
            raise UserError(_("Sorry, you don't have privileges to create Opportunity."))

# class PurchaseOrder(models.Model):
#     _inherit = 'purchase.order'
#
#     @api.model
#     def default_get(self, fields_list):
#         if self.env.user.has_group('create_project.group_create_rfq'):
#             res = super(PurchaseOrder, self).default_get(fields_list)
#             return res
#         else:
#             raise UserError(_("Sorry, you don't have privileges to create RFQ."))
#
#     @api.model
#     def create(self, vals):
#         if self.env.user.has_group('create_project.group_create_rfq'):
#             res = super(PurchaseOrder, self).create(vals)
#             return res
#         else:
#             raise UserError(_("Sorry, you don't have privileges to create RFQ."))
