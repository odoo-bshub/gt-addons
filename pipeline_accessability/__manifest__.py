# -*- encoding: utf-8 -*-
{
	"name": "Pipeline accessibility ",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm',
		# 'opportunity_stage_requeriments',
	],
	"category": "",
	"complexity": "",
	"description": """
	    Team members shouldn’t see all pipe line and each one should have access only to his pipe line.
	""",
	"data": [
		'security/ir.model.access.csv',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
