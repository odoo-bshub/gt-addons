"""Integrated Tests for object crm.lead"""

from datetime import datetime

from dateutil.relativedelta import relativedelta

from odoo import fields
from odoo.tests.common import TransactionCase


class TestCrmLead(TransactionCase):
    """Integrated Tests"""

    def test_missed_opportunity(self):
        """test Scenario: Missed opportunity"""
        stage2 = self.env.ref('crm.stage_lead2')
        stage2.missed_mail_template_id = self.env.ref(
            'missed_opportunity.lead_missed_mail_template').id
        stage2.missed_delay = 0.002
        lead_id = self.env['crm.lead'].create({
            'name': "Sample Lead #6",
            'type': "opportunity",
            'user_id': self.env.user.id,
            'stage_id': self.env.ref('crm.stage_lead2').id,
        })
        last_message_date = lead_id.message_ids.filtered(
            lambda message: not message.mail_activity_type_id)
        if last_message_date:
            lead_id.last_message_date = last_message_date[0].date
        self.assertEqual(lead_id.last_message_date, last_message_date[0].date)
        lead_id.last_message_date = datetime.utcnow() - relativedelta(days=10)
        self.env['crm.lead'].check_missed_lead()
        self.assertTrue(lead_id.missed_email_sent, "lead must be as sent.")
        activity = lead_id.activity_schedule(
            activity_type_id=self.env.ref('mail.mail_activity_data_todo').id,
            user_id=self.env.user.id)
        activity.action_feedback(feedback='feedback')
        self.assertEqual(lead_id.last_activity_done_date,
                         fields.Datetime.now().replace(second=0))
