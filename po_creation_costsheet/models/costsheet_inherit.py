from odoo import api, fields, models


class Cost_Sheet_Inherit_po_creation(models.Model):
    _inherit = 'cost.calc'


    def create_po_foreach_brand(self):
        for rec in self:
            dic_summ_brand = {}
            for summ_brand in rec.summary_line_ids:
                dic_summ_brand[summ_brand.brand_id.display_name] = []

            for brand in dic_summ_brand:
                for cost_line in rec.cost_ids:
                    if brand == cost_line.brand_id.name:
                        dic_summ_brand[brand].append((0, 0,{
                            'brand_id':rec.env['brand.config'].sudo().search([('name','=',brand)]).id,
                            'product_id':cost_line.product_id.id,
                            'name':cost_line.description,
                            'product_qty':cost_line.qty,
                            'cc_qty':cost_line.qty,
                            'price_unit':cost_line.unit_sar,
                        }))

            for brand_in_dic in dic_summ_brand:
                new_po = rec.env['purchase.order'].sudo().create({
                    'partner_id':rec.partner_id.id,
                    'order_line':dic_summ_brand[brand_in_dic],
                    'opportunity_id':rec.opportunity_id.id,
                    'analytic_account_id':rec.opportunity_id.project_created_from_crm_id.analytic_account_id.id,
                })

    is_appear_button_po_create_in_costsheet = fields.Boolean(string="",  )




