
from odoo import api, fields, models, _


class Solution(models.Model):
    _name = 'solution.solution'
    _rec_name = 'name'

    name = fields.Char(string="Solution Name", required=False, )

