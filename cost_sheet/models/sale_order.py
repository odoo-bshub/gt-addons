from odoo import api, fields, models

class Sale_Order_Inherit(models.Model):
    _inherit = 'sale.order'
    cost_calculation_id = fields.Many2one(comodel_name="cost.calc", )


class Sale_Order_Line_Inherit(models.Model):
    _inherit = 'sale.order.line'

    brand_id = fields.Many2one(comodel_name="brand.config", string="Brand",)
    part_num = fields.Char(string="Part Number",)
    is_created_from_cost_calculation = fields.Boolean(string="Is created from cost calcu",)