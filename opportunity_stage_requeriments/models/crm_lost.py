from odoo import api, fields, models, _
from odoo.exceptions import UserError


class LostLeadButton(models.TransientModel):
    _inherit = 'crm.lead.lost'

    # def action_lost_reason_apply(self):
    #     crm_sale = self.env['crm.lead'].browse(
    #         self.env.context.get('active_ids'))
    #     if not crm_sale.lost_ids :
    #         raise UserError(_(
    #             'You must attach Lost report '))
    #     else:
    #         leads = self.env['crm.lead'].browse(
    #             self.env.context.get('active_ids'))
    #         return leads.action_set_lost(lost_reason=self.lost_reason_id.id)


