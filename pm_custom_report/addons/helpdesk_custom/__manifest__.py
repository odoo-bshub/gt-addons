# -*- encoding: utf-8 -*-
{
	"name": "helpdesk  Updates",
	"version": "14.0",
	"author": "samah kandil.",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'helpdesk',
		# 'sale',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module for custom. 
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'security/groups_custom.xml',
		'views/email_template.xml',
		'views/helpdesk.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
