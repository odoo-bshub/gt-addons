from odoo import api, fields, models

class Project_Inherit_for_enhancement(models.Model):
    _inherit = 'project.project'

    @api.model
    def create(self, values):
        analytic_account = self.env['account.analytic.account'].create({
            'name': values.get('name'),
            # 'company_id': values.get('company_id'),
            'partner_id': values.get('partner_id'),
            'active': True,
        })

        values['analytic_account_id'] = analytic_account.id
        res = super(Project_Inherit_for_enhancement, self).create(values)
        if res.partner_id:
            analytic_all = self.env['account.analytic.account'].search([('company_id','=',self.env.company.id)])
            lst = []
            for analytic in analytic_all:
                if analytic.code and analytic.code.startswith('CC'):
                    if analytic.code[2:].isdigit():
                        lst.append(int(analytic.code[2:]))
                    else:
                        continue


            #     elif analytic.code and not analytic.code.startswith('CC'):
            #         i = -1
            #         for char in analytic.code:
            #             if analytic.code[i] == 'C':
            #                 lst.append(int(analytic.code[i+1:]))
            #             else:
            #                 i -= 1

            # if res.partner_id.customer_code:
            #     res.project_code_custom = res.partner_id.customer_code + '/CC' + str(max(lst)+ 1).zfill(3)
            #     # res.partner_id.project_counter += 1
            #
            # if res.partner_id.supplier_code:
            #     res.project_code_custom = res.partner_id.supplier_code + '/CC' + str(max(lst)+ 1).zfill(3)
                # res.partner_id.project_counter += 1
        #set reference code for analytic account related with project filled with project new code
        res.analytic_account_id.code = "CC" + str(max(lst)+ 1)
        res.project_code_custom = "CC" + str(max(lst)+ 1)
        res.name = res.analytic_account_id.display_name

        return res
    def get_so_count_related_to_project(self):
        for rec in self:
            count = rec.env['sale.order'].search_count([('analytic_account_id','=',rec.analytic_account_id.id)])
            rec.so_count_smart = count
    def get_po_count_related_to_project(self):
        for rec in self:
            count = rec.env['purchase.order'].search_count([('analytic_account_id','=',rec.analytic_account_id.id)])
            rec.po_count_smart = count
    def open_related_so(self):
        self.ensure_one()
        return {
            'name': 'Sale Order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'domain': [('analytic_account_id','=',self.analytic_account_id.id)],

        }
    def open_related_po(self):
        self.ensure_one()
        return {
            'name': 'Purchase Order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'domain': [('analytic_account_id','=',self.analytic_account_id.id)],

        }

    project_code_custom = fields.Char(string="Project Code", readonly=True,)
    opp_enhance_id = fields.Many2one(comodel_name="crm.lead", string="PR created By OPP ", )
    partner_id = fields.Many2one('res.partner', string='Customer', auto_join=True, tracking=True,required=True, domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    so_count_smart = fields.Integer(string="SO",compute=get_so_count_related_to_project)
    po_count_smart = fields.Integer(string="PO",compute=get_po_count_related_to_project)

