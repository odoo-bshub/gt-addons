# -*- coding: utf-8 -*-
##############################################################################
#
#    This module uses OpenERP, Open Source Management Solution Framework.
#    Copyright (C) 2017-Today Sitaram
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name': 'Discount Limit Per Users',
    'version': '14.0.0.0',
    'category': 'Extra Addons',
    "license": "AGPL-3",
    'summary': 'This app will helps you to set discount limit per users.',
    'description': """
        set discount limit
        user discount limit
        limited discount
        discount limit odoo apps
        how to limit for discount per user
        give restriction for giving discount per user
        discount restriction
        limit for the giving discount
        limit sales discount
        product discount limit
""",
    "price": 0,
    "currency": 'EUR',
    'author': 'Sitaram',
    'depends': ['base','sale_management','account'],
    'data': [
             'views/sr_inherit_users.xml',
    ],
    'installable': True,
    'auto_install': False,
    'live_test_url':'https://youtu.be/FuM_dusdtd8',
    "images":['static/description/banner.png'],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
