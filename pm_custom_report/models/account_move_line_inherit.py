from odoo import api, fields, models
from odoo.exceptions import ValidationError

class Account_move_Inherit(models.Model):
    _inherit = 'account.move'

    @api.model
    def create(self, values):
        res = super(Account_move_Inherit, self).create(values)
        # for line in res.invoice_line_ids:
        project = self.env['project.project'].search([('analytic_account_id','=',res.invoice_line_ids.analytic_account_id.id)])

                # lst_taxes_in_purchase = [p.amount / 100 for p in line.tax_ids]
                # lst_of_total = []
                # total_multiply = line.price_subtotal
                # for tax in lst_taxes_in_purchase:
                #     mul = total_multiply * tax
                #     lst_of_total.append(mul)
        if project.sale_order_amount or project.sale_order_amount != 0.0:
                project.write({
                            'collection_ids': [(0, 0, {
                                'project_id': project.id,
                                'invoice': res.id,
                                'invoice_value': res.amount_total,
                                'percentage': res.amount_total / project.sale_order_amount or 0.0,
                                'date_state': 'plan',
                                'bill_date': res.invoice_date,
                                'collection_state': 'plan',
                            })]
                        })
        if not project.sale_order_amount or project.sale_order_amount == 0.0:
                    project.write({
                        'collection_ids': [(0, 0, {
                            'project_id': project.id,
                            'invoice': res.id,
                            'invoice_value': res.amount_total,
                            'percentage': 0.0,
                            'date_state': 'plan',
                            'bill_date': res.invoice_date,
                            'collection_state': 'plan',
                        })]
                    })
                # else:
                #     raise ValidationError("Kindly ,First you must create sale order related to this analytic account")
        return res

    def action_post(self):
        project_collection_lines = self.env['project.collection'].search([('invoice','=',self.id)])
        for line in project_collection_lines:
            line.update({
                    'date_state':'issue',
                    'collection_state':'issue',
                })

        return self._post(soft=False)

    custom_purchase_id = fields.Many2one(comodel_name="purchase.order", string="Purchase", readonly=True )


