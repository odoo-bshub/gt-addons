from odoo import api, fields, models

class Stock_Picking_Inherit(models.Model):
    _inherit = 'stock.picking'

    delivery_method = fields.Char(string="Delivery Method",)
    contact_id = fields.Many2one(comodel_name="res.partner", string="Select Contact", required=False,
                                 )