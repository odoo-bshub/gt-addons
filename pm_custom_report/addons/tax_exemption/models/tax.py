from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PartnerTaxExemption(models.Model):
    _inherit = 'res.partner'

    tax_exemption = fields.Boolean(string="Tax Exemption", )
    sales_tax_ids = fields.Many2many(comodel_name="account.tax",
                                     string="Sales Tax", )

    @api.onchange('tax_exemption')
    def set_value_null_sales_tax(self):
        for rec in self:
            if rec.tax_exemption == True:
                rec.sales_tax_ids = False


class AccountTaxExemption(models.Model):
    _inherit = 'account.move'

    tax_exemption_account = fields.Boolean(string="",
                                           compute="_set_tax_exemption_readonly_account")

    tax_exemption_account_ids = fields.Many2many(comodel_name="account.tax",
                                                 compute="_set_tax_exemption_account_ids",
                                                 compute_sudo=True, )

    @api.depends('partner_id')
    def _set_tax_exemption_account_ids(self):
        for rec in self:
            if rec.tax_exemption_account_ids or not rec.tax_exemption_account_ids:
                if rec.partner_id.tax_exemption == False:
                    rec.tax_exemption_account_ids = rec.partner_id.sales_tax_ids

    @api.depends('partner_id')
    def _set_tax_exemption_readonly_account(self):
        for rec in self:
            if rec.tax_exemption_account or not rec.tax_exemption_account:
                if rec.partner_id.tax_exemption == True and rec.is_sale_document(include_receipts=True):
                    rec.tax_exemption_account = True


class AccountMoveLineTax(models.Model):
    _inherit = 'account.move.line'

    tax_exemption_line_move = fields.Boolean(string="",
                                             related="move_id.tax_exemption_account")

    tax_ids = fields.Many2many('account.tax', string='Taxes', help="Taxes "
                                                                   "that "
                                                                   "apply on "
                                                                   "the base "
                                                                   "amount",
                               check_company=True,
                               # related='move_id.tax_exemption_account_ids',
                               readonly=False)

    def _get_computed_taxes(self):
        self.ensure_one()

        if self.move_id.is_sale_document(include_receipts=True):
            # Out invoice.
            if self.product_id.taxes_id:
                tax_ids = self.product_id.taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            elif self.move_id.tax_exemption_account_ids:
                tax_ids = self.move_id.tax_exemption_account_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_sale_tax_id
        elif self.move_id.is_purchase_document(include_receipts=True):
            # In invoice.
            if self.product_id.supplier_taxes_id:
                tax_ids = self.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == self.move_id.company_id)
            elif self.account_id.tax_ids:
                tax_ids = self.account_id.tax_ids
            else:
                tax_ids = self.env['account.tax']
            if not tax_ids and not self.exclude_from_invoice_tab:
                tax_ids = self.move_id.company_id.account_purchase_tax_id
        else:
            # Miscellaneous operation.
            tax_ids = self.account_id.tax_ids

        if self.company_id and tax_ids:
            tax_ids = tax_ids.filtered(lambda tax: tax.company_id == self.company_id)

        return tax_ids


class SaleTaxExemption(models.Model):
    _inherit = 'sale.order'

    tax_exemption_true = fields.Boolean(string="",
                                        compute="_set_tax_exemption_readonly")
    tax_exemption_ids = fields.Many2many(comodel_name="account.tax",
                                         compute="_set_tax_exemption_ids",
                                         compute_sudo=True, )

    @api.depends('partner_id')
    def _set_tax_exemption_readonly(self):
        for rec in self:
            if rec.tax_exemption_true or not rec.tax_exemption_true:
                if rec.partner_id.tax_exemption == True:
                    rec.tax_exemption_true = True

    @api.depends('partner_id')
    def _set_tax_exemption_ids(self):
        for rec in self:
            if rec.tax_exemption_ids or not rec.tax_exemption_ids:
                if rec.partner_id.tax_exemption == False:
                    rec.tax_exemption_ids = rec.partner_id.sales_tax_ids


class SaleOrderLineTax(models.Model):
    _inherit = 'sale.order.line'

    tax_exemption_line = fields.Boolean(string="",
                                        related="order_id.tax_exemption_true")
    tax_id = fields.Many2many('account.tax', string='Taxes',
                              compute_sudo=True, store=True,
                              domain=['|', ('active', '=', False),
                                      ('active', '=', True)])

    def _compute_tax_id(self):
        for line in self:
            line = line.with_company(line.company_id)
            fpos = line.order_id.fiscal_position_id or line.order_id.fiscal_position_id.get_fiscal_position(
                line.order_partner_id.id)
            # If company_id is set, always filter taxes by the company
            taxes = line.product_id.taxes_id.filtered(
                lambda t: t.company_id == line.env.company)
            line.tax_id = fpos.map_tax(taxes, line.product_id,
                                       line.order_id.partner_shipping_id)
            if line.order_id.tax_exemption_ids:
                line.tax_id = line.order_id.tax_exemption_ids


