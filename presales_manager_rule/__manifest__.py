
{
    "name": "Presales Manager Rule ",
    "version": "14.0.1.0.0",
    "category": "Project",
    "website": "https://bshub.com",
    "author": "samahkandil, " "Senior Odoo Developer",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "summary": "Presales Manager",
    "depends": ["crm"],
    "data": [
        "views/group.xml",
    ],
}
