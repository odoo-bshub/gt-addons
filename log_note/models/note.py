""" init object crm.lead """

from odoo import fields, models, api, _


class MailMessageNew(models.Model):
    _inherit = 'mail.message'

    @api.depends('date')
    @api.onchange('date')
    def get_message_log(self):
        for rec in self:
            if rec.model == 'crm.lead':
                self.env['note.note'].sudo().create(
                    {
                        'memo': rec.body,
                    }
                )
        return

    @api.model_create_multi
    def create(self, vals):
        cashboxes = super(MailMessageNew, self).create(vals)
        cashboxes.get_message_log()
        print('Ahmed')
        return cashboxes
