from odoo import api, fields, models
from datetime import datetime


class CRM_Lead_Inherit(models.Model):
    _inherit = 'crm.lead'

    def check_unupdated_opportunity(self):
        for rec in self :
            if (datetime.now() - rec.write_date).days > int(self.env['ir.config_parameter'].sudo().search([('key','=','unupdated_opportunity.no_of_days')]).value) :
                color = 1 # red
            else:
                color = 0 # No Color

            rec.color = color

    color = fields.Integer('Color Index', compute="check_unupdated_opportunity")



class CRM_Configuration(models.TransientModel):
    _inherit = ['res.config.settings']

    no_of_days = fields.Integer(string='No. Of Days')

    def set_values(self):
        super(CRM_Configuration, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param("unupdated_opportunity.no_of_days",
                                                         self.no_of_days)

    @api.model
    def get_values(self):
        res = super(CRM_Configuration, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        res['no_of_days'] = int(get_param('unupdated_opportunity.no_of_days'))
        return res
