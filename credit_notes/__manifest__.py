# -*- encoding: utf-8 -*-
{
	"name": "Custom Credit Notes",
	"version": "14.0",
	"author": "Ahmed Hegazy , bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'base','account','account_accountant',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	    Adding configuration for credit notes related to bills and invoices.
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/credit_note_settings.xml'
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
