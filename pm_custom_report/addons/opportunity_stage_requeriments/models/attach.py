from odoo import api, fields, models


class CRMAttach(models.Model):
    _inherit = 'ir.attachment'

    stage_attach_id = fields.Char(string="",readonly=True,)

    @api.model
    def create(self, vals):
        res = super(CRMAttach, self).create(vals)
        opportunity_id = self.env.context.get('active_id')
        attach_won = self.env['crm.lead'].search([
             ('id', '=', opportunity_id)])
        print(attach_won)
        if attach_won:
            attach_won.update({
                'stage_id':attach_won.next_stage_id
            })
        return res