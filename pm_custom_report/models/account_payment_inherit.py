from odoo import api, fields, models ,_

class AccountPaymentRegister_inherit(models.TransientModel):
    _inherit = 'account.payment.register'

    def action_create_payments(self):
        payments = self._create_payments()

        # start of my code
        # set payment in M2M field in account.move relation with hr.expense.sheet
        project_collection_lines = self.env['project.collection'].search(
            [('invoice', '=', self._context.get('active_id'))])
        for line in project_collection_lines:
            line.update({
                'collection_state': 'collect',
            })

        if self._context.get('dont_redirect_to_payments'):
            return True

        action = {
            'name': _('Payments'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment',
            'context': {'create': False},
        }
        if len(payments) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': payments.id,
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('id', 'in', payments.ids)],
            })
        return action