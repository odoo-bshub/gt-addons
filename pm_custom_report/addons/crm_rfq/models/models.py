from odoo import api, fields, models, _
from odoo.exceptions import UserError


class RFQCrm(models.Model):
    _inherit = 'crm.lead'

    def action_create_rfq(self):
        """ button to run action """
        self.ensure_one()
        action = \
            self.env.ref('purchase.purchase_rfq').read()[
                0]
        action['context'] = {
            'default_opportunity_id':
                self.id or False,
        }
        action['views'] = [(
            self.env.ref('purchase.purchase_order_form').id,
            'form'
        )]
        return action


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity", )