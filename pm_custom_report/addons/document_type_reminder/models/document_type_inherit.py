from odoo import fields, models,_
from datetime import timedelta


class document_Type_Inherit(models.Model):
    _inherit = 'document.type'

    def get_email_with_recipient(self):
        users = self.env['res.users'].search([])
        email_list =[]
        for user in users:
            if user.has_group('hr.group_hr_manager'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        print(email_list)
        return ",".join(email_list)

    def run_the_schedule_action(self):
        date_now = fields.Date.today()
        employees = self.env['hr.employee'].search([])
        doc_type = self.search([])
        for type in doc_type:
            if type.specific_date:
                for emp in employees:
                    if emp.joining_date:
                        joining_date = fields.Date.from_string(emp.joining_date)
                        reminder_date = joining_date + timedelta(days=type.specific_date)
                        print('joining_date :', joining_date)
                        print('reminder_date :', reminder_date)
                        if date_now == reminder_date:
                            # send mail for employee
                            mail_content_for_employee = "  Hello  " + emp.name + ",<br>Your Document (" + type.name + " ) is required , Please send it or contact your hr department."
                            main_content_of_employee = {
                                'subject': _('Document-%s is required') % (type.name),
                                'author_id': self.env.user.partner_id.id,
                                'body_html': mail_content_for_employee,
                                'email_to': emp.work_email,
                            }
                            self.env['mail.mail'].create(main_content_of_employee).send()

                            #send email for group admin of employee
                            mail_content_for_group_admin_of_employee = "Hello , Employee : " + emp.name + " Suppose to deliver a Document (" + type.name + " )  today. "
                            main_content_of_group_admin_of_employee = {
                                'subject': _('Document-%s is required today') % (type.name),
                                'author_id': self.env.user.partner_id.id,
                                'body_html': mail_content_for_group_admin_of_employee,
                                'email_to': self.get_email_with_recipient(),
                            }
                            self.env['mail.mail'].create(main_content_of_group_admin_of_employee).send()

