""" init object crm.lead """

from odoo import fields, models,api ,_
from odoo.exceptions import UserError


class StockPickingReport(models.Model):
    _inherit = 'account.move'


class ReportDaily(models.TransientModel):
    _name = 'daily.report'

    analytic_account_id = fields.Many2one(
        comodel_name="account.analytic.account",
                              string=" Analytic Account", required=False, )
    date_form = fields.Date(string="Date From", required=True, )
    date_to = fields.Date(string="Date To  ", required=True, )

    def get_analytic_account_report(self):
        lst = []
        person_crm = self.env['account.move'].search([(
            'line_ids.analytic_account_id', '=', self.analytic_account_id.id),
            ('move_type', '!=', 'entry')])
        for rec in person_crm:
            if rec.create_date.date() >= self.date_form and \
                    rec.create_date.date() <= self.date_to:
                lst.append(
                    {'analytic': self.analytic_account_id.name,
                     'name':
                         rec.name,
                     'partner':
                         rec.partner_id.name,
                     'date': rec.invoice_date,

                     })
        return lst


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    def open_related_invoice(self):
        self.ensure_one()
        return {
            'name': 'Invoices/Bills',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': [('line_ids.analytic_account_id', '=', self.id),
                       ('move_type', '!=', 'entry')],

        }




