# -*- encoding: utf-8 -*-
{
	"name": "CRM Approval for Opportunity Data Entered",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm',
		# 'opportunity_stage_requeriments',
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'templates/create_opportunity.xml',
		'templates/review_crm_data.xml',
		'templates/after_review_data_in_crm.xml',
		# 'views/crm_lead.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
