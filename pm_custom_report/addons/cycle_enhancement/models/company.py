from odoo import api, fields, models

class Company_Inherit(models.Model):
    _inherit = 'res.company'

    company_code = fields.Char(string="Company Code", )
    num_customer = fields.Integer(string="num Customer", default=1, readonly=True)
    num_supplier = fields.Integer(string="num Supplier", default=1, readonly=True)