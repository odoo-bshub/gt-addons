from odoo import api, fields, models


class Summary(models.Model):
    _name = 'summary.summary'

    def action_open_related_cost_calcu(self):
        action = self.env.ref('cost_sheet.cost_calcu_action_view').read()[0]
        action['context'] = {}
        action['domain'] = [('id', '=', self.cost_calculation_id.id)]
        return action

    def get_cost_calcu_count(self):
        for rec in self:
            count = self.env['cost.calc'].search_count([('id', '=', self.cost_calculation_id.id)])
            rec.cost_calcu_count = count or 0

    name = fields.Char(string="Name", )
    partner_id = fields.Many2one(comodel_name="res.partner", string="Customer", )
    project_id = fields.Many2one(comodel_name="project.project", string="Project")
    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Related Opportunity",related='cost_calculation_id.opportunity_id')
    currency_id = fields.Many2one(comodel_name="res.currency", string="Currency", required=True,
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id)
    cost_calculation_id = fields.Many2one(comodel_name="cost.calc", )
    summary_line_ids = fields.One2many(comodel_name="summary.summary.line", inverse_name="summary_id",
                                       string="Summary Line")

    # smart button
    cost_calcu_count = fields.Integer(string="Cost Calculation", compute=get_cost_calcu_count)


class SummaryLine(models.Model):
    _name = 'summary.summary.line'

    @api.onchange('cost_dollar', 'selling_dollar')
    @api.depends('cost_dollar', 'selling_dollar')
    def get_overall_margin(self):
        for rec in self:
            if rec.cost_dollar and rec.selling_dollar:
                rec.overall_margin = (1 - rec.cost_dollar / rec.selling_dollar) * 100
            else:
                rec.overall_margin = 0.0

    # @api.onchange('cost_discount', 'input_margin')
    # @api.depends('cost_discount', 'input_margin')
    # def on_change_discount_and_margin_change_related_lines_in_cost_calculation(self):
    #     for rec in self:
    #         if rec.cost_discount or rec.input_margin:
    #             for record in rec.cost_line_ids.ids:
    #                 self.env['cost.calc.line'].search([('id','=',38)]).update({
    #                     'discount': rec.cost_discount,
    #                 })
                    # self.cost_calculation_id.write({
                    #     'cost_ids': [
                    #         (1, record, {'discount': rec.cost_discount,
                    #                      'margin': rec.input_margin,
                    #                      }),
                    #     ],
                    # })
                #excute method that get the total_ids
                # self.summary_id.cost_calculation_id.on_change_cost_ids_set_total_ids_by_partner()

    summary_id = fields.Many2one(comodel_name="summary.summary", string="Summary ID", )

    cost_line_ids = fields.Many2many(comodel_name="cost.calc.line", relation="", column1="", column2="", string="", )

    brand_id = fields.Many2one(comodel_name="brand.config", string="Brand", required=True, )
    list = fields.Float(string='List', readonly=True, )
    cost_discount = fields.Float(string="Cost Disc", )
    input_margin = fields.Float(string="Input margin", )
    cost_dollar = fields.Float(string="Cost $", readonly=True)
    selling_dollar = fields.Float(string="Selling $", readonly=True)
    selling_sar = fields.Float(string="Selling SAR", readonly=True)
    overall_margin = fields.Float(string="Overall Margin", readonly=True,compute=get_overall_margin,store=True)

    cost_calculation_id = fields.Many2one(comodel_name="cost.calc", )


