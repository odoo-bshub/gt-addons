from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountMoveTax(models.Model):
    _inherit = 'account.move'

    global_box = fields.Boolean(string="Set Global Tax ",  )
    tax_line_ids = fields.Many2many(comodel_name="account.tax",
                                    string="Global Tax", )

    # @api.onchange('tax_line_ids')
    # @api.depends('tax_line_ids')
    # def set_tax_lines(self):
    #     for rec in self:
    #         if rec.tax_line_ids:
    #             for line in rec.invoice_line_ids:
    #                 line.tax_ids=rec.tax_line_ids

    @api.constrains('invoice_line_ids')
    @api.onchange('invoice_line_ids')
    def set_constrain_lines_global_tax(self):
        for rec in self:
            if rec.global_box == True and not rec.tax_line_ids:
                raise UserError(_(
                    'You must Set Global Tax before set line '))
            else:
                pass

    # @api.depends('global_box')
    @api.onchange('global_box')
    def delete_invoice_line(self):
        for rec in self:
            # if rec.global_box or not rec.global_box:
            rec.invoice_line_ids =False
            rec.invoice_line_ids.unlink()

    @api.depends('tax_line_ids')
    @api.onchange('tax_line_ids')
    def delete_invoice_line_tax(self):
        for line in self:
            if line.tax_line_ids != True:
                line.invoice_line_ids.unlink()



class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    @api.onchange('product_id', 'move_id.tax_line_ids')
    @api.depends('product_id', 'move_id.tax_line_ids')
    def set_tax_lines_set(self):
        for rec in self:
            if rec.move_id.tax_line_ids:
                rec.tax_ids = rec.move_id.tax_line_ids


def _get_computed_taxes(self):
    self.ensure_one()

    if self.move_id.is_sale_document(include_receipts=True):
        # Out invoice.
        if self.product_id.taxes_id:
            tax_ids = self.product_id.taxes_id.filtered(
                lambda tax: tax.company_id == self.move_id.company_id)
        elif self.account_id.tax_ids:
            tax_ids = self.account_id.tax_ids
        elif self.move_id.global_box==True:
            tax_ids = self.move_id.tax_line_ids
        else:
            tax_ids = self.env['account.tax']
        if not tax_ids and not self.exclude_from_invoice_tab:
            tax_ids = self.move_id.company_id.account_sale_tax_id
    elif self.move_id.is_purchase_document(include_receipts=True):
        # In invoice.
        if self.product_id.supplier_taxes_id:
            tax_ids = self.product_id.supplier_taxes_id.filtered(
                lambda tax: tax.company_id == self.move_id.company_id)
        elif self.account_id.tax_ids:
            tax_ids = self.account_id.tax_ids
        elif self.move_id.global_box==True:
            tax_ids = self.move_id.tax_line_ids
        else:
            tax_ids = self.env['account.tax']
        if not tax_ids and not self.exclude_from_invoice_tab:
            tax_ids = self.move_id.company_id.account_purchase_tax_id
    else:
        # Miscellaneous operation.
        tax_ids = self.account_id.tax_ids

    if self.company_id and tax_ids:
        tax_ids = tax_ids.filtered(
            lambda tax: tax.company_id == self.company_id)

    return tax_ids