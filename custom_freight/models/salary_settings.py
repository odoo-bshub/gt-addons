from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class SalarySettings(models.Model):
    _inherit = 'hr.contract'

    variable_salary = fields.Float(string="Variable",  required=True, )
    medical = fields.Float(string="Medical",  required=True, )
    allowance = fields.Float(string="Allowance",  required=True, )
    deduction = fields.Float(string="Deduction",  required=True, )

    # def _compute_customer_name(self):
    #     for rec in self:
    #         if rec.direction == 'export':
    #             rec.customer = rec.shipper_id.name
    #         elif rec.direction == 'import':
    #             rec.customer = rec.consignee_id.name
