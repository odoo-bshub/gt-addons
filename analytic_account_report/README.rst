.. class:: text-left

CRM Opportunity Type
====================

CRM Opportunity Type

Usage
=====

* add new fields of opportunity type and tender specification amount

Credits
-------

.. |copy| unicode:: U+000A9 .. COPYRIGHT SIGN
.. |tm| unicode:: U+2122 .. TRADEMARK SIGN

- `Eslam Abdelmaaboud <eslam.abdelmabood@core-bpo.com>`_ |copy|
  `CORE B.P.O <http://www.core-bpo.com>`_ |tm| 2020
