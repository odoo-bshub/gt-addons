# -*- encoding: utf-8 -*-
{
	"name": "Description In Pivot",
	"version": "14.0",
	"author": "Ahmed Hegazy , BsHub",
	"website": "http://www.bshub.com",
	"depends": [
	'stock',
	],
	"category": "Reports",
	"description": """
	Adding new tab in model= product.product & model=product.template called "Report Description"
    Using this description in inventory pivot table reports
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'views/product_inherit.xml',
		'views/pivot_inherit.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
