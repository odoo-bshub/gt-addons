from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    company_type = fields.Selection(string='Company Type',
        selection=[('company', 'Company')],
        compute='_compute_company_type', inverse='_write_company_type',default='company',readonly=True)

    @api.depends('is_company')
    def _compute_company_type(self):
        for partner in self:
            partner.company_type = 'company' if partner.is_company else False

