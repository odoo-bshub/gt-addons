from odoo import api, models, _, fields


class LeadSubmissionDueDateMail(models.Model):
    _inherit = 'crm.lead'

    def get_email_to_rfp(self):
        email_list = []
        user_group = self.env.ref("sales_team.group_sale_manager")
        email_list = [
            usr.partner_id.email for usr in user_group.users if
            usr.partner_id.email]
        print(email_list)
        return ",".join(email_list)

