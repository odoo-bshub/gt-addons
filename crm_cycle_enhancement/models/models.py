# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree
import datetime
from dateutil import relativedelta
from odoo.exceptions import ValidationError


class CRMCreateLead(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False,
                        submenu=False):
        res = super(CRMCreateLead, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=False)
        doc = etree.XML(res['arch'])
        if view_type == 'form':
            nodes = doc.xpath("//form")
            for node in nodes:
                node.set('create', '0')
            res['arch'] = etree.tostring(doc)
        return res

    def create_project_from_crm(self):
        for rec in self:
            if rec.overall_margin < 20.0 and rec.is_dm_approval_required and rec.is_dm_approved:
                project = self.env['project.project'].sudo().create({
                    'name': rec.name,
                    'project_status': 1,
                    'partner_id': rec.partner_id.id,
                    'opp_enhance_id': rec.id,
                    'custom_start_date': datetime.datetime.today(),
                    'custom_end_date': datetime.datetime.today() + relativedelta.relativedelta(days=30)
                })
                rec.project_created_from_crm_id = project.id
                rec.project_created_from_crm_id.opportunity_id = rec.id
                rec.create_project_is_done = True
                rec.is_created_project = True

                # create_qoutation from related cost_sheet
                related_cost_calc = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                related_cost_calc.sudo().create_quotation()
                rec.expected_revenue = related_cost_calc.total_selling_sar

                # create PO from create project
                # lst_po = []
                # if related_cost_calc:
                #     for product in related_cost_calc.cost_ids:
                #         lst_po.append((0, 0, {'brand_id': product.brand_id.id,
                #                               'product_id': product.product_id.id,
                #                               'name': product.description,
                #                               'product_qty': product.qty,
                #                               'price_unit': product.unit_sar,
                #                               'account_analytic_id': rec.project_created_from_crm_id.analytic_account_id.id,
                #
                #                               }))
                    # PO = self.env['purchase.order'].create({
                    #     'partner_id': rec.partner_id.id,
                    #     'order_line': lst_po,
                    #     'company_id': rec.company_id.id,
                    #     'opportunity_id': rec.id,
                    #     'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                    # })

                rec.env['sale.order'].search([('opportunity_id', '=', self.id)]).action_confirm()
                sales = rec.env['sale.order'].search([('opportunity_id', '=', self.id)])

                rec.project_created_from_crm_id.sale_order_amount = sales.amount_total

                sales.update({
                    'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                })

                cost_sheet = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                cost_sheet.sudo().update({
                    'is_appear_button_po_create_in_costsheet': True
                })

                tasks = rec.env['project.task'].search([('opportunity_id', '=', rec.id)])
                tasks.sudo().update({
                    'project_id': rec.project_created_from_crm_id.id
                })
                rec.update({
                    'stage_id': self.next_stage_id
                })

            elif (20.0 <= rec.overall_margin <= 25.0) and rec.is_bum_approval_required and rec.is_bum_approved:
                project = self.env['project.project'].sudo().create({
                    'name': rec.name,
                    'project_status': 1,
                    'partner_id': rec.partner_id.id,
                    'opp_enhance_id': rec.id,
                    'custom_start_date': datetime.datetime.today(),
                    'custom_end_date': datetime.datetime.today() + relativedelta.relativedelta(days=30)
                })
                rec.project_created_from_crm_id = project.id
                rec.project_created_from_crm_id.opportunity_id = rec.id
                rec.create_project_is_done = True
                rec.is_created_project = True
                rec.update({
                    'stage_id': self.next_stage_id
                })
                # create_qoutation from related cost_sheet
                related_cost_calc = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                related_cost_calc.sudo().create_quotation()
                rec.expected_revenue = related_cost_calc.total_selling_sar

                # create PO from create project
                # lst_po = []
                # if related_cost_calc:
                #     for product in related_cost_calc.cost_ids:
                #         lst_po.append((0, 0, {'brand_id': product.brand_id.id,
                #                               'product_id': product.product_id.id,
                #                               'name': product.description,
                #                               'product_qty': product.qty,
                #                               'price_unit': product.unit_sar,
                #                               'account_analytic_id': rec.project_created_from_crm_id.analytic_account_id.id,
                #
                #                               }))
                    # PO = self.env['purchase.order'].create({
                    #     'partner_id': rec.partner_id.id,
                    #     'order_line': lst_po,
                    #     'company_id': rec.company_id.id,
                    #     'opportunity_id': rec.id,
                    #     'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                    # })

                rec.env['sale.order'].search([('opportunity_id', '=', self.id)]).action_confirm()
                sales = rec.env['sale.order'].search([('opportunity_id', '=', self.id)])

                rec.project_created_from_crm_id.sale_order_amount = sales.amount_total

                sales.update({
                    'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                })

                cost_sheet = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                cost_sheet.sudo().update({
                    'is_appear_button_po_create_in_costsheet': True
                })
                tasks = rec.env['project.task'].search([('opportunity_id', '=', rec.id)])
                tasks.sudo().update({
                    'project_id': rec.project_created_from_crm_id.id
                })
                rec.update({
                    'stage_id': self.next_stage_id
                })

            elif rec.overall_margin > 25.0:
                project = self.env['project.project'].sudo().create({
                    'name': rec.name,
                    'project_status': 1,
                    'partner_id': rec.partner_id.id,
                    'opp_enhance_id': rec.id,
                    'custom_start_date': datetime.datetime.today(),
                    'custom_end_date': datetime.datetime.today() + relativedelta.relativedelta(days=30)
                })
                rec.project_created_from_crm_id = project.id
                rec.project_created_from_crm_id.opportunity_id = rec.id
                rec.create_project_is_done = True
                rec.is_created_project = True
                # rec.update({
                #     'stage_id': self.next_stage_id
                # })
                # create_qoutation from related cost_sheet
                related_cost_calc = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                if related_cost_calc:
                    related_cost_calc.sudo().create_quotation()
                    rec.expected_revenue = related_cost_calc.total_selling_sar

                # create PO from create project
                # lst_po = []
                # if related_cost_calc:
                #     for product in related_cost_calc.cost_ids:
                #         lst_po.append((0, 0, {'brand_id': product.brand_id.id,
                #                               'product_id': product.product_id.id,
                #                               'name': product.description,
                #                               'product_qty': product.qty,
                #                               'price_unit': product.unit_sar,
                #                               'account_analytic_id': rec.project_created_from_crm_id.analytic_account_id.id,
                #                               }))
                    # PO = self.env['purchase.order'].create({
                    #     'partner_id': rec.partner_id.id,
                    #     'order_line': lst_po,
                    #     'company_id': rec.company_id.id,
                    #     'opportunity_id': rec.id,
                    #     'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                    # })

                rec.env['sale.order'].search([('opportunity_id', '=', self.id)]).action_confirm()
                sales = rec.env['sale.order'].search([('opportunity_id', '=', self.id)])

                rec.project_created_from_crm_id.sale_order_amount = sales.amount_total

                sales.update({
                    'analytic_account_id': rec.project_created_from_crm_id.analytic_account_id.id,
                })

                cost_sheet = rec.env['cost.calc'].search([('opportunity_id', '=', rec.id)])
                cost_sheet.sudo().update({
                    'is_appear_button_po_create_in_costsheet': True
                })
                tasks = rec.env['project.task'].search([('opportunity_id', '=', rec.id)])
                tasks.sudo().update({
                    'project_id': rec.project_created_from_crm_id.id
                })
                rec.update({
                    'stage_id': self.next_stage_id
                })
            else:
                raise ValidationError("""The Overall Margin requires an approval:
                                      - If less than 20% ---> Managing Director approval
                                      - If between 20% and 25% ---> Sales Manager approval """)

    # def action_create_task(self):
    #     """ button to run action """
    #     self.ensure_one()
    #     action = \
    #         self.env.ref('project.action_view_task').read()[
    #             0]
    #     action['context'] = {
    #         'default_name': 'HandOver : ' + self.name or False,
    #         'default_opportunity_id': self.id or
    #                                   False,
    #         'default_partner_id': self.partner_id.id or False,
    #         'search_default_my_tasks': 1 or False,
    #         'default_count': self.count,
    #         'default_user_id': self.project_manager_id.id,
    #         'default_is_created_task': True,
    #                   'default_project_id': self.project_created_from_crm_id.id or False,
    #     }
    #     action['views'] = [(
    #         self.env.ref('project.view_task_form2').id,
    #         'form'
    #     )]
    #     print(self._context.get('active_id'))
    #     return action

    def get_po_count(self):
        for rec in self:
            count = rec.env['purchase.order'].search_count([('opportunity_id', '=', rec.id)])
            rec.po_count = count

    def action_open_related_po(self):
        action = self.env.ref('purchase.purchase_rfq').read()[0]
        action['context'] = {}
        action['domain'] = [('opportunity_id', '=', self.id)]
        return action

    po_count = fields.Integer(string="Related PO", compute=get_po_count,)
    is_created_project = fields.Boolean(string="",  )