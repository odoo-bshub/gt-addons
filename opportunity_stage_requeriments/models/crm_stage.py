from odoo import api, fields, models, _


class Stage(models.Model):
    _inherit = "crm.stage"

    handover = fields.Boolean('Hand Over Task',
                                help=_('This stage require a handover task.'))

    proposal = fields.Boolean('Attachment Proposal',
                                help=_('This stage require a attachment '
                                       'Technical or Financial Proposal.'))
    new_prospect = fields.Boolean('Criteria of New Prospect',
                                help=_('This stage requires Check Box Fields '
                                       'True'
                                       '.'))
    approval = fields.Boolean('Criteria of approval',
                                help=_('This stage requires Check Box Fields '
                                       'True'
                                       '.'))
    disqualify = fields.Boolean('Criteria of Disqualify',
                             help=_('This stage requires Check Box Fields '
                                    'true'
                                    '.'))
    qualify = fields.Boolean('Criteria of Qualify',
                                help=_('This stage requires Check Box Fields '
                                       'true'
                                       '.'))
    develop = fields.Boolean('Criteria of Develop',
                          help=_('This stage requires Check Box Fields.'))

    contract = fields.Boolean('Contract',
                          help=_('This stage attachment Contract.'))
    negotiation = fields.Boolean('Negotiation',
                              help=_('This stage Negotiation .'))

    probability = fields.Float(string="Probability", required=False,
                                 help=_('for each stage set Probability.')
                                 )
    next_stage = fields.Many2one(comodel_name="crm.stage", string="Next Stage", required=False, )



    # @api.onchange('notification')
    # def _onchange_notification(self):
    #     for state in self:
    #         if not state.notification:
    #             state.responsible = None


# class SaleEdit(models.Model):
#     _inherit = 'sale.order'
#
#     def action_confirm(self):
#         if self.amount_total <= 0:
#             raise UserError(_(
#                 'Must There is At Least One Sale Order line'
#             ))
#         else:
#             if self._get_forbidden_state_confirm() & set(self.mapped('state')):
#                 raise UserError(_(
#                     'It is not allowed to confirm an order in the following states: %s'
#                 ) % (', '.join(self._get_forbidden_state_confirm())))
#
#             for order in self.filtered(lambda
#                                                order: order.partner_id not in order.message_partner_ids):
#                 order.message_subscribe([order.partner_id.id])
#             self.write({
#                 'state': 'sale',
#                 'date_order': fields.Datetime.now()
#             })
#             self._action_confirm()
#             if self.env.user.has_group('sale.group_auto_done_setting'):
#                 self.action_done()
#         return True