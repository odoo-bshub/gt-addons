from odoo import api, fields, models

class Project_Inherit_for_enhancement(models.Model):
    _inherit = 'project.project'

    @api.model
    def create(self, values):
        analytic_account = self.env['account.analytic.account'].create({
            'name': values.get('name'),
            # 'company_id': values.get('company_id'),
            'partner_id': values.get('partner_id'),
            'active': True,
        })

        values['analytic_account_id'] = analytic_account.id
        res = super(Project_Inherit_for_enhancement, self).create(values)
        if res.partner_id:
            if res.partner_id.customer_code:
                res.project_code_custom = res.partner_id.customer_code + '/PR' + str(res.partner_id.project_counter).zfill(2)
                res.partner_id.project_counter += 1

            if res.partner_id.supplier_code:
                res.project_code_custom = res.partner_id.supplier_code + '/PR' + str(res.partner_id.project_counter).zfill(2)
                res.partner_id.project_counter += 1
        #set reference code for analytic account related with project filled with project new code
        res.analytic_account_id.code = res.project_code_custom
        return res

    project_code_custom = fields.Char(string="Project Code", readonly=True,)
    opp_enhance_id = fields.Many2one(comodel_name="crm.lead", string="PR created By OPP ", )