from odoo import api, fields, models, _

TICKET_PRIORITY = [
    ('0', 'All'),
    ('1', 'Low priority'),
    ('2', 'Medium priority'),
    ('3', 'Med priority'),
    ('4', 'High priority'),
    ('5', 'Urgent'),
]
class Helpdesk_inherit_custom(models.Model):
    _inherit = 'helpdesk.ticket'

    @api.model
    def create(self, values):
        res = super(Helpdesk_inherit_custom, self).create(values)
        if res.assign_id:
            template_id = self.env.ref('helpdesk_custom.template_for_helpdesk_officer').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(res.id, force_send=True)
        return res

    def write(self, values):
        res= super(Helpdesk_inherit_custom, self).write(values)
        if values.get('assign_to_team_id'):
            template_id = self.env.ref('helpdesk_custom.template_for_helpdesk_assign_to_team_by_permission_group').id
            print(template_id)
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)
        return res

    def _get_user_of_ticket_officer(self):
        domain = [('id', '=', -1)]
        users_list = []
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group("helpdesk_custom.group_helpdesk_permission"):
                users_list.append(user.id)
        if users_list:
            domain = [('id', 'in', users_list)]
        return domain

    def _get_team_member(self):
        domain = [('id', '=', -1)]
        users_list = []
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group("helpdesk.group_helpdesk_manager"):
                users_list.append(user.id)
        if users_list:
            domain = [('id', 'in', users_list)]
        return domain

    description_1 = fields.Html()

    priority = fields.Selection(TICKET_PRIORITY, string='Priority', default='0')
    partner_name = fields.Many2one('res.users',string='Customer Name',default=lambda self: self.env.user , readonly=1)
    assign_id = fields.Many2one(comodel_name="res.users", string="Team admin",required=True,domain=_get_user_of_ticket_officer)
    assign_to_team_id = fields.Many2one(comodel_name="res.users", string="Assign To Team",required=False,domain=_get_team_member)
    attach_custom_id = fields.Many2many('ir.attachment', string="Attachment",
                                 help='You can attach the copy of your document', copy=False)








