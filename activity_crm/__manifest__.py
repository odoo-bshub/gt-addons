# -*- coding: utf-8 -*-
{
    'name': "Activity CRM",

    'summary': """
        """,

    'description': """
        Adding some modification to schedule activity action.
    """,

    'author': "",
    'website': "",

    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','crm'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/email_template.xml',
        'views/email_done_schedule.xml',
        'views/mail_message.xml',
        'views/views.xml',
    ],
   
}
