
.. class:: text-center

Missed Opportunity
==================

.. class:: text-left

Features
--------

- Odoo bot sends notification if no one work on opportunity for specific days

.. class:: text-left

Credits
-------

.. |copy| unicode:: U+000A9 .. COPYRIGHT SIGN
.. |tm| unicode:: U+2122 .. TRADEMARK SIGN

- `Muhamed Abd El-Rhman <muhamed.abdelrhman@core-bpo.com>`_ |copy|
  `CORE B.P.O <http://www.core-bpo.com>`_ |tm| 2020
