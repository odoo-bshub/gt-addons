from odoo import http
from odoo.http import request


class FreightApi(http.Controller):

    @http.route('/web/session/authenticate', type='json', auth="none")
    def authenticate(self, db, login, password, base_location=None):
        request.session.authenticate(db, login, password)
        return request.env['ir.http'].session_info()

    @http.route('/get_tracking', type="json", auth='user', csrf=False, methods=['GET'])
    def get_all_trackings(self, **kw):
        all_operations_with_tracking = request.env['freight.operation'].search(
            [('tracking_number', '=', int(kw['tracking_number']))])
        print('all_operations_with_tracking', all_operations_with_tracking)
        opt = []
        for rec in all_operations_with_tracking:
            for track in rec.freight_log:
                vals = {
                    'id': track.id,
                    'location': track.location,
                    'description': track.description,
                    'date': track.date,
                }
                opt.append(vals)
        if opt:
            opt = sorted(opt, key=lambda i: i['date'])
            data = {
                'status': "200",
                'message': "success",
                'response': opt,
            }
        else:
            data = {

                'status': "400",
                'message': "error",
                'response': False,
            }
        return data
