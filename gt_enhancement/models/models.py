from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CRMNew(models.Model):
    _inherit = 'crm.lead'

    pre_sales = fields.Many2one(comodel_name="res.users", string="Pre-Sales",
                                required=False,domain="[]", tracking=True, track_visibility='onchange')

    def get_email_sales_presales(self):
        email_list = []
        if self.user_id or self.pre_sales:
            email_list.append(self.user_id.email)
            email_list.append(self.pre_sales.email)
        print(email_list)
        return ",".join(email_list)