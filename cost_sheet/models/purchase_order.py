from odoo import api, fields, models

class PURCHASE_ORDER_INHERIT(models.Model):
    _inherit = 'purchase.order'


class PURCHASE_ORDER_LINE_INHERIT(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_id')
    @api.depends('product_id')
    def on_change_product_id_set_brand(self):
        for rec in self:
            if rec.product_id.brand_id:
                rec.brand_id = rec.product_id.brand_id.id

    brand_id = fields.Many2one(comodel_name="brand.config", string="Brand",readonly=True)
    is_created_from_crm = fields.Boolean(string="Is created from crm",)
