from odoo import api, fields, models


class Project_inherit(models.Model):
    _inherit = 'project.project'

    @api.model
    def create(self, values):
        res = super(Project_inherit, self).create(values)
        # if res.opportunity_id:
        #     my_crm = res.env['crm.lead'].search([('id', '=', res.opportunity_id.id)])
        #     my_crm.update({
        #         'custom_project_id': res.id
        #     })
        my_task = res.env['project.task'].search([('opportunity_id','=',res.opportunity_id.id)])
        my_task.update({
            'project_id': res.id
        })
        res.is_created_project = True
        return res

    def compute_project_crm_attach(self):
        self.ensure_one()
        return {
            'name': 'Project Attachment',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'ir.attachment',
            'domain': [('res_model', '=', 'crm.lead'), ('res_id', '=', self.opportunity_id.id)],
        }

    def get_count_of_related_opportunity(self):
        for rec in self:
            if rec.document_count or not rec.document_count:
                total = self.env['ir.attachment'].search_count(
                    [('res_model', '=', 'crm.lead'), ('res_id', '=', self.opportunity_id.id)])
                rec.document_count = total

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Related Opportunity",
                                     domain="[('custom_project_id','=',False),('type','=','opportunity')]")
    is_created_project = fields.Boolean(string="", )
    document_count = fields.Integer(string="", compute=get_count_of_related_opportunity)


class Project_Task_Inherit(models.Model):
    _inherit = 'project.task'

    @api.model
    def create(self, values):
        res = super(Project_Task_Inherit, self).create(values)
        if res.project_id.opportunity_id :
            res.opportunity_id = res.project_id.opportunity_id.id
        return res

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="opportunity",
                                     required=False, domain="[('type','=',"
                                                            "'opportunity'),"
                                                            "('custom_task_id',"
                                                            "'=',False)]",)


class CRM_Inherit(models.Model):
    _inherit = 'crm.lead'


    def open_related_project(self):
        self.ensure_one()
        return {
            'name': 'Task',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'project.project',
            'domain': [('opportunity_id', '=', self.id)],
            }

    custom_project_id = fields.Many2one(comodel_name="project.project", string="Project",)
    project_count = fields.Integer(string="",default=1,readonly=True)

