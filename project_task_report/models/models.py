from odoo import api, fields, models ,_
from datetime import datetime


class ProjectTask(models.Model):
    _inherit = 'project.task'

    milestone = fields.Many2one(comodel_name="project.status", string="Milestone", required=True,
                                related="project_id.project_status")
    commit = fields.Text(string="GT Response Commit", required=False, )
    details = fields.Text(string="Implementation Details", required=False, )
    # task_dependency= fields.Char( string="Task Dependency", required=False, )
    task_dependency_id = fields.Many2one(comodel_name="project.task", string="Task Dependency", required=False, )


class ProjectProject(models.Model):
    _inherit = 'project.project'

    project_reference = fields.Char(string="Project Reference", required=False, )
    ratio_complete = fields.Float(string="Project(% Completed)", required=False, compute="compute_progress_ratio",
                                  store=True)
    ratio_pending = fields.Float(string="Project(% Pending)", required=False, compute="compute_progress_ratio_pending",
                                 store=True)
    count_tasks = fields.Integer(string="Count All", compute="count_of_tasks", )
    count_complete = fields.Integer(string="Count", compute="count_of_tasks_complete", )

    @api.depends('task_ids')
    def count_of_tasks(self):
        for rec in self:
            if rec.count_tasks or not rec.count_tasks:
                rec.count_tasks = len(rec.task_ids)

    @api.depends('task_ids')
    def count_of_tasks_complete(self):
        for rec in self:
            if rec.count_complete or not rec.count_complete:
                for task in rec.task_ids:
                    if task.stage_id.complete == True:
                        rec.count_complete = len(task)

    @api.depends('count_complete', 'count_tasks')
    def compute_progress_ratio(self):
        for task in self:
            if task.ratio_complete or not task.ratio_complete:
                if (task.count_tasks > 0):
                    if task.count_tasks > task.count_complete:
                        task.ratio_complete = round(100.0 * task.count_complete / task.count_tasks, 2)
                else:
                    task.ratio_complete = 0.0

    def compute_progress_ratio_pending(self):
        for rec in self:
            if rec.ratio_pending or not rec.ratio_pending:
                rec.ratio_pending = 100 - rec.ratio_complete


class TaskProjectType(models.Model):
    _inherit = 'project.task.type'

    complete = fields.Boolean(string="Is Completed")


class AccountAnalyticLineNew(models.Model):
    _inherit = 'account.analytic.line'

    location = fields.Char(string="Location", required=False, )
    customer_contact = fields.Many2one(comodel_name="res.partner", string="Customer Contact", required=False, )
    time_in = fields.Datetime(string="Time In", required=False, )
    time_out = fields.Datetime(string="Time out", required=False, )
    daily_activity = fields.Text(string="Daily Activity Progress", required=False, )
    planned_activity = fields.Text(string="Planned Activity", required=False, )
    challenge = fields.Text(string="Challenge", required=False, )


class TaskTimeSheetNew(models.Model):
    _inherit = "project.task"

    def action_timer_start(self):
        if not self.user_timer_id.timer_start and self.display_timesheet_timer:
            self.write({
                'timesheet_ids': [(0, 0, {
                    'time_in': fields.Datetime.now(),
                    'name': '/',
                    'account_id': self.project_id.analytic_account_id.id,
                })]
            })
            return super(TaskTimeSheetNew, self).action_timer_start()

    daily_activity = fields.Text(string="Daily Activity Progress", required=False, )
    planned_activity = fields.Text(string="Planned Activity", required=False, )
    challenge = fields.Text(string="Challenge", required=False, )

class ProjectTaskCreateTimesheet(models.TransientModel):
    _inherit='project.task.create.timesheet'

    def save_timesheet(self):
        for line in self.task_id.timesheet_ids:
            if line.time_in and not line.time_out:
                line.update({
                    'time_out':datetime.now(),
                    'unit_amount': self.time_spent,
                    'task_id': self.task_id.id,
                    'project_id': self.task_id.project_id.id,
                    'date': datetime.now(),
                    'name': self.description,
                    'user_id': self.env.uid,
                })
        # values = {
        #     'task_id': self.task_id.id,
        #     'project_id': self.task_id.project_id.id,
        #     'date': datetime.now(),
        #     'name': self.description,
        #     'user_id': self.env.uid,
        #     'unit_amount': self.time_spent,
        # }
        self.task_id.user_timer_id.unlink()
        # return self.env['account.analytic.line'].create(values)



