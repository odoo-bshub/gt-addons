
{
    'name': "project stages requeriments",
    'version': '14.0.1.0.1',
    'category': 'Extra Tools',
    'author': 'samah kandil',
    'summary': """  
        Add requirements to configure project states.""",
    'license': 'AGPL-3',
    'depends': [
        'project',
        'project_status',
    ],
    'data':[
        # 'security/ir.model.access.csv',
        'security/group.xml',
        'views/stage.xml',
        'views/project.xml',
    ],
    'installable':True,
}
