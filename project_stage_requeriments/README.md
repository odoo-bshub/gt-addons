(crm_stage_requirements)
--------------------------------
--------------------------------

Restrict Stages In CRM.

CRM STAGES RESTRICT.


Usage:
-------------------
-Restrict Stage in Crm depend on Fields in Stage Configration.


Credits
-------

.. |copy| unicode:: U+000A9 .. COPYRIGHT SIGN
.. |tm| unicode:: U+2122 .. TRADEMARK SIGN

- `Samah Kandil <s.kandil@bshub.com>`_ |copy|
  `Elite <http://www.eliteg.com>`_ |tm| 2020
