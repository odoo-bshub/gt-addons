from odoo import api, fields, models, _
from datetime import timedelta


class CRM_Inherit(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def create(self, values):
        res = super(CRM_Inherit, self).create(values)
        if res.date_deadline:
            res.is_created_crm = True
        return res

    def check_date_deadline(self):
        for rec in self:
            if rec.date_deadline:
                user = self.env.user
                if user.has_group('sales_team.group_sale_manager'):
                    rec.is_created_crm = False
                else:
                    rec.is_created_crm = True
            else:
                rec.is_created_crm = False

    def open_wizard_approval_closing(self):
        wizard_form = self.env.ref("closing_date_cycle_reminder.closing_approval_form_view", False)
        return {
            'res_model': 'closing.approval',
            'type': 'ir.actions.act_window',
            'context': {},
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': wizard_form.id,
            'target': 'new'
        }



    def send_notification_for_remind_expected_closing_date(self):
        date_now = fields.Date.today()
        crms = self.search([])
        for opp in crms:
            if opp.date_deadline:
                close_date = fields.Date.from_string(opp.date_deadline)
                reminder_date_before_6_days = close_date - timedelta(days=6)
                reminder_date_before_4_days = close_date - timedelta(days=4)
                reminder_date_before_2_days = close_date - timedelta(days=2)
                if date_now == reminder_date_before_6_days or date_now == reminder_date_before_4_days or date_now == reminder_date_before_2_days or date_now == opp.date_deadline:
                    # send mail for employee
                    users= self.env['res.users'].search([])
                    recipients = []
                    #created by employee
                    if opp.create_uid.employee_id.work_email:
                        recipients.append(opp.create_uid.employee_id.work_email)
                    #sales person
                    if opp.user_id.employee_id.work_email:
                        recipients.append(opp.user_id.employee_id.work_email)

                    # direct manager for sales person
                    if opp.user_id.employee_id.parent_id.work_email:
                        recipients.append(opp.user_id.employee_id.parent_id.work_email)

                    #group sales adminstartor
                    for user in users:
                        if user.has_group('sales_team.group_sale_manager'):
                            if user.partner_id.email:
                                recipients.append(user.partner_id.email)
                    # print(recipients)

                    if recipients:
                        recipients_without_duplication= set(recipients)
                        final_version_lst_recipient = ",".join(recipients_without_duplication)
                        print(final_version_lst_recipient)

                    mail_content_for_employee = "  Hello , <br> This is a reminder for closing date of " + opp.name + " on " + str(opp.date_deadline)
                    main_content_of_employee = {
                        'subject': _('reminder for closing date of %s') % (opp.name),
                        'author_id': self.env.user.partner_id.id,
                        'body_html': mail_content_for_employee,
                        'email_to':final_version_lst_recipient ,
                    }
                    self.env['mail.mail'].create(main_content_of_employee).send()


    is_created_crm = fields.Boolean(string="is created", compute=check_date_deadline)


class Send_Email_For_Approval(models.TransientModel):
    _name = 'closing.approval'

    def get_email_with_recipient(self):
        users = self.env['res.users'].search([])
        email_list = []
        for user in users:
            if user.has_group('sales_team.group_sale_manager'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        print(email_list)
        return ",".join(email_list)

    def send_email_for_approval_by_sale_admin(self):
        template_id = self.env.ref(
            'closing_date_cycle_reminder.template_mail_approval_for_update_expected_closing_date').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(self.id, force_send=True)

    crm_id = fields.Many2one(comodel_name="crm.lead", string="Crm ID",
                             default=lambda self: self._context.get("active_id"), readonly=True)
    new_date = fields.Date(string="New Date", required=True, )
    company_id = fields.Many2one(comodel_name="res.company", string="Company", default=lambda self: self.env.company.id,
                                 readonly=True)
