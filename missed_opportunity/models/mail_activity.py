""" Mail Activity """
from odoo import fields, models


class MailActivity(models.Model):
    """ inherit Mail Activity """
    _inherit = 'mail.activity'

    # pylint: disable=no-member
    def action_feedback(self, feedback=False, attachment_ids=None):
        """ update feedback and mark as done """
        for activity in self:
            if activity.res_model == "crm.lead":
                # get the lead
                lead = self.env['crm.lead'] \
                    .search([('id', '=', activity.res_id)], limit=1)
                if lead:
                    done_time = fields.Datetime.now().replace(second=0)
                    lead.write({'last_activity_done_date': done_time})
        return super().action_feedback(feedback, attachment_ids)
