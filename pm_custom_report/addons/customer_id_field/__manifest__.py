# -*- encoding: utf-8 -*-
{
	"name": "Customer ID In Contact",
	"version": "14.0",
	"author": "",
	"website": "",
	"sequence": 5,
	"depends": [
		'contacts','cycle_enhancement'
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/res_partner.xml'

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
