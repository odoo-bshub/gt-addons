from odoo import api, fields, models,_
from odoo.exceptions import ValidationError


class CRM_Inherit(models.Model):
    _inherit = 'crm.lead'

    def create_cost_calculation(self):
        self.ensure_one()
        return {
            'name': _('Cost Calculation'),
            'view_mode': 'tree',
            'view_id': self.env.ref('cost_sheet.cost_calcu_tree_view').id,
            'res_model': 'cost.calc',
            'domain': [('opportunity_id', '=', self.id)],
            'type': 'ir.actions.act_window',
        }

    def action_open_related_cost_calc(self):
        action = self.env.ref('cost_sheet.cost_calcu_action_view').read()[0]
        action['context'] = {}
        action['domain'] = [('opportunity_id', '=', self.id)]
        return action

    def action_open_related_purchase_order(self):
        action = self.env.ref('purchase.purchase_rfq').read()[0]
        action['context'] = {}
        action['domain'] = [('opportunity_id', '=', self.id)]
        return action

    def create_quotation_from_crm(self):
        if self.created_cost_calculation_id:
            if self.created_cost_calculation_id.cost_ids:
                quotation = self.created_cost_calculation_id.sudo().create_quotation()
                self.create_quotation_from_crm_bool = True
                return {
                    'effect': {
                        'fadeout': 'slow',
                        'message': "Congratulations you have created a new Quotation with id ( " + str(quotation.id) + " )",
                        'type': 'rainbow_man',
                    }
                }
            else:
                raise ValidationError("You must fill Cost Calculation table with brands/products")
        else:
            raise ValidationError("You must create Cost calculation first")

    def create_purchase_order(self):
        for rec in self:
            if rec.created_cost_calculation_id:
                if rec.created_cost_calculation_id.cost_ids:
                    line = []

                    for product in rec.created_cost_calculation_id.cost_ids:
                        line.append((0, 0, {'brand_id': product.brand_id.id,
                                            'product_id': product.product_id.id,
                                            'product_uom_qty': product.qty,
                                            'price_unit': product.unit_sar,
                                            'is_created_from_crm': True,
                                            }))
                    purchase = self.env['purchase.order'].sudo().create({
                        'partner_id': rec.partner_id.id,
                        'order_line': line,
                        'opportunity_id': self.id,
                        'company_id': self.company_id.id,
                    })
                    rec.update({
                        'purchase_custom_id': purchase.id,
                    })
                    return {
                        'effect': {
                            'fadeout': 'slow',
                            'message': "Congratulations you have created a new RFQ with id ( " + str(purchase.id) + " )",
                            'type': 'rainbow_man',
                        }
                    }
                else:
                    raise ValidationError("You must fill cost calculation table before creating RFQ")
            else:
                raise ValidationError("You must fill cost calculation table before creating RFQ")

    def get_cost_calc_count(self):
        for rec in self:
            count = rec.env['cost.calc'].search_count([('opportunity_id', '=', rec.id)])
            print(count)
            rec.cost_calc_count = count

    # def get_purchase_order_count(self):
    #     for rec in self:
    #         count = self.env['purchase.order'].search_count([('opportunity_id', '=', self.id)])
    #         rec.purchase_from_crm_count = count

    def action_view_sale_quotation(self):
        action = self.env.ref('sale.action_quotations_with_onboarding').read()[0]
        action['context'] = {}
        action['domain'] = [('opportunity_id', '=', self.id)]
        return action

    cost_calc_count = fields.Integer(string="", compute=get_cost_calc_count,)
    # purchase_from_crm_count = fields.Integer(string="", compute=get_purchase_order_count)
    created_cost_calculation_id = fields.Many2one(comodel_name="cost.calc", string="related cost calc", readonly=True)
    create_cost_calculation_bool = fields.Boolean(string="", )
    create_quotation_from_crm_bool = fields.Boolean(string="", )
    purchase_custom_id = fields.Many2one(comodel_name="purchase.order", string="Purchase order", readonly=True)
    quotation_count = fields.Integer(related='created_cost_calculation_id.quotation_count', string="Number of Quotations")

