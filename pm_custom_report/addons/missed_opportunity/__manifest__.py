# pylint: disable=missing-docstring,manifest-required-author
{
    'name': 'Missed Opportunity',
    'summary': 'Missed Opportunity',
    'author': "Samah-Kandil, BsHub",
    'website': "http://www.bshub.com",
    'version': '14.0.1.0.0',
    'category': '',
    'license': 'AGPL-3',
    'depends': [
        'crm',
    ],
    'data': [
        'data/ir_cron_data.xml',
        'data/mail_template.xml',
        'views/crm_lead.xml',
        'views/crm_stage.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
