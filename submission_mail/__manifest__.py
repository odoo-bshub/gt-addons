
{
    'name': "Submission Due Mail",
    'version': '14.0.1.0.1',
    'category': 'Submission Due',
    'author': 'samah kandil',
    'summary': """  
        Send Mail if Submission Due date  Created or changed.""",
    'license': 'AGPL-3',
    'depends': [
        'opportunity_stage_requeriments',
    ],
    'data':[
        'security/ir.model.access.csv',
        'data/mail.xml',

    ],
    'installable':True,
}
