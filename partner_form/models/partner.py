from odoo import api, fields, models, _


class PartnerForm(models.Model):
    _inherit = 'res.partner'

    arabic_name = fields.Char(string="Arabic Name", required=False, )
    partner_iban = fields.Integer(string="Partner IBAN ", required=False, )
    cr = fields.Char(string="CR", required=False, )
    supplier_credit = fields.Float(string="Supplier Credit Limit",
                               required=False, )
    user_id = fields.Many2one('res.users', string='Salesperson', required=True,
      help='The internal user in charge of this contact.')


class CRM_Arabic_Name(models.Model):
    _inherit = 'crm.lead'

    @api.onchange('partner_id')
    def onchange_partner_id_set_arabic_name(self):
        for rec in self:
            if rec.partner_id :
                rec.arabic_name = rec.partner_id.arabic_name or False

            if rec.partner_id.is_company:
                rec.partner_is_company = True
            else:
                rec.partner_is_company = False

    arabic_name = fields.Char(string="Arabic Name",readonly=True)
    partner_is_company = fields.Boolean(string="",  )

    @api.onchange('partner_id')
    def change_user_id_contact(self):
        for rec in self:
            rec.user_id=rec.partner_id.user_id

    user_id = fields.Many2one('res.users', string='Salesperson', index=True, tracking=True,readonly=True,)




