# -*- encoding: utf-8 -*-
{
	"name": "Required Amount in Tree View",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'account',
		'account_accountant',
		'era_tax_partner',

	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/account_move_inherit.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
