# -*- encoding: utf-8 -*-
{
	"name": "Partner Screen Updates",
	"version": "14.0",
	"author": "bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'base','contacts','account','purchase','opportunity_stage_requeriments'
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to add products on opportunity and create quote with that. 
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/partner.xml',
		'views/crm_lead.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
