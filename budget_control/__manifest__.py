# -*- encoding: utf-8 -*-
{
	"name": "Budget Control and restrictions",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'account_budget','account','hr_expense',
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
