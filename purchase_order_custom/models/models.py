from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PurchaseOrderNew(models.Model):
    _inherit = 'purchase.order'

    def get_default_for_notes(self):
        return """
            1: Please acknowledge receipt of this order by E-mail (to: GTOperations@gt-ict.com) 
            2: Enter this order in accordance with the prices, terms, delivery method, and specifications listed above.
            3: partial delivery subject to approval.
            4: Credit period starts from the date of receiving the original commercial invoice.
            """

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Related Opportunity", readonly=True, )

    analytic_account_id = fields.Many2one(comodel_name="account.analytic.account", string="Project Analytic Account",
                                          required=False, )
    contact_id = fields.Many2one(comodel_name="res.partner", string="Select Contact", required=False,
                                 )
    country = fields.Selection(string="Country", selection=[('ksa', 'KSA'), ('egy', 'EGY'), ], default="ksa", )
    notes_in_po = fields.Html(string="Notes", default=get_default_for_notes)
    current_approve_po = fields.Char(string="Current Approve", readonly=True, )

    state = fields.Selection([
        ('draft', 'RFP'),
        ('sent', 'RFA Sent'),
        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', tracking=True)

    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            if self.env.context.get('send_rfq', False):
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase')[1]
            else:
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase_done')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.order',
            'active_model': 'purchase.order',
            'active_id': self.ids[0],
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'custom_layout': "mail.mail_notification_paynow",
            'force_email': True,
            'mark_rfq_as_sent': True,
        })

        # In the case of a RFQ or a PO, we want the "View..." button in line with the state of the
        # object. Therefore, we pass the model description in the context, in the language in which
        # the template is rendered.
        lang = self.env.context.get('lang')
        if {'default_template_id', 'default_model', 'default_res_id'} <= ctx.keys():
            template = self.env['mail.template'].browse(ctx['default_template_id'])
            if template and template.lang:
                lang = template._render_lang([ctx['default_res_id']])[ctx['default_res_id']]

        self = self.with_context(lang=lang)
        if self.state in ['draft', 'sent']:
            ctx['model_description'] = _('Request for Quotation')
        else:
            ctx['model_description'] = _('Purchase Order')

        self.current_approve_po = "BU Approval"

        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def pmo_approval_group(self):
        email_list = []
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group('purchase_order_custom.purchase_approve_custom'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        return ",".join(email_list)

    def manage_director_approval_group(self):
        email_list = []
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group('purchase_order_custom.purchase_final_confirm'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        return ",".join(email_list)

    def button_PM_approve(self):
        for rec in self:
            rec.is_approve_by_pm = True

            template_id = self.env.ref('purchase_order_custom.template_mail_pmo_approve_purchase').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)

    def button_approve_new(self):
        self.write({'state': 'to approve', 'date_approve': fields.Datetime.now()})
        self.current_approve_po = "Managing Director Approval"

        template_id = self.env.ref('purchase_order_custom.template_mail_md_purchase_approval').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(self.id, force_send=True)

    def _approval_allowed(self):
        """Returns whether the order qualifies to be approved by the current user"""
        self.ensure_one()
        return (
                self.company_id.po_double_validation == 'one_step'
                or (self.company_id.po_double_validation == 'two_step'
                    and self.amount_total < self.env.company.currency_id._convert(
                    self.company_id.po_double_validation_amount, self.currency_id, self.company_id,
                    self.date_order or fields.Date.today()))
                or self.user_has_groups('purchase.group_purchase_manager'))

    def button_confirm(self):
        for order in self:
            self.current_approve_po = " "
            if order.state not in ['draft', 'sent', 'to approve']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order._approval_allowed():
                order.button_approve()
            else:
                order.write({'state': 'purchase'})
            if order.partner_id not in order.message_partner_ids:
                order.message_subscribe([order.partner_id.id])
        return True

    is_approve_by_pm = fields.Boolean(string="", )


class PurchaseOrderLineNew(models.Model):
    _inherit = 'purchase.order.line'

    @api.depends('product_id', 'date_order', 'order_id.analytic_account_id')
    def _compute_analytic_id_and_tag_ids(self):
        for rec in self:
            default_analytic_account = rec.env['account.analytic.default'].sudo().account_get(
                product_id=rec.product_id.id,
                partner_id=rec.order_id.partner_id.id,
                user_id=rec.env.uid,
                date=rec.date_order,
                company_id=rec.company_id.id,
            )
            rec.account_analytic_id = rec.order_id.analytic_account_id or default_analytic_account.analytic_id
            rec.analytic_tag_ids = rec.analytic_tag_ids or default_analytic_account.analytic_tag_ids

    account_analytic_id = fields.Many2one('account.analytic.account', store=True, string='Analytic Account',
                                          compute='_compute_analytic_id_and_tag_ids', readonly=True)
