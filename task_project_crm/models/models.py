from odoo import api, fields, models, _
from odoo.exceptions import UserError


class TaskCrm(models.Model):
    _inherit = 'project.task'

    user_id = fields.Many2one('res.users',
        string='Task Owner',
        default=lambda self: self.env.uid,
        index=True, tracking=True)

    manager_id = fields.Many2one(comodel_name="res.users",string="Project Manager", required=False,related="opportunity_id.project_manager_id" )


class CRMTask(models.Model):
    _inherit = 'crm.lead'

    task_owner_id = fields.Many2one(comodel_name="res.users", string="Task Owner",related="custom_task_id.user_id" )

