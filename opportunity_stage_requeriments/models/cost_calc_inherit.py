from odoo import api, fields, models

class Cost_Calc_Inherit(models.Model):
    _inherit = 'cost.calc'

    def write(self, values):
        if 'overall_margin_total' in values and  self.opportunity_id.stage_id.id == 13:
            if values['overall_margin_total'] < 20.0 :
                self.opportunity_id.update({'is_dm_approval_required': True})
                self.opportunity_id.update({'is_bum_approval_required' : False })
                self.opportunity_id.update({'is_sales_admin_approval_required': False})
                template_id = self.env.ref('opportunity_stage_requeriments.template_mail_for_managing_director_approval').id
                template = self.env['mail.template'].browse(template_id)
                template.sudo().send_mail(self.opportunity_id.id, force_send=True)

            elif 20.0 <= values['overall_margin_total'] <= 25.0 and self.opportunity_id.stage_id.id == 13:
                self.opportunity_id.update({'is_bum_approval_required' : True})
                self.opportunity_id.update({'is_dm_approval_required': False})
                self.opportunity_id.update({'is_sales_admin_approval_required':False})
                template_id = self.env.ref(
                    'opportunity_stage_requeriments.template_mail_for_business_unit_approval').id
                template = self.env['mail.template'].browse(template_id)
                template.sudo().send_mail(self.opportunity_id.id, force_send=True)

            # elif values['overall_margin_total'] > 25.0 and self.opportunity_id.stage_id.id == 13:
            #     self.opportunity_id.update({'is_sales_admin_approval_required':True})
            #     self.opportunity_id.update({'is_bum_approval_required':False})
            #     self.opportunity_id.update({'is_dm_approval_required':False})
            #     template_id = self.env.ref(
            #         'opportunity_stage_requeriments.template_mail_for_sales_admin_approval').id
            #     template = self.env['mail.template'].browse(template_id)
            #     template.sudo().send_mail(self.opportunity_id.id, force_send=True)
        return super(Cost_Calc_Inherit, self).write(values)