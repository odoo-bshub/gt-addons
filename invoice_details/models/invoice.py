from odoo import api, fields, models, _
import datetime


class AccountMoveTax(models.Model):
    _inherit = 'account.move'


    def get_large_date_in_payment_term(self):
        for rec in self:
            if rec.move_type =='entry':
                if not rec.invoice_payment_term_id:
                    return rec.invoice_date_due
                else:
                    lst_dates = []
                    for line in rec.line_ids:
                        lst_dates.append(line.date_maturity)

                    max_date = max(d for d in lst_dates if isinstance(d, datetime.date))
                    return max_date
            else:
                return rec.invoice_payment_term_id.display_name

    # this method to check down payment in invoice lines
    # used in custom report to print down payment with the products in related sale order
    def check_down_payment_method_when_printing_report_invoice(self):
        for rec in self:
            length_of_invoice_lines = len(rec.invoice_line_ids)
            dic_of_sale_order = {}
            for line in rec.invoice_line_ids:
                # check invoice lines length = 1 as (Down payment)
                if length_of_invoice_lines == 1 and line.product_id.name == "Down payment":
                    # get related sale order record
                    sale_order_record = self.env['sale.order'].search([('name', '=', rec.invoice_origin)])
                    for sale_line in sale_order_record.order_line:
                        # exclude (Down payment) Line in sale oder lines
                        if sale_line.product_id.name != "Down payment":
                            if sale_line.product_id.id in dic_of_sale_order:
                                dic_of_sale_order[sale_line.product_id.id].append(
                                    {'product_name': sale_line.product_id.name,
                                     'description': sale_line.name,
                                     'qty': sale_line.product_uom_qty,
                                     'qty_unit': sale_line.product_uom.name,
                                     'unit_price': sale_line.price_unit,
                                     'tax': ', '.join(map(lambda x: (x.description or x.name), sale_line.tax_id)),
                                     'subtotal': sale_line.price_subtotal
                                     })
                            else:
                                dic_of_sale_order[sale_line.product_id.id] = [
                                    {'product_name': sale_line.product_id.name,
                                     'description': sale_line.name,
                                     'qty': sale_line.product_uom_qty,
                                     'qty_unit': sale_line.product_uom.name,
                                     'unit_price': sale_line.price_unit,
                                     'tax': ', '.join(map(lambda x: (x.description or x.name), sale_line.tax_id)),
                                     'subtotal': sale_line.price_subtotal
                                     }]
            return dic_of_sale_order




    shipping_method = fields.Char(string="Shipping Method", )




