# -*- encoding: utf-8 -*-
{
	"name": "Closing Date Approval Cycle and reminder",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm',
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/crm_view.xml',
		'views/closing_approval.xml',
		'views/approval_email_temp.xml',
		'views/cron_reminder.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
