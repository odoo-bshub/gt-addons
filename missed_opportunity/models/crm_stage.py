""" Crm Stage """
from odoo import fields, models


class CrmStage(models.Model):
    """ inherit Crm Stage """
    _inherit = 'crm.stage'

    missed_mail_template_id = fields.Many2one(
        'mail.template', domain="[('model', '=', 'crm.lead')]",
    )
    missed_delay = fields.Float(
        help="# days after which the opportunity is considered missed.",
        string="Is missed After",
        default=7.0
    )
    user_ids = fields.Many2many('res.users')
