from odoo import api, fields, models

class Sale_Inherit_for_enhancement(models.Model):
    _inherit = 'sale.order'

    # @api.model
    # def create(self, values):
    #     res = super(Sale_Inherit_for_enhancement, self).create(values)
    #
    #     if res.analytic_account_id:
    #         related_proj =self.env['project.project'].search([('analytic_account_id','=',res.analytic_account_id.id)])
    #         if related_proj.project_code_custom:
    #             opp_lst = []
    #             related_opp = related_proj.opp_enhance_id
    #             my_opp_code = related_opp.crm_code_custom.split(sep='O')
    #
    #             #code from project analytic account + opp number + sale order number
    #             res.sale_code_custom = related_proj.project_code_custom + '/O' + my_opp_code[1] + '/SO' + str(
    #                 related_opp.sale_counter).zfill(2)
    #             related_opp.sale_counter += 1
    #     return res

    sale_code_custom = fields.Char(string="Code", readonly=True, )