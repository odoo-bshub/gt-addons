# pylint: disable=missing-docstring, manifest-required-author
{
    'name': 'GT Custom Report',
    'summary': ' Report Custom',
    'author': "Samah Kandil,BsHub",
    'website': "http://www.bshub.com",
    'category': 'tool',
    'version': '14.0.1.0.1',
    'license': 'AGPL-3',
    'depends': [
        'base',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'report/custom_footer.xml',
        'report/custom_header.xml',
        'report/custom_layout.xml',
        'views/res_company_views.xml',
    ],
    'demo': [

    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
