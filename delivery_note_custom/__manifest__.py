# -*- encoding: utf-8 -*-
{
    "name": "Delivery Note Custom",
    "version": "14.0",
    "author": "Ahmed Hegazy ,Bshub",
    "website": "http://www.bshub.com",
    "sequence": 5,
    "depends": [
        'stock',
    ],
    "category": "",
    "complexity": "",
    "description": """
	
	""",
    "data": [
        'security/ir.model.access.csv',
        'reports/delivery_note_temp.xml',
        'reports/reports.xml',
        'views/stock_picking_inherit.xml',

    ],
    "demo": [
    ],
    "test": [
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
    'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
