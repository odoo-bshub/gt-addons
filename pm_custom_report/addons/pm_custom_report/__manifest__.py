# -*- encoding: utf-8 -*-
{
	"name": "Custom Project Management with Report",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'cost_sheet','adhoc_report','project','sale','purchase','account_accountant',
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'reports/pm_report.xml',
		'views/project_inherit.xml',
		'views/account_move_inherit.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
