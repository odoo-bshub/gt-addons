from odoo import api, fields, models
from odoo.exceptions import ValidationError
from collections import defaultdict


class CRM_Activity(models.Model):
    _inherit = 'mail.activity'
    _description = 'Adding some modification to schedule activity action.'

    @api.depends('res_model')
    def get_current_model(self):
        for rec in self:
            if rec.res_model == 'crm.lead':
                rec.is_crm = True
            else:
                rec.is_crm = False

    @api.depends('res_model')
    def get_stage_from_crm(self):
        for rec in self:
            if rec.res_model == 'crm.lead':
                rec.stage_id = rec.env['crm.lead'].browse(rec.res_id).stage_id.id
            else:
                rec.stage_id = False
    @api.depends('sales_team_id')
    def get_value_of_sales_team(self):
        for rec in self:
            if rec.sales_team_id :
                rec.is_sales_team_filled = True
            else:
                rec.is_sales_team_filled= False

    def get_email_with_recipient(self):
        email_list=[]
        email_list = [
            usr.partner_id.email for usr in self.recipient_ids if
            usr.partner_id.email]
        if self.user_id:
            email_list.append(self.user_id.partner_id.email)
        print(email_list)
        return ",".join(email_list)


    def action_close_dialog(self):
        if self.res_model == 'crm.lead':
            if self.user_id or self.recipient_ids:
                template_id = self.env.ref('activity_crm.template_mail_schedule_activity').id
                template = self.env['mail.template'].browse(template_id)
                template.send_mail(self.id,force_send=True)
            else:
                raise ValidationError("You must select at least one recipient Or (user to) field")
        return super(CRM_Activity, self).action_close_dialog()

    def action_done(self):
        if not self.user_id:
            raise ValidationError('You must Assign activity to someone (select from assign to field)')

        # self.is_clicked_on_mark_as_to_do_or_done_and_schedule_next = True

        super(CRM_Activity, self).action_done()

    def _action_done(self, feedback=False, attachment_ids=None):
        """ Private implementation of marking activity as done: posting a message, deleting activity
            (since done), and eventually create the automatical next activity (depending on config).
            :param feedback: optional feedback from user when marking activity as done
            :param attachment_ids: list of ir.attachment ids to attach to the posted mail.message
            :returns (messages, activities) where
                - messages is a recordset of posted mail.message
                - activities is a recordset of mail.activity of forced automically created activities
        """
        # marking as 'done'
        messages = self.env['mail.message']
        next_activities_values = []

        # Search for all attachments linked to the activities we are about to unlink. This way, we
        # can link them to the message posted and prevent their deletion.
        attachments = self.env['ir.attachment'].search_read([
            ('res_model', '=', self._name),
            ('res_id', 'in', self.ids),
        ], ['id', 'res_id'])

        activity_attachments = defaultdict(list)
        for attachment in attachments:
            activity_id = attachment['res_id']
            activity_attachments[activity_id].append(attachment['id'])

        for activity in self:
            # extract value to generate next activities
            if activity.force_next:
                Activity = self.env['mail.activity'].with_context(activity_previous_deadline=activity.date_deadline)  # context key is required in the onchange to set deadline
                vals = Activity.default_get(Activity.fields_get())

                vals.update({
                    'previous_activity_type_id': activity.activity_type_id.id,
                    'res_id': activity.res_id,
                    'res_model': activity.res_model,
                    'res_model_id': self.env['ir.model']._get(activity.res_model).id,
                })
                virtual_activity = Activity.new(vals)
                virtual_activity._onchange_previous_activity_type_id()
                virtual_activity._onchange_activity_type_id()
                next_activities_values.append(virtual_activity._convert_to_write(virtual_activity._cache))

            # post message on activity, before deleting it
            record = self.env[activity.res_model].browse(activity.res_id)
            record.message_post_with_view(
                'mail.message_activity_done',
                values={
                    'activity': activity,
                    'feedback': feedback,
                    'display_assignee': activity.user_id != self.env.user
                },
                subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
                mail_activity_type_id=activity.activity_type_id.id,
                attachment_ids=[(4, attachment_id) for attachment_id in attachment_ids] if attachment_ids else [],
            )

            # Moving the attachments in the message
            # TODO: Fix void res_id on attachment when you create an activity with an image
            # directly, see route /web_editor/attachment/add
            activity_message = record.message_ids[0]
            message_attachments = self.env['ir.attachment'].browse(activity_attachments[activity.id])
            if message_attachments:
                message_attachments.write({
                    'res_id': activity_message.id,
                    'res_model': activity_message._name,
                })
                activity_message.attachment_ids = message_attachments
            messages |= activity_message

        next_activities = self.env['mail.activity'].create(next_activities_values)
        if not self.user_id:
            raise ValidationError('You must Assign activity to someone (select from assign to field)')
        if self.res_model == 'crm.lead':
            if self.user_id or self.recipient_ids:
                messages.sudo().update({
                    'activity_type_id':self.activity_type_id.id,
                    'summary':self.summary,
                    'user_id':self.user_id.id,
                    'sales_team_id':self.sales_team_id.id,
                    'recipient_ids':self.recipient_ids.ids,
                    'is_crm':self.is_crm,
                    'is_sales_team_filled':self.is_sales_team_filled,
                    'company_id':self.company_id.id,
                    'stage':self.stage_id.name,
                    'my_due_date':self.date_deadline,
                })
                template_id = self.env.ref('activity_crm.template_mail_done_schedule_activity').id
                template = self.env['mail.template'].browse(template_id)
                template.sudo().send_mail(messages.id, force_send=True)
                current_record_activity = self.env['mail.message'].search([('res_id', '=', self.id)])
                current_record_activity.unlink()
            else:
                raise ValidationError("You must select at least one recipient")

        self.unlink()  # will unlink activity, dont access `self` after that

        return messages, next_activities
    
    def action_done_schedule_next(self):
        if not self.user_id:
            raise ValidationError('You must Assign activity to someone (select from assign to field)')

        self.is_clicked_on_mark_as_to_do_or_done_and_schedule_next = True
        return self.action_feedback_schedule_next()

    user_id = fields.Many2one('res.users', 'User to',default=lambda self: self.env.user, index=True, required=False)
    sales_team_id = fields.Many2one(comodel_name="crm.team", string="Sales Team",)
    recipient_ids = fields.Many2many(comodel_name="res.users", string="Recipient", )
    is_crm = fields.Boolean(string="Is CRM", compute=get_current_model)
    is_sales_team_filled = fields.Boolean(string="", compute=get_value_of_sales_team)
    company_id = fields.Many2one(comodel_name="res.company",default=lambda self : self.env.company )
    stage_id = fields.Many2one(comodel_name="crm.stage", string="stage",compute=get_stage_from_crm)
    is_clicked_on_mark_as_to_do_or_done_and_schedule_next = fields.Boolean(string="",  )


class Mail_Message_Inherit(models.Model):
    _inherit = 'mail.message'

    def get_email_with_recipient(self):
        email_list=[]
        email_list = [
            usr.partner_id.email for usr in self.recipient_ids if
            usr.partner_id.email]
        if self.user_id:
            email_list.append(self.user_id.partner_id.email)
        print(email_list)
        return ",".join(email_list)

    activity_type_id = fields.Many2one(comodel_name="mail.activity.type", string="Activity Type", readonly=True)
    summary = fields.Char(string="Summary", readonly=True )
    user_id = fields.Many2one('res.users', 'User to', readonly=True)
    sales_team_id = fields.Many2one(comodel_name="crm.team", string="Sales Team", readonly=True )
    recipient_ids = fields.Many2many(comodel_name="res.users", string="Recipient",readonly=True )
    is_crm = fields.Boolean(string="Is CRM", readonly=True)
    is_sales_team_filled = fields.Boolean(string="", readonly=True)
    company_id = fields.Many2one(comodel_name="res.company", readonly=True)
    stage = fields.Char(string="Stage", readonly=True, )
    my_due_date = fields.Date(string="Due Date", readonly=True, )
