# -*- encoding: utf-8 -*-
{
	"name": "HR Screen Updates",
	"version": "14.0",
	"author": "bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'hr',
		'oh_employee_documents_expiry',
		'hr_payroll',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to add Updates on employee screen. 
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'views/hr.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
