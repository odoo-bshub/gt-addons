# -*- encoding: utf-8 -*-
{
	"name": "Sale Invoice BsHub",
	"version": "14.0",
	"author": "Samah Kandil , BsHub.",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'sale',
		'sale_management',
		'account',
		'account_accountant',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to add global tax for account. 
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'views/sale.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
