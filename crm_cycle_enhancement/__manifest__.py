# -*- coding: utf-8 -*-
{
    'name': "CRM Cycle enhancement",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "samah kandil",
    'website': "http://bshub.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['crm',
                'opportunity_stage_requeriments',
                'cycle_enhancement',
                'sale_crm',
                'abs_opportunity_task',
                'project',
                'cost_sheet',
                'purchase_order_custom',
                'project_status',
                'purchase',
                'pm_custom_report',
                ],

    # always loaded
    'data': [
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'installable': True,
    'application': True,
    'auto_install': False,
}
