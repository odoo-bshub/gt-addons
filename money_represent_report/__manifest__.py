# -*- encoding: utf-8 -*-
{
    "name": "Money Represent Report",
    "version": "14.0",
    "author": "Ahmed Hegazy ,Bshub",
    "website": "http://www.bshub.com",
    "sequence": 5,
    "depends": [
        'crm','sale_management','num_to_words','custom_report','cost_sheet'
    ],
    "category": "",
    "complexity": "",
    "description": """
	
	""",
    "data": [
        'security/ir.model.access.csv',
        'views/fin_pro.xml',
        'reports/financial_proposal_report.xml',
        'reports/reports.xml',

    ],
    "demo": [
    ],
    "test": [
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
    'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
