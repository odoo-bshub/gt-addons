# -*- coding: utf-8 -*-
{
    'name': "Project new Task",

    'summary': """
        """,

    'description': """
 	Adding a field called pipeline to link project with a specific opportunity.   
	This field should include all of pipelines not linked to a specific project.
	Project should include all of pipeline's attachments.
	Project can be linked only with only one pipeline.
	related opportunity  in all task related to project,
 	handover task related with project,
 	project should only related with handover task that is related to opportunity,


    """,

    'author': "",
    'website': "",

    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['project','project_status','hr_timesheet','analytic','timer'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/template.xml',
        'report/report.xml',
        'views/project_task.xml',
        'wizards/project_report.xml',
    ],
   
}
