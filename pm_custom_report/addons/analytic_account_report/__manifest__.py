# pylint: disable=missing-docstring, manifest-required-author
{
    'name': ' Analytic Account Report ',
    'summary': ' Analytic Account Report ',
    'author': "samah kandil, BsHub",
    'website': "http://www.BsHub.com",
    'version': '14.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'account',
        'account_accountant',

    ],
    'data': [
        'security/ir.model.access.csv',
        'views/report_template.xml',
        # 'views/report_template_details.xml',
        'views/report.xml',
        'views/inventory.xml',
    ],
    'auto_install': False,
}
