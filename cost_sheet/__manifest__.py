# -*- encoding: utf-8 -*-
{
    "name": "Cost Sheet",
    "version": "14.0",
    "author": "Ahmed Hegazy ,Bshub",
    "website": "http://www.bshub.com",
    "sequence": 5,
    "depends": [
		'sale',
		'purchase',
		'sale_management',
		'project',
        'crm',
        'sale_crm',
        'cycle_enhancement',
    ],
    "category": "",
    "complexity": "",
    "description": """
		Create 2 new screens 
		Cost Calculation 
		Summary 
	""",
    "data": [
        'security/ir.model.access.csv',
        'views/summary.xml',
        'views/cost_calcu.xml',
        'views/sale_order.xml',
        'views/purchase_order.xml',
        'views/configuration.xml',
        'views/product_inherit.xml',
        'views/crm_inherit.xml',
        'views/menuitem.xml',
    ],
    "demo": [
    ],
    "test": [
    ],
    "auto_install": False,
    "installable": True,
    "application": False,
    'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
