from odoo import api, fields, models
from odoo.exceptions import ValidationError

class Respartner_Inherit(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create(self, values):
        res = super(Respartner_Inherit, self).create(values)
        #IF Customer ( created from customer form ) will  increase code acccourding to company
        if res.kind == 'customer':
            res.customer_rank = 1
            company = self.env.company
            res.customer_code = company.company_code +"/CUS"+ str(company.num_customer).zfill(4)
            company.num_customer += 1
            res.company_id = self.env.company.id
        # IF Supplier(created from vendor form) increase code acccourding to company
        if res.kind == 'vendor':
            res.supplier_rank = 1
            company = self.env.company
            res.supplier_code = company.company_code + "/SUP" + str(company.num_supplier).zfill(4)
            company.num_supplier += 1
            res.company_id = self.env.company.id

        res.is_created_partner = True

        # if contact child of customer or vendor will make code on his own kind as customer or supplier
        if res.parent_id:
            # if his parent is customer
            if res.parent_id.is_created_partner != True:
                raise ValidationError('You are  not allowed to add child contact before saving this record!! please remove any child contact before saving')
            else:
                if res.parent_id.customer_code:
                    res.customer_code = res.parent_id.customer_code +'/'+ str(res.parent_id.child_num).zfill(2)
                    res.parent_id.child_num += 1
                    res.kind = 'customer'
                    res.company_id = self.env.company.id
                # if his parent is Vendor
                if res.parent_id.supplier_code:
                    res.supplier_code = res.parent_id.supplier_code +'/'+ str(res.parent_id.child_num).zfill(2)
                    res.parent_id.child_num += 1
                    res.kind = 'vendor'
                    res.company_id = self.env.company.id

        return res


    supplier_code = fields.Char(string="Supplier Code", required=False,readonly=True )
    customer_code = fields.Char(string="Customer Code", required=False,readonly=True )
    child_num = fields.Integer(string="Child Num", default=1, readonly=True)
    project_counter = fields.Integer(string="Project Counter",default=1, readonly=True)
    crm_counter = fields.Integer(string="crm Counter",default=1, readonly=True)
    is_created_partner = fields.Boolean(string="",  )

class Import(models.TransientModel):
    _inherit = 'base_import.import'

    def do(self, fields, columns, options, dryrun=False):
        """ Actual execution of the import

        :param fields: import mapping: maps each column to a field,
                       ``False`` for the columns to ignore
        :type fields: list(str|bool)
        :param columns: columns label
        :type columns: list(str|bool)
        :param dict options:
        :param bool dryrun: performs all import operations (and
                            validations) but rollbacks writes, allows
                            getting as much errors as possible without
                            the risk of clobbering the database.
        :returns: A list of errors. If the list is empty the import
                  executed fully and correctly. If the list is
                  non-empty it contains dicts with 3 keys ``type`` the
                  type of error (``error|warning``); ``message`` the
                  error message associated with the error (a string)
                  and ``record`` the data which failed to import (or
                  ``false`` if that data isn't available or provided)
        :rtype: dict(ids: list(int), messages: list({type, message, record}))
        """
        self.ensure_one()
        self._cr.execute('SAVEPOINT import')

        try:
            data, import_fields = self._convert_import_data(fields, options)
            # Parse date and float field
            data = self._parse_import_data(data, import_fields, options)
        except ValueError as error:
            return {
                'messages': [{
                    'type': 'error',
                    'message': str(error),
                    'record': False,
                }]
            }

        _logger.info('importing %d rows...', len(data))

        name_create_enabled_fields = options.pop('name_create_enabled_fields', {})
        import_limit = options.pop('limit', None)
        model = self.env[self.res_model].with_context(import_file=True,
                                                      name_create_enabled_fields=name_create_enabled_fields,
                                                      _import_limit=import_limit)
        import_result = model.load(import_fields, data)
        _logger.info('done')

        # If transaction aborted, RELEASE SAVEPOINT is going to raise
        # an InternalError (ROLLBACK should work, maybe). Ignore that.
        # TODO: to handle multiple errors, create savepoint around
        #       write and release it in case of write error (after
        #       adding error to errors array) => can keep on trying to
        #       import stuff, and rollback at the end if there is any
        #       error in the results.
        try:
            if dryrun:
                self._cr.execute('ROLLBACK TO SAVEPOINT import')
                # cancel all changes done to the registry/ormcache
                self.pool.clear_caches()
                self.pool.reset_changes()
            else:
                self._cr.execute('RELEASE SAVEPOINT import')
        except psycopg2.InternalError:
            pass

        # Insert/Update mapping columns when import complete successfully
        if import_result['ids'] and options.get('headers'):
            BaseImportMapping = self.env['base_import.mapping']
            for index, column_name in enumerate(columns):
                if column_name:
                    # Update to latest selected field
                    mapping_domain = [('res_model', '=', self.res_model), ('column_name', '=', column_name)]
                    column_mapping = BaseImportMapping.search(mapping_domain, limit=1)
                    if column_mapping:
                        if column_mapping.field_name != fields[index]:
                            column_mapping.field_name = fields[index]
                    else:
                        BaseImportMapping.create({
                            'res_model': self.res_model,
                            'column_name': column_name,
                            'field_name': fields[index]
                        })
        if 'name' in import_fields:
            index_of_name = import_fields.index('name')
            skipped = options.get('skip', 0)
            # pad front as data doesn't contain anythig for skipped lines
            r = import_result['name'] = [''] * skipped
            # only add names for the window being imported
            r.extend(x[index_of_name] for x in data[:import_limit])
            # pad back (though that's probably not useful)
            r.extend([''] * (len(data) - (import_limit or 0)))
        else:
            import_result['name'] = []

        skip = options.get('skip', 0)
        # convert load's internal nextrow to the imported file's
        if import_result['nextrow']:  # don't update if nextrow = 0 (= no nextrow)
            import_result['nextrow'] += skip

        # Start Of My Code +++++++++++++++++
        if "res.partner" == self.res_model:
            for record in import_result['ids']:
                current_record = self.env[self.res_model].browse(record)
                # IF Customer ( created from customer form ) will  increase code acccourding to company
                if current_record.kind == 'customer':
                    current_record.customer_rank = 1
                    current_record.customer_code = current_record.company_id.company_code + "/CUS" + str(current_record.company_id.num_customer).zfill(4)
                    current_record.company_id.num_customer += 1
                # IF Supplier(created from vendor form) increase code acccourding to company
                if current_record.kind == 'vendor':
                    current_record.supplier_rank = 1
                    current_record.supplier_code = current_record.company_id.company_code + "/SUP" + str(current_record.company_id.num_supplier).zfill(4)
                    current_record.company_id.num_supplier += 1

                current_record.is_created_partner = True

                # if contact child of customer or vendor will make code on his own kind as customer or supplier
                # if res.parent_id:
                #     # if his parent is customer
                #     if res.parent_id.is_created_partner != True:
                #         raise ValidationError(
                #             'You are  not allowed to add child contact before saving this record!! please remove any child contact before saving')
                #     else:
                #         if res.parent_id.customer_code:
                #             res.customer_code = res.parent_id.customer_code + '/' + str(res.parent_id.child_num).zfill(
                #                 2)
                #             res.parent_id.child_num += 1
                #         # if his parent is Vendor
                #         if res.parent_id.supplier_code:
                #             res.supplier_code = res.parent_id.supplier_code + '/' + str(res.parent_id.child_num).zfill(
                #                 2)
                #             res.parent_id.child_num += 1

        return import_result

