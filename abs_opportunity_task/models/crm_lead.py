from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class CrmLead(models.Model):
    _inherit = "crm.lead"

    task_count = fields.Integer(compute='_compute_task_count', string="Tasks Count")
    custom_task_id = fields.Many2one(comodel_name="project.task", string="task",
                                     required=False, )
    is_tech_proposal = fields.Boolean(string="Updated Technical Proposal", )
    is_fin_proposal = fields.Boolean(string="Updated Financial Proposal", )
    is_contract_updated = fields.Boolean(string="Updated Contract", )
    is_cost_updated = fields.Boolean(string="Updated Cost Sheet", )
    overall_margin = fields.Float(string="OverAll Margin", compute="compute_overall_all_margin",required=False, )
    scope_work = fields.Boolean(string="Scope Of Work", )
    supplier_quotation = fields.Boolean(string="Supplier Quotation", )
    customer_po = fields.Boolean(string="Customer PO", )
    is_hand_over = fields.Boolean(string="", )

    def compute_overall_all_margin(self):
        cost_sheets = self.env['cost.calc'].search([('opportunity_id', '=', self.id)])
        for rec in self:
            if rec.overall_margin or not rec.overall_margin:
                for sheet in cost_sheets:
                    rec.overall_margin=sheet.overall_margin_total

    @api.onchange('is_tech_proposal','is_fin_proposal','is_contract_updated','is_cost_updated','scope_work','supplier_quotation','customer_po')
    @api.depends('is_tech_proposal','is_fin_proposal','is_contract_updated','is_cost_updated','scope_work','supplier_quotation','customer_po')
    def compute_hand_over_task(self):
        for rec in self:
            if rec.is_hand_over or not rec.is_hand_over:
                if rec.is_tech_proposal == True and rec.is_fin_proposal == True and rec.is_contract_updated == True and \
                        rec.is_cost_updated == True and rec.scope_work == True and rec.supplier_quotation == True and rec.customer_po == True:
                    rec.is_hand_over = True
                else:
                    rec.is_hand_over = False

    def get_email_with_recipient_for_handovertask(self):
        users = self.env['res.users'].search([])
        email_list = []
        for user in users:
            if user.has_group('project.group_project_manager'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        print(email_list)
        return ",".join(email_list)

    def action_create_task(self):
        """ button to run action """
        if not self.is_hand_over  :
            raise ValidationError(
                "You Must Check all the Handover task requirements before you can create handover task")
        else:
            self.ensure_one()
            action = \
                self.env.ref('project.action_view_task').read()[
                    0]
            action['context'] = {
                'default_name': 'HandOver : ' + self.name or False,
                'default_opportunity_id': self.id or
                                          False,
                'default_partner_id': self.partner_id.id or False,
                'search_default_my_tasks': 1 or False,
                'default_count': self.count,
                'default_manager_id': self.project_manager_id.id,
                'default_is_created_task': True,
                # 'default_project_id': self.custom_project_id.id or False,
                # 'default_project_id': self.project_created_from_crm_id.id or False,
            }
            action['views'] = [(
                self.env.ref('project.view_task_form2').id,
                'form'
            )]
            # self.update({
            #     'stage_id': 4
            # })
            print(self.stage_id.name)
            print(self.next_stage_id.name)
            # print(self._context.get('active_id'))
            template_id = self.env.ref(
                'abs_opportunity_task.template_mail_for_project_handover_task').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)
            return action

    def _compute_task_count(self):
        count = 0
        task_ids = self.env['project.task'].search([('is_created_task', '=',
                                                     True)])
        for record in self:
            if record.task_count or not record.task_count:
                if record.name:
                    for task_id in task_ids:
                        if record.name == task_id.opportunity_id.name:
                            count = count + 1
                            record.task_count = count
                        else:
                            record.task_count = count
                else:
                    record.task_count = count
            return True

    def open_related_task(self):
        self.ensure_one()
        return {
            'name': 'Task',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'project.task',
            'domain': [('opportunity_id', '=', self.id), ('is_created_task', '=', True)],

        }


class TaskProjectTask(models.Model):
    _inherit = 'project.task'

    @api.model
    def create(self, values):
        res = super(TaskProjectTask, self).create(values)
        if res.opportunity_id:
            res.env['crm.lead'].search([('id', '=', res.opportunity_id.id)]).update({
                'custom_task_id': res.id
            })
        # res.is_created_task = True
        return res

    def action_open_attachments(self):
        pass

    def compute_task_crm_attach(self):
        self.ensure_one()
        return {
            'name': 'Task Attachment',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'ir.attachment',
            'domain': [('res_model', '=', 'crm.lead'),
                       ('res_id', '=', self.opportunity_id.id)],
        }

    count = fields.Integer(string="", required=False, )
    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="opportunity",
                                     required=False, domain="[('type','=',"
                                                            "'opportunity'),"
                                                            "('custom_task_id',"
                                                            "'=',False)]", )
    is_created_task = fields.Boolean(string="", )
