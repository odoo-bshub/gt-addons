# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree


class PurchaseEdit(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False,
                        submenu=False):
        res = super(PurchaseEdit, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=False)
        # print('gggggg')
        doc = etree.XML(res['arch'])
        if not self.env.user.has_group('hide_edit_project_purchase.group_edit_purchase'):
            if view_type == 'kanban':
                nodes = doc.xpath("//kanban")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
            if view_type == 'tree':
                nodes = doc.xpath("//tree")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
            if view_type == 'form':
                nodes = doc.xpath("//form")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
        return res


class ProjectEdit(models.Model):
    _inherit = 'project.project'

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False,
                        submenu=False):
        res = super(ProjectEdit, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=False)
        # print('gggggg')
        doc = etree.XML(res['arch'])
        if not self.env.user.has_group('hide_edit_project_purchase.group_edit_project'):
            if view_type == 'kanban':
                nodes = doc.xpath("//kanban")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
            if view_type == 'tree':
                nodes = doc.xpath("//tree")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
            if view_type == 'form':
                nodes = doc.xpath("//form")
                for node in nodes:
                    node.set('edit', '0')
                res['arch'] = etree.tostring(doc)
        return res
