# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime
from odoo.tools.misc import xlwt

class SalesCommession(models.TransientModel):
    _name = "sales.commession"

    employee_id = fields.Many2one('hr.employee', string='Employee',domain = [('department_id', '=','Sales Dept')],Required="True")
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    def print_report_excel(self):
        return self.env.ref('custom_freight.report_sales_commesion_xlx').report_action(self)

class SalesCommessionXLS(models.AbstractModel):
    _name = 'report.commession_sales_report_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        

        domain=[]
        if lines.employee_id:
            domain.append(('employee_id' , '=' , lines.employee_id.id))
        if lines.date_from and lines.date_to:
            st_date = datetime.strftime(lines.date_from , "%Y-%m-%d %H:%M:%S")
            en_date = datetime.strftime(lines.date_to , "%Y-%m-%d 23:59:59")
            domain.append(('datetime' , '>=' , st_date))
            domain.append(('datetime' , '<=' , en_date))
        if lines.date_from:
            st_date = datetime.strftime(lines.date_from , "%Y-%m-%d %H:%M:%S")
            domain.append(('datetime' , '>=' , st_date))
        if lines.date_to:
            en_date = datetime.strftime(lines.date_to , "%Y-%m-%d 23:59:59")
            domain.append(('datetime' , '<=' , en_date))

        records = self.env['freight.operation'].search(domain)
        
        worksheet = workbook.add_worksheet("Employee Sales Commession")
        f1 = workbook.add_format({'bold': True, 'font_color': '#000000', 'border': True, 'align': 'vcenter'})
        f2 = workbook.add_format({'bold': True, 'font_color': '#000000', 'border': False, 'align': 'vcenter'})

        cell_text_format = workbook.add_format({'align': 'center',
                                                'bold': True,
                                                'size': 12, })
        cell_text_format_values = workbook.add_format({'align': 'center',
                                                'bold': False,
                                                'size': 10, })
        worksheet.set_column('A:A', 1)
        worksheet.set_column('B:B', 23)
        worksheet.set_column('C:C', 25)
        worksheet.set_column('D:D', 30)
        worksheet.set_column('E:E', 30)
        worksheet.set_column('F:F', 18)
        worksheet.set_column('G:G', 20)
       
        row = 0
        col = 0

        worksheet.write(row + 5, col + 1, 'Name', cell_text_format)
        worksheet.write(row + 5, col + 2, 'Consignee', cell_text_format)
        worksheet.write(row + 5, col + 3, 'Total Invoiced(Receivables)', cell_text_format)
        worksheet.write(row + 5, col + 4, 'Total Bills(Payables)', cell_text_format)
        worksheet.write(row + 5, col + 5, 'Margin', cell_text_format)
        worksheet.write(row + 5, col + 6, 'Invoice Residual', cell_text_format)
      
        row = 6
        seq = 0
        
        total_inv=[]
        total_bill=[]
        total_margin=[]
        comm_paid=[]

        for rec in records:
            
            sales_percentage=0
            comm_percentage=0
            comm_paid_percentage=0
            summpaid=0
            invoice_amount =0
            bill_amount=0
            invoice_residual=0
            
            row += 1
            seq += 1
            worksheet.write(row, col + 1, rec.name, cell_text_format_values)
            worksheet.write(row, col + 2, rec.consignee_id.name, cell_text_format_values)
            for invoice in rec.invoice_created_ids:
                if (invoice .type == 'out_invoice' or invoice.type == 'out_receipt') and invoice.state == 'posted':
                    invoice_amount += abs(invoice .amount_untaxed_signed)
                    if invoice.invoice_payment_state=='not_paid':
                       invoice_residual += abs(invoice.amount_residual_signed)
                else:
                    if invoice.state == 'posted':
                        bill_amount += abs(invoice.amount_untaxed_signed)
                
            worksheet.write(row, col + 3, invoice_amount, cell_text_format_values)
            worksheet.write(row, col + 4, bill_amount, cell_text_format_values)
            worksheet.write(row, col + 5, round(invoice_amount-bill_amount), cell_text_format_values)
            worksheet.write(row, col + 6, invoice_residual, cell_text_format_values)

            total_inv.append(invoice_amount)
            total_bill.append(bill_amount)
            total_margin.append(invoice_amount-bill_amount)
            if invoice_residual ==0:
                   comm_paid.append(invoice_amount-bill_amount)
        summpaid=sum(comm_paid)
        Suminv = sum(total_inv)
        worksheet.write(row+1, col + 3, Suminv, cell_text_format)

        Sumbill = sum(total_bill)
        worksheet.write(row+1, col + 4, Sumbill, cell_text_format)
    
        Summargin = sum(total_margin)
        worksheet.write(row+1, col + 5, Summargin, cell_text_format)

        
        worksheet.write(row+6, col+1, 'MONTHLY TARGET', cell_text_format)
        worksheet.write(row+6, col+2, lines.employee_id.monthly_target, cell_text_format_values)

        worksheet.write(row+7, col+1, 'ACHIEVEMENT', cell_text_format)
        margin=round(Summargin)
        worksheet.write(row+7, col+2, margin, cell_text_format_values)

        worksheet.write(row+8, col+1, 'PERCENTAGE', cell_text_format)
        if margin < 0 or lines.employee_id.monthly_target==0:
             worksheet.write(row+8, col+2,"0% ", cell_text_format_values) 
        else:
            percentage=int(margin/lines.employee_id.monthly_target*100)
            worksheet.write(row+8, col+2, str(percentage) +'%', cell_text_format_values)
             
            if 0<=percentage<=60:
                sales_percentage=0
                comm_percentage=0
                comm_paid_percentage=summpaid
            elif 61 <= percentage <= 80:
                sales_percentage=0.07
                comm_percentage=0.07
                comm_paid_percentage=summpaid
            elif 81 <= percentage <= 150:
                sales_percentage=0.1
                comm_percentage=0.1
                comm_paid_percentage=summpaid
            elif percentage >= 150:
                sales_percentage=((lines.employee_id.monthly_target*1.5)*0.1+(margin-(lines.employee_id.monthly_target*1.5))*0.15)
                margin=1
                if summpaid > (lines.employee_id.monthly_target*1.5)*0.1:
                    comm_percentage=((lines.employee_id.monthly_target*1.5)*0.1+(summpaid-(lines.employee_id.monthly_target*1.5))*0.15)
                    comm_paid_percentage=1
                else:
                    comm_percentage=0

        worksheet.write(row+9, col+1, 'COMMISSION', cell_text_format)
        if margin> 0 :
            worksheet.write(row+9, col+2, margin*sales_percentage, cell_text_format_values) 
        else:
            worksheet.write(row+9, col+2, "0", cell_text_format_values) 
        
        worksheet.write(row+10, col+1, 'COMMISSION PAID', cell_text_format)
        if summpaid <=0:
            worksheet.write(row+10, col+2, '0', cell_text_format_values)
        else:
            worksheet.write(row+10, col+2, summpaid, cell_text_format_values)
      
        worksheet.write(row+11, col+1, 'COMMISSION DUE', cell_text_format)
        worksheet.write(row+11, col+2, comm_paid_percentage*comm_percentage, cell_text_format_values)            
