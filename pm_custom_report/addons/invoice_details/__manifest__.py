# -*- encoding: utf-8 -*-
{
	"name": "invoice details",
	"version": "14.0",
	"author": "Ahmed Hegazy , BsHub.",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'account',
		'account_accountant',
		# 'invoice_report',
		'era_tax_partner',
		'customer_id_field',
		'jt_amount_in_words',
		'sale_discount_total',
	],
	"category": "Settings",
	"complexity": "easy",
	"description": """
	This module allow to Adding information in invoices. 
	""",
	"data": [
		# 'security/ir.model.access.csv',
		# 'reports/invoice.xml',
		'reports/securenass_invoice.xml',
		'reports/arabic_invoice.xml',
		'views/views.xml',
		# 'views/res_partner.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
