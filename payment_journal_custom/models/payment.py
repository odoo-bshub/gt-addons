from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountMovePayment(models.Model):
    _inherit = 'account.move'

    def action_post(self):
        self._post(soft=False)
        for line in self.invoice_line_ids:
            if not line.analytic_account_id:
                return {
                    'name': "Warning Message",
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'line.analytic.account',
                    'view_id': self.env.ref('payment_journal_custom.analytic_account_wizard_form').id,
                    'target': 'new',
                }

    # @api.model
    # def create(self, values):
    #     # Add code here
    #     res = super(AccountMovePayment, self).create(values)
    #     for line in res.invoice_line_ids:
    #         if not line.analytic_account_id:
    #             raise UserError('Analytic account should not be empty')
    #     return res

    # def write(self, values):
    #     if self.move_type == 'entry':
    #         if values:
    #             for value in values['line_ids']:
    #                 for v in value:
    #                     if isinstance(v, dict):
    #                         key = 'analytic_account_id'
    #                         if not v[key]:
    #                             raise UserError('Analytic account should not be empty')
    #     else:
    #         if values:
    #             for value in values['invoice_line_ids']:
    #                 for v in value:
    #                     if isinstance(v, dict):
    #                         key = 'analytic_account_id'
    #                         if not v[key]:
    #                             raise UserError('Analytic account should not be empty')
    #
    #     return super(AccountMovePayment, self).write(values)

    new_line_ids = fields.One2many("payment.move.line", 'move_id', string="Payment Journal", )

    @api.onchange('invoice_date', 'invoice_line_ids', 'invoice_payment_term_id')
    def set_payment_journal_custom(self):
        for rec in self:
            rec.new_line_ids = False

            # Grouping Product By Acccount
            product_account = 0
            lst_total_for_products = []
            for product in rec.invoice_line_ids:
                product_account = product.account_id.id
                lst_total_for_products.append(product.price_subtotal)

            # When Due Date Does Not set to payment term
            # Customer Invoice Or Vendor refund
            if rec.move_type == 'out_invoice' or rec.move_type == 'in_refund':
                if not rec.invoice_payment_term_id and rec.line_ids:
                    for line in rec.line_ids:
                        # Without produscts
                        if line.account_id.id != product_account:
                            rec.new_line_ids = [(0, 0, {
                                'account_id': line.account_id.id,
                                'name': line.name,
                                'analytic_tag_ids': line.analytic_tag_ids.ids,
                                'debit': line.debit,
                                'credit': line.credit,
                            })]
                    # Now Adding Credit Values IN Payment Line
                    rec.new_line_ids = [(0, 0, {
                        'account_id': product_account,
                        'debit': 0.0,
                        'credit': sum(lst_total_for_products),
                    })]

                # When Deu Date Set to Payment term
                else:
                    debit_lst = []
                    credit_lst = []
                    if rec.line_ids:
                        for p_line in rec.line_ids:
                            # get debit values in line_ids
                            if p_line.debit:
                                debit_lst.append(p_line)
                            # get credit values in line_ids
                            else:
                                credit_lst.append(p_line)
                    # IN DEBIT LST remove the redundunt data
                    debit_dic = {}
                    for d_value in debit_lst:
                        if d_value.account_id.id in debit_dic:
                            debit_dic[d_value.account_id.id].append(d_value.debit)
                        else:
                            debit_dic[d_value.account_id.id] = [d_value.debit]

                    # Grouping Product By Acccount
                    product_account = 0
                    lst_total_for_products = []
                    for product in rec.invoice_line_ids:
                        product_account = product.account_id.id
                        lst_total_for_products.append(product.price_subtotal)

                    # ADDING IN new_line_ids
                    # debit values
                    for debit_value in debit_dic:
                        rec.new_line_ids = [(0, 0, {
                            'account_id': debit_value,
                            'debit': sum(debit_dic[debit_value]),
                            'credit': 0.0,
                        })]

                    # Credit Values
                    for credit_value in credit_lst:
                        # Credit Values WITHOUT PRODCTS
                        if credit_value.account_id.id != product_account:
                            rec.new_line_ids = [(0, 0, {
                                'account_id': credit_value.account_id.id,
                                'name': credit_value.name,
                                'analytic_tag_ids': credit_value.analytic_tag_ids.ids,
                                'debit': 0.0,
                                'credit': credit_value.credit,
                            })]
                    # Now Adding Credit Values IN Payment Line
                    rec.new_line_ids = [(0, 0, {
                        'account_id': product_account,
                        'debit': 0.0,
                        'credit': sum(lst_total_for_products),
                    })]


            # Customer Credit Note Or Vendor Bill
            elif rec.move_type == 'out_refund' or rec.move_type == 'in_invoice':
                if not rec.invoice_payment_term_id and rec.line_ids:
                    # Grouping Product By Acccount
                    product_account = 0
                    lst_total_for_products = []
                    for product in rec.invoice_line_ids:
                        product_account = product.account_id.id
                        lst_total_for_products.append(product.price_subtotal)

                    for line in rec.line_ids:
                        # Without produscts
                        if line.account_id.id != product_account:
                            rec.new_line_ids = [(0, 0, {
                                'account_id': line.account_id.id,
                                'name': line.name,
                                'analytic_tag_ids': line.analytic_tag_ids.ids,
                                'debit': line.debit,
                                'credit': line.credit,
                            })]
                    # Now Adding Debit Values IN Payment Line
                    rec.new_line_ids = [(0, 0, {
                        'account_id': product_account,
                        'debit': sum(lst_total_for_products),
                        'credit': 0.0,
                    })]
                else:
                    debit_lst = []
                    credit_lst = []
                    if rec.line_ids:
                        for p_line in rec.line_ids:
                            # get debit values in line_ids
                            if p_line.debit:
                                debit_lst.append(p_line)
                            # get credit values in line_ids
                            else:
                                credit_lst.append(p_line)
                    # IN Credit LST remove the redundunt data
                    credit_dic = {}
                    for c_value in credit_lst:
                        if c_value.account_id.id in credit_dic:
                            credit_dic[c_value.account_id.id].append(c_value.credit)
                        else:
                            credit_dic[c_value.account_id.id] = [c_value.credit]

                    # Grouping Product By Acccount
                    product_account = 0
                    lst_total_for_products = []
                    for product in rec.invoice_line_ids:
                        product_account = product.account_id.id
                        lst_total_for_products.append(product.price_subtotal)

                    # ADDING IN new_line_ids
                    # debit values
                    for credit_value in credit_dic:
                        rec.new_line_ids = [(0, 0, {
                            'account_id': credit_value,
                            'credit': sum(credit_dic[credit_value]),
                            'debit': 0.0,
                        })]
                    # Debit Values
                    for debit_value in debit_lst:
                        if debit_value.account_id.id != product_account:
                            # WITHOUT Products
                            rec.new_line_ids = [(0, 0, {
                                'account_id': debit_value.account_id.id,
                                'name': debit_value.name,
                                'analytic_tag_ids': debit_value.analytic_tag_ids.ids,
                                'credit': 0.0,
                                'debit': debit_value.debit,
                            })]
                    # Now Adding Debit Values IN Payment Line
                    rec.new_line_ids = [(0, 0, {
                        'account_id': product_account,
                        'debit': sum(lst_total_for_products),
                        'credit': 0.0,
                    })]


class NewMoveLine(models.Model):
    _name = 'payment.move.line'

    move_id = fields.Many2one('account.move', string='Payment Journal',
                              index=True, required=True, readonly=True,
                              auto_join=True, ondelete="cascade",
                              check_company=True, )

    account_id = fields.Many2one('account.account', string='Account',
                                 index=True, ondelete="cascade",
                                 domain="[('deprecated', '=', False), ('company_id', '=', 'company_id'),('is_off_balance', '=', False)]",
                                 check_company=True,
                                 tracking=True)
    name = fields.Char(string='Label', tracking=True)
    debit = fields.Monetary(string='Debit', default=0.0,
                            )
    credit = fields.Monetary(string='Credit', default=0.0,
                             )

    company_id = fields.Many2one(related='move_id.company_id', store=True, readonly=True,
                                 default=lambda self: self.env.company)
    currency_id = fields.Many2one('res.currency', string='Currency', required=False)
    tax_tag_ids = fields.Many2many(string="Tags",
                                   comodel_name='account.account.tag',
                                   ondelete='restrict',
                                   help="Tags assigned to this line by the tax creating it, if any. It determines its impact on financial reports.",
                                   tracking=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag',
                                        string='Analytic Tags',
                                        store=True, readonly=False,
                                        check_company=True, copy=True)
    partner_id = fields.Many2one('res.partner', string='Partner',
                                 ondelete='restrict')
    product_uom_id = fields.Many2one('uom.uom', string='Unit of Measure',
                                     domain="[('category_id', '=', product_uom_category_id)]")
    product_id = fields.Many2one('product.product', string='Product')
    product_uom_category_id = fields.Many2one('uom.category',
                                              related='product_id.uom_id.category_id')
    tax_ids = fields.Many2many('account.tax', string='Taxes', help="Taxes that apply on the base amount",
                               check_company=True)
    analytic_account_id = fields.Many2one('account.analytic.account',
                                          string='Analytic Account',
                                          index=True,
                                          store=True, readonly=False,
                                          check_company=True, copy=True)

    @api.depends('product_id', 'account_id', 'partner_id', 'date')
    def _compute_analytic_account(self):
        for record in self:
            if not record.exclude_from_invoice_tab or not record.move_id.is_invoice(
                    include_receipts=True):
                rec = self.env['account.analytic.default'].account_get(
                    product_id=record.product_id.id,
                    partner_id=record.partner_id.commercial_partner_id.id or record.move_id.partner_id.commercial_partner_id.id,
                    account_id=record.account_id.id,
                    user_id=record.env.uid,
                    date=record.date,
                    company_id=record.move_id.company_id.id
                )
                if rec:
                    record.analytic_account_id = rec.analytic_id
                    record.analytic_tag_ids = rec.analytic_tag_ids


class Account_Vaalidation_model(models.TransientModel):
    _name = 'line.analytic.account'

    my_text = fields.Text(string="Warning", readonly=True, default='Analytic account should not be empty',store=True)

