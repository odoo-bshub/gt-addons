from odoo import api, fields, models,_
from odoo.exceptions import UserError, ValidationError
from odoo.tools import email_split, float_is_zero


class HR_Expense_Inherit(models.Model):
    _inherit = 'hr.expense'

    @api.onchange('employee_id')
    def on_change_employee_id_cahange_beneficiary(self):
        for rec in self:
            if rec.employee_id:
                rec.beneficiary = rec.employee_id.name


    beneficiary = fields.Char(string="Beneficiary",default='employee_id.name')
    po_num = fields.Char(string="PO #",)

class Payment_Module(models.TransientModel):
    _inherit = 'account.payment.register'

    def action_create_payments(self):
        payments = self._create_payments()
        #start of my code
        #set payment in M2M field in account.move relation with hr.expense.sheet
        journal_entry = self.env['hr.expense.sheet'].search([('id','=',self._context.get('active_id'))]).account_move_id
        journal_entry.custom_payment_ids =[(4,payments.id)]
        #End Of My Code
        if self._context.get('dont_redirect_to_payments'):
            return True

        action = {
            'name': _('Payments'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment',
            'context': {'create': False},
        }
        if len(payments) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': payments.id,
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('id', 'in', payments.ids)],
            })
        return action

class Account_Move_Inherit(models.Model):
    _inherit = 'account.move'

    custom_payment_ids = fields.Many2many(comodel_name="account.payment", relation="", column1="", column2="", string="Payments",readonly=True )

class Project_project_Inherit(models.Model):
    _inherit = 'project.project'

    def get_expenses_by_project_name(self):
        for rec in self:
            ex_dic= {}
            lst_amount =[]
            expenses = self.env['hr.expense'].search([('analytic_account_id','=',self.analytic_account_id.id)])
            for ex in expenses:
                #check if expense is paid or not
                    if ex.id in ex_dic:
                        ex_dic[ex.id]['main_project'] = ex.analytic_account_id.code
                        ex_dic[ex.id]['description_expense'] = ex.name
                        ex_dic[ex.id]['amount'] = ex.total_amount
                        ex_dic[ex.id]['beneficiary'] = ex.beneficiary
                        ex_dic[ex.id]['invoice_num'] = ex.reference
                        ex_dic[ex.id]['payments'] = [pay.name for pay in ex.sheet_id.account_move_id.custom_payment_ids]
                        ex_dic[ex.id]['po_num'] = ex.po_num
                    else:
                        ex_dic[ex.id] = {'main_project': ex.analytic_account_id.code,
                                                       'description_expense': ex.name,
                                                       'amount': ex.total_amount ,
                                                       'beneficiary': ex.beneficiary,
                                                       'invoice_num':ex.reference,
                                                       'po_num':ex.po_num,
                                                       'payments': [pay.name for pay in ex.sheet_id.account_move_id.custom_payment_ids],
                                                       }
            #get total of ad_hoc
            for expens in expenses:
                lst_amount.append(expens.total_amount)
            ad_hoc_total = sum(lst_amount)


            return ex_dic , ad_hoc_total
