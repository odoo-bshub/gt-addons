from odoo import api, fields, models
import datetime
from dateutil import relativedelta



class Crm_enhancement(models.Model):
    _inherit = 'crm.lead'

    # @api.model
    # def create(self, values):
    #     res = super(Crm_enhancement, self).create(values)
    #     # project = self.env['project.project'].sudo().create({
    #     #     'name':res.name,
    #     #     'partner_id':res.partner_id.id,
    #     #     'opp_enhance_id':res.id
    #     # })
    #     # res.project_created_from_crm_id = project.id
    #     # res.project_created_from_crm_id.opportunity_id = res.id
    #     # res.project_count_custom = 1
    #
    #     #create lead code
    #     if res.partner_id:
    #         if res.partner_id.customer_code:
    #             res.crm_code_custom = res.partner_id.customer_code + '/OPP' + str(res.partner_id.crm_counter).zfill(2)
    #             res.partner_id.crm_counter += 1
    #
    #         if res.partner_id.supplier_code:
    #             res.project_code_custom = res.partner_id.supplier_code + '/OPP' + str(res.partner_id.crm_counter).zfill(2)
    #             res.partner_id.crm_counter += 1
    #     return res
    def create_project_from_crm(self):
        for rec in self:
            # rec.project_count_custom += 1

            project = self.env['project.project'].sudo().create({
                'name': rec.name,
                'partner_id': rec.partner_id.id,
                'opp_enhance_id': rec.id,
                'custom_start_date': datetime.datetime.today(),
                'custom_end_date': datetime.datetime.today() + relativedelta.relativedelta(days=30)
            })
            rec.project_created_from_crm_id = project.id
            rec.project_created_from_crm_id.opportunity_id = rec.id
            # rec.project_count_custom = 1
            rec.create_project_is_done = True
            # rec.update({
            #     'stage_id':self.next_stage_id
            # })
            # rec.env['sale.order'].search([('','',)])

    def open_related_project(self):
        self.project_count_custom = self.env['project.project'].search_count([('opportunity_id','=',self.id)])
        action = self.env.ref('project.open_view_project_all').read()[0]
        action['context'] = {}
        action['domain'] = [('opportunity_id', '=', self.id)]
        return action

    project_created_from_crm_id = fields.Many2one(comodel_name="project.project", string='Related Project',
                                                  readonly=True)
    crm_code_custom = fields.Char(string="Lead Code", readonly=True, )
    sale_counter = fields.Integer(string="Sale Counter", default=1, readonly=True)
    project_count_custom = fields.Integer(string="Project Count", default=0,compute=open_related_project)
    create_project_is_done = fields.Boolean(string="",  )
