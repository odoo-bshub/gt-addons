from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError, RedirectWarning


class EmployeeForm(models.Model):
    _inherit = 'hr.employee'

    iqama_start_date = fields.Date(string="Iqama Start Date", required=False, )
    iqama_end_date = fields.Date(string="Iqama Start Date", required=False, )
    employee_grad = fields.Char(string="Employee Grad", required=False, )


class EmployeeDocuments(models.Model):
    _inherit = 'hr.employee.document'

    file_type = fields.Selection(string="File Type", selection=[('original', 'Original'), ('copy', 'Copy'), ], required=False, )
    is_required = fields.Boolean(string="Required",related="document_type.is_required")

    @api.constrains('document_type')
    @api.onchange('document_type')
    def set_constrain_required_doc_type(self):
        for rec in self:
            for line in rec.document_type:
                if line.is_required == False :
                    return {
                        'warning': {
                            'message': _(
                                'Employee is missed for Required Documents.')
                        }
                    }


class DocumentHrType(models.Model):
    _inherit = 'document.type'

    is_required = fields.Boolean(string="Required",)
    specific_date = fields.Integer(string="Days", required=False, )


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    # @api.constrains('employee_id')
    # # @api.onchange('employee_id')
    # def set_constrain_required_doc(self):
    #     for rec in self:
    #         list= self.env['hr.employee.document'].search([('employee_ref','=',rec.employee_id.id)])
    #         if not list:
    #             raise UserError(_('Employee is missed for Required Documents'))

    def compute_sheet(self):
        list = self.env['hr.employee.document'].search([('employee_ref', '=', self.employee_id.id)])
        if not list:
            raise UserError(_('Employee is missed for Required Documents'))
        else:
            for payslip in self.filtered(lambda slip: slip.state in ['draft', 'verify']):
                number = payslip.number or self.env['ir.sequence'].next_by_code('salary.slip')
                # delete old payslip lines
                payslip.line_ids.unlink()
                lines = [(0, 0, line) for line in payslip._get_payslip_lines()]
                payslip.write({'line_ids': lines, 'number': number, 'state': 'verify', 'compute_date': fields.Date.today()})
            return True




