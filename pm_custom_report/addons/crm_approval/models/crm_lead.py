from odoo import api, fields, models


class CRM_lead_crm_approval(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def create(self, values):
        res = super(CRM_lead_crm_approval, self).create(values)
        template_id = self.env.ref('crm_approval.template_mail_for_create_opportunity').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(res.id, force_send=True)
        return res

    def write(self, values):
        lst = ['is_appear_button_sales_manager_approve', 'project_count_custom', 'is_created_crm',
               'project_created_from_crm_id', 'quotation_count', 'is_appear_button_sales_manager_approve','is_approved_data_from_sales_admin','stage_id']
        if self.stage_id.id == 1:
            for v in values:
                if v not in lst:
                    print(v)
                    template_id = self.env.ref('crm_approval.template_mail_for_after_review_data').id
                    template = self.env['mail.template'].browse(template_id)
                    template.send_mail(self.id, force_send=True)
                    break
        return super(CRM_lead_crm_approval, self).write(values)

    def get_email_with_recipient_approval_in_crm_create(self):
        email_list = []
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group('sales_team.group_sale_manager'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
        # print(email_list)

        return ",".join(email_list)

    # def action_data_correct_in_crm(self):
    #     for rec in self:
    #         rec.is_approved_data_from_sales_admin = True

    def action_data_review_request_in_crm(self):
        for rec in self:
            template_id = self.env.ref('crm_approval.template_mail_for_review_crm_data').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(rec.id, force_send=True)

    is_approved_data_from_sales_admin = fields.Boolean(string="- Approved Data", readonly=True)
