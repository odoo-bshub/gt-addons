from odoo import api, fields, models

class Sale_Order_Inherit(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, values):
        # Add code here
        res = super(Sale_Order_Inherit, self).create(values)
        project = self.env['project.project'].search([('analytic_account_id','=',res.analytic_account_id.id)])
        project.sudo().write({
            'sale_order_amount': res.amount_total
        })
        return res

    _sql_constraints = [
        ('unique_analytic_account_id', 'unique (analytic_account_id)', 'Analytic Account already used for another Quotation!')
    ]

    analytic_account_id = fields.Many2one(
        'account.analytic.account', 'Analytic Account',
        readonly=True, copy=False, check_company=True,  # Unrequired company
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="The analytic account related to a sales order.",)