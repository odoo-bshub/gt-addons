""" Crm Lead """

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models


class CrmLead(models.Model):
    """ inherit Crm Lead """
    _inherit = 'crm.lead'

    # @api.model
    def get_email_to_missed(self):
        email_list = []
        user_group = self.env.ref("sales_team.group_sale_manager")
        email_list = [
            usr.partner_id.email for usr in user_group.users if
            usr.partner_id.email]
        email_list.append(self.user_id.partner_id.email)
        print(email_list)
        return ",".join(email_list)

    @api.depends('message_ids.date')
    def _compute_last_message_date(self):
        for lead in self:
            last_message_date = lead.message_ids.filtered(
                lambda message: not message.mail_activity_type_id)
            if last_message_date:
                lead.last_message_date = last_message_date[0].date

    # pylint: disable=protected-access
    @api.model
    def check_missed_lead(self):
        """
        This method is called from a cron job.
        """
        records = self.search([
            ('type', '=', 'opportunity'),
            ('missed_email_sent', '=', False),
            ('probability', '!=', 100.0),
            ('user_id', '!=', False),
            ('active', '=', True),
        ])
        for rec in records:
            if rec._is_missed_opportunity():
                rec._opportunity_missed_email_send()

    def _get_latest_missed_date(self):
        """
        Check for latest date that used to compute missed days

        :return: latest date for action made in opportunity
        :type: Datetime
        """
        self.ensure_one()
        last_activity_done_date = self.last_activity_done_date
        last_message_date = self.last_message_date
        if last_activity_done_date and last_message_date \
                and last_activity_done_date > last_message_date:
            done_date = last_activity_done_date
        else:
            done_date = last_message_date or last_activity_done_date
        return done_date

    def _is_missed_opportunity(self):
        """
        compute if opportunity is missed based on configuration condition
        :return: if lead is missed or not
        :type:Boolean
        """
        for opportunity in self:
            is_missed_lead = False
            missed_delay = opportunity.stage_id.missed_delay
            missed_datetime = datetime.utcnow() - relativedelta(
                days=missed_delay or 1.0)
            opportunity_date = opportunity._get_latest_missed_date()
            if opportunity_date and opportunity_date <= missed_datetime:
                is_missed_lead = True
            return is_missed_lead

    def _opportunity_missed_email_send(self):
        """
        Send the missed email on the current records,
        making sure that marking the missed email as sent.
        """
        opportunities = self.env['crm.lead']
        for opportunity in self:
            template = opportunity.stage_id.missed_mail_template_id
            if template:
                user_id = opportunity.user_id
                for user in opportunity.stage_id.user_ids:
                    template_ctx = {'lang': user.lang,
                                    'email_to': user.email}
                    template.with_context(**template_ctx).send_mail(
                        opportunity.id)
                if user_id:
                    template_ctx = {'lang': user_id.lang,
                                    'email_to': user_id.email}
                    template.with_context(**template_ctx).send_mail(
                        opportunity.id)
                opportunities |= opportunity
        opportunities.write({'missed_email_sent': True})

    missed_email_sent = fields.Boolean(
        'Missed email already sent', copy=False
    )
    last_activity_done_date = fields.Datetime()
    last_message_date = fields.Datetime(
        compute="_compute_last_message_date", store=True
    )
