from odoo import api, fields, models


class Brands_Configuration(models.Model):
    _name = 'brand.config'
    _rec_name = 'name'

    name = fields.Char(string="Brand Name", required=True, )