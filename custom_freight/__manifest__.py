# -*- coding: utf-8 -*-
{
    'name': "Custom Freight",

    'summary': """
        Custom module to add additional features to Freight module. """,

    'description': """
        Custom module to add additional features to Freight module, like adding receipts and make
        additional invoice templates.
    """,

    'author': "Makdzone",
    'website': "http://makdzone.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'freight',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'freight', 'account_accountant', 'hr','report_xlsx'],

    # always loaded
    'data': [
        'data/hr_salary_rule_data.xml',
        'security/ir.model.access.csv',
        'reports/template.xml',
        'reports/template_receipt.xml',
        'reports/releasing_letter.xml',
        'reports/air_delivery_order.xml',
        'reports/shipment_arrival_notice.xml',
        'reports/bill_of_lading.xml',
        'reports/custom_land_report.xml',
        'reports/operation.xml',
        'reports/report.xml',
        'views/views.xml',
        'views/receipt_customer_vendor.xml',
        'views/commission.xml',
        'views/commission_details.xml',
        'views/salary_settings.xml',
        'views/sales_commession_report.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
