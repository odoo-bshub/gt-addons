# -*- encoding: utf-8 -*-
{
	"name": "Cycle Enhancements",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm','purchase','project','sale_management','kind_field_in_contact','sale_crm','account'
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'views/crm_form.xml',
		'views/company_form.xml',
		'views/res_pertner_inherit.xml',
		'views/project_inherit.xml',
		'views/sale_order_inherit.xml',
		'views/account_move.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
