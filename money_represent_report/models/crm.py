from odoo import api, fields, models
from num2words import num2words
from odoo.exceptions import ValidationError
from deep_translator import GoogleTranslator


class ProductNew(models.Model):
    _inherit = 'product.product'

    def name_get(self):
        res = []
        for rec in self:
            name = rec.default_code
            res.append((rec.id, name))
        return res

class Crm_Inherit(models.Model):
    _inherit = 'crm.lead'

    def action_all_financial_report(self):
        self.ensure_one()
        return {
            'name': 'Financial Proposal',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'fin.pro',
            'domain': [('opportunity_id', '=',
                        self.id)],
            'context': {
                'default_opportunity_id': self.id, 'default_partner_id': self.partner_id.id
            }
        }

    def get_count_fb(self):
        for rec in self:
            count = rec.env['fin.pro'].search_count([('opportunity_id', '=',
                                                   rec.id)])
            rec.fp_count = count

    fp_count = fields.Integer(string="", compute=get_count_fb)


class Financial_Report_proposal(models.Model):
    _name = 'fin.pro'
    @api.model
    def create(self, values):
        res = super(Financial_Report_proposal, self).create(values)
        all_fin_related_to_opp = self.search([('opportunity_id', '=', res.opportunity_id.id)])
        res.name = len(all_fin_related_to_opp)

        cost_sheet = self.env['cost.calc'].search([('opportunity_id','=',res.opportunity_id.id)])
        if cost_sheet :
            res.cost_calc_id = cost_sheet.id
            for line in cost_sheet.cost_ids:
                res.quant_price_ids = [(0,0,{
                    # 'product_id':line.product_id.default_code,
                    'product_id':line.product_id.id,
                    'description':line.description,
                    'quant':line.qty,
                    # 'total_without_compute_quantity':line.unit_list,
                    'total_without_compute_quantity':line.unit_sar,
                })]

                res.pay_table_ids = False
                for line_quant in res.quant_price_ids:
                    res.pay_table_ids = [(0, 0, {
                            'service_description': line_quant.product_id.name,
                            'quant_table_id': line_quant.id,
                        })]


                    # compute cost of payment pable
                    # compute percentage of line
                    if line_quant.total and res.total_after_tax_applied:
                        for line_pay in res.pay_table_ids:
                            if line_quant == line_pay.quant_table_id:
                                line_pay.update({
                                    'cost': (line_quant.total * 0.15) + line_quant.total,

                                })
                                line_pay.update({
                                    'percentage': (line_pay.cost / res.total_after_tax_applied) * 100,
                                })
        else:
            raise ValidationError('Please Upload/Create Cost sheet for this opportunity before creating Financial Proposal')
        return res

    @api.depends('total_after_tax_applied')
    def amount_to_words(self):
        # if self.env.company.text_amount_language_currency:
        #     self.text_amount = num2words(self.total_after_tax_applied, to='currency',
        #                                  lang=self.env.company.text_amount_language_currency)
        for rec in self:
            rec.text_amount_english = str(rec.env.company.currency_id.amount_to_text(rec.total_after_tax_applied)) + ' only'

    @api.onchange('text_amount_english')
    @api.depends('text_amount_english')
    def amount_to_words_to_arabic(self):
        for rec in self:
            if rec.text_amount_english:
                rec.text_amount = GoogleTranslator(source='auto', target='ar').translate(rec.text_amount_english)
            else:
                rec.text_amount = " "

    @api.depends('quant_price_ids')
    def get_total_of_quantity_table(self):
        for rec in self:
            totals = []
            if rec.quant_price_ids:
                for line in rec.quant_price_ids:
                    if line.total:
                        totals.append(line.total)
                rec.total_of_table = sum(totals)
            else:
                rec.total_of_table = 0.0

    @api.depends('total_of_table')
    def get_value_of_tax(self):
        for rec in self:
            if rec.total_of_table:
                rec.tax_applied = rec.total_of_table * 0.15
            else:
                rec.tax_applied = 0.0

    @api.depends('total_of_table', 'tax_applied')
    def get_total_plus_tax(self):
        for rec in self:
            if rec.total_of_table and rec.tax_applied:
                rec.total_after_tax_applied = rec.total_of_table + rec.tax_applied
            else:
                rec.total_after_tax_applied = 0.0

    def count_financial_proposal(self):
        for rec in self:
            if rec.financial_pro_count or not rec.financial_pro_count:
                rec.financial_pro_count = self.env['fin.pro'].search_count([('opportunity_id', '=', rec.id)])
            else:
                rec.financial_pro_count = 0

    @api.onchange('quant_price_ids')
    @api.depends('quant_price_ids')
    def fill_values_of_pay_table_from_quantity_table(self):
        for rec in self:
            rec.pay_table_ids = False
            if rec.quant_price_ids:
                for line in rec.quant_price_ids:
                    rec.write({
                        'pay_table_ids': [(0, 0, {
                            'service_description': line.product_id.name,
                            'quant_table_id': line.id,
                        })]
                    })

                    # compute cost of payment pable
                    # compute percentage of line
                    if line.total and rec.total_after_tax_applied:
                        for line_pay in rec.pay_table_ids:
                            if line == line_pay.quant_table_id:
                                line_pay.update({
                                    'cost': (line.total * 0.15) + line.total,

                                })
                                line_pay.update({
                                    'percentage': (line_pay.cost / rec.total_after_tax_applied) * 100,
                                })

    def get_default_for_notes(self):
        return """
            <p>* Delivery Period: within 10-12 weeks</p> 
            <p>* Delivery Term: onsite </p>
            <p>* Delivery Destination: Riyadh </p>
            <p>* Currency: All prices are in SAR. </p>
            <p>* Payment Method: as per payment policy of National Anti-corruption Commission </p>
            <p>* Warranty & Support: 36 months.</p>
            """

    name = fields.Float(string="Code", readonly=True, )
    opportunity_id = fields.Many2one(comodel_name="crm.lead", readonly=True)
    date = fields.Date(string="Date", required=False, )
    partner_id = fields.Many2one(comodel_name="res.partner", string="Customer", readonly=True, )
    # financial_pro_count = fields.Integer(string="", compute=count_financial_proposal, )
    quant_price_ids = fields.One2many(comodel_name="quant.price", inverse_name="fin_pro_id",readonly=True )
    pay_table_ids = fields.One2many(comodel_name="pay.table", inverse_name="fin_pro_id", readonly=True)

    total_of_table = fields.Float(string="Total", compute=get_total_of_quantity_table, store=True)
    tax_applied = fields.Float(string="Tax 15%", compute=get_value_of_tax, store=True)
    total_after_tax_applied = fields.Float(string="Total after Tax", compute=get_total_plus_tax, store=True)
    text_amount = fields.Char(string="إجمالي المبلغ", required=False, compute="amount_to_words_to_arabic", store=True)
    text_amount_english = fields.Char(string="Total", required=False, compute="amount_to_words", store=True)

    conditions_terms_no_of_days = fields.Integer(string="Condition and Terms(Days)", )
    notes_in_fp = fields.Html(string="Notes", default=get_default_for_notes)
    cost_calc_id = fields.Many2one(comodel_name="cost.calc", string="Cost",readonly=True)



class Quantities_Prices(models.Model):
    _name = 'quant.price'

    @api.depends('quant', 'total_without_compute_quantity')
    @api.onchange('quant', 'total_without_compute_quantity')
    def get_total_by_quantity(self):
        for rec in self:
            if rec.quant and rec.total_without_compute_quantity:
                rec.total = rec.quant * rec.total_without_compute_quantity
            else:
                rec.total = 0.0

    fin_pro_id = fields.Many2one(comodel_name="fin.pro", )

    product_id = fields.Many2one(comodel_name="product.product", string="Part NO.", )
    description = fields.Char(string="Description", required=False, )
    uom_id = fields.Many2one(comodel_name="uom.uom", string="Unit of measure")
    quant = fields.Integer(string="Quantity", )
    total_without_compute_quantity = fields.Float(string="Price(SAR)", )
    total = fields.Float(string="Total(SAR)", compute=get_total_by_quantity, store=True)


class Payments_Table(models.Model):
    _name = 'pay.table'

    fin_pro_id = fields.Many2one(comodel_name="fin.pro", )

    service_description = fields.Char(string="Service Description", store=True)
    percentage = fields.Float(string="Percent.%", store=True)
    cost = fields.Float(string="Cost(SAR)", store=True)
    quant_table_id = fields.Many2one(comodel_name="quant.price", )
