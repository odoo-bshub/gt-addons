from odoo import api, models, _, fields
from odoo.exceptions import UserError, ValidationError


class ProjectCustom(models.Model):
    _inherit = 'project.project'

    @api.onchange('project_status')
    @api.depends('project_status')
    def get_constrain_initiated_stage(self):
        for rec in self:
            if rec.is_initiated or not rec.is_initiated:
                if self.charter == True and self.ho_check == True and self.kick == True and self.pm_tool == True \
                        and self.system_engineer == True and self.billing == True and self.request_supplier_po == True:
                    rec.is_initiated = True

    is_initiated = fields.Boolean(string="", compute=get_constrain_initiated_stage)

    @api.onchange('project_status')
    @api.depends('project_status')
    def get_constrain_planning_stage(self):
        for rec in self:
            if rec.is_planning or not rec.is_planning:
                if self.pip == True and self.supplier_po == True and self.customer_kick == True and self.progress_report == True:
                    rec.is_planning = True

    is_planning = fields.Boolean(string="", compute=get_constrain_planning_stage)

    @api.onchange('project_status')
    @api.depends('project_status')
    def get_constrain_execution_stage(self):
        for rec in self:
            if rec.is_execution or not rec.is_execution:
                if self.item_delivery == True and self.ps_complete == True and self.updated_progress_report == True and self.time_sheet == True:
                    rec.is_execution = True

    is_execution = fields.Boolean(string="", compute=get_constrain_execution_stage)

    @api.onchange('project_status')
    @api.depends('project_status')
    def get_constrain_finance_stage(self):
        for rec in self:
            if rec.is_finance or not rec.is_finance:
                if self.sign_off_project == True and self.invoicing == True and self.project_doc == True and self.project_closure == True:
                    rec.is_finance = True

    is_finance = fields.Boolean(string="", compute=get_constrain_finance_stage)

    def set_project_closure(self):
        for rec in self:
            rec.write({
                'project_status': 5,
                'project_closure' : True,
            })


    @api.constrains('project_status')
    @api.onchange('project_status')
    def _onchange_stage_id_check(self):
        """
        Check that the project can change from one stage to another
        depending on if it has data of the requirements that each one requires.

        :raises: UserError
        """
        if self.project_status.initiate:
            if self.is_initiated != True:
                err_msg = _("""
                You can not change status if the Project lacks Stage Requirements:
                - Must select All check Box is Selected .
                            """)
                raise UserError(_(err_msg))

        if self.project_status.planning:
            if self.is_planning != True:
                err_msg = _("""
                You can not change status if the Project lacks Stage Requirements:
                - Must select All check Box is Selected .
                            """)
                raise UserError(_(err_msg))

        if self.project_status.execution:
            if self.is_execution != True :
                err_msg_amount = _("""
                You can not change status if the Project lacks Stage Requirements:
                - Must select All check Box is Selected .
                                   """)
                raise UserError(_(err_msg_amount))

        if self.project_status.finance:
            if self.is_finance != True :
                err_msg_amount = _("""
                You can not change status if the Project lacks Stage Requirements:
                - Must select All check Box is Selected .
                                   """)
                raise UserError(_(err_msg_amount))

    """" fields required for Initiation stage"""

    charter = fields.Boolean(string="- Project Charter ", )
    ho_check = fields.Boolean(string="- HO check ", )
    kick = fields.Boolean(string="- Internal Kick off  ", )
    pm_tool = fields.Boolean(string="- PM tool ", )
    system_engineer = fields.Boolean(string="- Assign System Engineer ", )
    billing = fields.Boolean(string="- CashFlow ", )
    request_supplier_po = fields.Boolean(string="- Request for Supplier PO ", )

    """" fields required for planning stage"""

    pip = fields.Boolean(string="- PIP", )
    supplier_po = fields.Boolean(string="- Supplier PO ", )
    customer_kick = fields.Boolean(string="- Customer Kick-off", )
    progress_report = fields.Boolean(string="- Progress Report", )

    """" fields required for execution stage"""

    item_delivery = fields.Boolean(string="- Item delivered", )
    ps_complete = fields.Boolean(string="- PS completed  ", )
    updated_progress_report = fields.Boolean(string="- Updated Progress Report ", )
    time_sheet = fields.Boolean(string="- Project Time Sheet", )

    """" fields required for finance stage"""

    sign_off_project = fields.Boolean(string="- Project Sign-off", )
    invoicing = fields.Boolean(string="- Project Invoicing", )
    project_doc = fields.Boolean(string="- Updated Project Documents", )
    project_closure = fields.Boolean(string="- Project Closure", )

    cash_in_op1 = fields.Float(string="Cash IN", required=False, )
    cash_in_op2 = fields.Float(string="Chas IN", required=False, )
    cash_in_op3 = fields.Float(string="Chas IN", required=False, )
    cash_in_op4 = fields.Float(string="Cash IN", required=False, )
    cash_in_op5 = fields.Float(string="Chas IN", required=False, )
    cash_in_op6 = fields.Float(string="Chas IN", required=False, )
    cash_in_op7 = fields.Float(string="Cash IN", required=False, )
    cash_in_op8 = fields.Float(string="Chas IN", required=False, )
    cash_in_op9 = fields.Float(string="Chas IN", required=False, )
    cash_in_op10 = fields.Float(string="Chas IN", required=False, )

    cash_in_date1 = fields.Date(string="Date", required=False, )
    cash_in_date2 = fields.Date(string="Date", required=False, )
    cash_in_date3 = fields.Date(string="Date", required=False, )
    cash_in_date4 = fields.Date(string="Date", required=False, )
    cash_in_date5 = fields.Date(string="Date", required=False, )
    cash_in_date6 = fields.Date(string="Date", required=False, )
    cash_in_date7 = fields.Date(string="Date", required=False, )
    cash_in_date8 = fields.Date(string="Date", required=False, )
    cash_in_date9 = fields.Date(string="Date", required=False, )
    cash_in_date10 = fields.Date(string="Date", required=False, )

