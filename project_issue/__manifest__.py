# -*- coding: utf-8 -*-
{
    'name': "Project Issue",

    'summary': """
        """,

    'description': """
 	Adding a field called pipeline to link project with a specific opportunity.   
	This field should include all of pipelines not linked to a specific project.
	Project should include all of pipeline's attachments.
	Project can be linked only with only one pipeline.
	related opportunity  in all task related to project,
 	handover task related with project,
 	project should only related with handover task that is related to opportunity,


    """,

    'author': "",
    'website': "",

    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','crm','project','opportunity_stage_requeriments','abs_opportunity_task'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
   
}
