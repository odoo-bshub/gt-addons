# -*- coding: utf-8 -*-
from . import crm
from . import company
from . import res_partner_inherit
from . import project_inherit
from . import sale_inherit
from . import account_move

