from odoo import api, fields, models, _ ,tools
from odoo.modules import get_module_resource
import base64
from odoo.exceptions import UserError, ValidationError
from datetime import date, datetime

x='iVBORw0KGgoAAAANSUhEUgAAAHMAAAAnCAYAAAA8XHcHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABahJREFUeNrsW9tx2kAUFYz+QyoAKoD85seigsCkgEAFmAoMFYArABeQEa4A+Se/kSuwXEGUCpJd5ois1/fuQ7wz2hkNIGkfuuc+zr0rasGR24/oa1d8zMUReXTLxZGK41Ecq8/J9zyomrXVTzBH7AmkbA30kUrwIhTitoLqMsBs7dlfAjsXgC4ruM4P5qHasLLQ84M5OuBYdwLQRgUb3WqnmAQkyBUEeW9TWiLTZyYI0bSC7kxg7qEAGwLQTIDZrqC7opgpAJOpyYoiVALoVgXd9RGgB4Mrrto1gQnrzCsw/5/UJCXONSvorjvPPGQh4r9sYSUClk1vK09g0zO4/ArMPQVKxcenE0w/R65bxOiLT4fqFwxki8kzt7nmCZYQXZtbDy/UGmUN9o65RbLb9YnjclaBWQ7EcWAu/d0fe39TrCVyYNQVmIzwhrBEmztLXeuyAKSljJmjf+LQXY/TzyXnzDBn6ti/Lz6+iONR9FmbuAP1HOGVgBjAtY4chCktu2+4R4J6L46FwcKbrpaJ2H6HORvMPRLUB5MiYu0xfsrtvh7kMqZIoLguP1Zg2tswUDsTiH2wRRcQcyx4YXHRSxOIDIkaUFYjxttoBKhdCEyb8w6hwacA0qOUSIw3NfAEm3xG0pJrJwYxwoIjxy4ziwWZdldcAf2kjy/G/KWOJ67XCCA3QbmyIgmoGDP2VEYd0E/hhYL4xn2UALJ4GSxRYmBHyRtVxjpX3TfcpjoeFWMpIAuW/QQlkePcEO63q8/JxGl13ESJ201iTPl9XLtGEBWh/9QeKoPLSTzB37lRrHmjrklcU8G+BRhqW2DdObPOmADrjesW9/1h5DEhrJjyDEn9WCAi7mwcgUzgekYuQKItNVBSuEyWrSI+DohLfaZYQDFZXRHkmidcKMDz9IL3uz9jQyq0UyJqXJyb6UWO+oFBbOEtOl8Qe44pg8qCI80V9VzyT8yjz3WjfO9YmOxaAUYKfOUwp7x/YnCrutVmqjdgxlwfJTVR6PnQsUsCt5RIlwEm90FquGP/sfZ74llIeNSUoWEo3aWEdX8sISZdgSJDKnRfZvzwxCDuYpoC4q7iI34/2zQdcU/X5G/i/DePpTccCwZ5oSR41lZwnNa1AO9SpSpnmSVBnBVAMeRlGwfFNVvFJHI8V+a5upRVEqTo0C0irN/WdMV6qvuCiJj44ghkYYlt1eJACjhXsrG8sNU5gjBzxkKeDqkslPURz+rKHXQ5ZOEpLJEJ4FMxbpMYU1prLMtZTBykYtq+hfcHZuzsiNaY7jnnOy8SWkC0bUd5g6gTFyyqSyyUSqzJMXyYsKXdWJisaj2+G+S6DF8dUyEnMKVrDi0g2rajVDdlrJ9ylF3MNWDipyw2vzrskniREtSFSWqvj2WIXbnPW/VIpTjL7DgqUGCoUm371HUQwTBfoEkNFxBRzViUMQUlqSa1uRA+Ecd2TNZDqDGqMTGeU1fgFiNU3Y33Xf/zAsHPmXzXmgo5KvA/MPcEcbrvZjEsgHOpS41lvsvXGM3XharvqtxYYlBmISVzR3YcE/LMmHkzR1mSrrkOQZwFRA1QGWcXTE4YF5YAjc4IwKeUtUiBorQ4tFg4uyENZUuJMLBk5myghsvtrKRMruhKfsj91jD49zqhDUTbhu4hAJ1AmyPCrcSKOx4ReZ9UxrHMU/FwRZJPCTMhYl7HkiJMiDmHADXBnL9h8bZ/vT1zuWJZJlu42cY5LNHQBkzciAp3C+scMVYcgbz1GSDXAV1sN6YImHNhcHsF648cjOOQacmuSlVncrRzgLhjuAAqJxhkqrnlkUeOWezIDxyeJ6d2b1A7nnkWI2aElWcmd1kyX92COVEEcjYQCUI00NbVY+JsGxbDaXXx7lDbkv8+Kt+NxQ7MuTLMmUCubdw/U55lrSjlg3I+8XhrXl3frpL2V4ABALGJxs9eRBqlAAAAAElFTkSuQmCC'
class Freight_Operations(models.Model):
    _inherit = 'freight.operation'

    datetime = fields.Datetime(string="Date", required=True, )

    image = fields.Binary("Photo",copy=False, attachment=True, max_width=1024, max_height=1024 ,default=x,compute='image_show')
    def image_show (self):
        self.image=x

    def _compute_customer_name(self):
        for rec in self:
            if rec.direction == 'export':
                rec.customer = rec.shipper_id.name
            elif rec.direction == 'import':
                rec.customer = rec.consignee_id.name

    def get_date_from_datetime(self):
         t_date = ""
         for rec in self:
             for route in rec.freight_routes:
                 if route[0].ata:
                    t_date = datetime.strptime(str(route[0].ata), "%Y-%m-%d %H:%M:%S").date()
         return  t_date
    # Air Delivery Order Report Sequence

    @api.model
    def create(self, values):
        res = super(Freight_Operations, self).create(values)
        res.create_analytic_account_per_operation()
        return res

    @api.onchange('freight_packages')
    def onchange_package_not_more_than_one(self):
        for rec in self:
            if rec.transport != 'ocean':
                if len(rec.freight_packages) > 1:
                    raise ValidationError(
                        'Maximum number of packages allowed is one.')

    def create_analytic_account_per_operation(self):
        analytic_account = self.env['account.analytic.account'].create({
            'name': self.name
        })

        self.custom_analytic_acc_id = analytic_account.id

    @api.depends('freight_services')
    def compute_total_amount(self):
        for order in self:
            invoices = self.env['account.move'].sudo().search(
                [('freight_operation_id', '=', order.id), ('type', 'in', ['out_invoice', 'out_receipt']), ('state', '=', 'posted')])
            invoice_amount = 0.0
            bill_amount = 0.0
            invoice_residual = 0.0
            bills_residual = 0.0
            invoice_paid_amount = 0.0
            bills_paid_amount = 0.0
            # for invoice in invoices:
            #     for line in invoice.line_ids:
            #         invoice_amount += line.amount_total
            #         invoice_residual += invoice.amount_residual
            #         if invoice.invoice_payment_state == 'paid':
            #             invoice_paid_amount += invoice.amount_total
            #         if invoice.invoice_payment_state == 'not_paid':
            #             paid = invoice.amount_total - invoice.amount_residual
            #             invoice_paid_amount += abs(paid)
            for inv in invoices:
                if (inv.type == 'out_invoice' or inv.type == 'out_receipt') and inv.state == 'posted':
                    invoice_amount += abs(inv.amount_total_signed)
                    invoice_residual += abs(inv.amount_residual_signed)
                    if inv.invoice_payment_state == 'paid':
                        invoice_paid_amount += abs(inv.amount_total_signed)
                    if inv.invoice_payment_state == 'not_paid':
                        paid = abs(inv.amount_total_signed) - \
                            abs(inv.amount_residual_signed)
                        invoice_paid_amount += abs(paid)
                else:
                    bill_amount += abs(inv.amount_total_signed)
                    bills_residual += abs(inv.amount_residual_signed)
                    if inv.invoice_payment_state == 'paid':
                        bills_paid_amount += abs(inv.amount_total_signed)
                    if inv.invoice_payment_state == 'not_paid':
                        paid = abs(inv.amount_total_signed) - \
                            abs(inv.amount_residual_signed)
                        bills_paid_amount += abs(paid)

            # bills = self.env['account.move'].sudo().search(
            #     [('freight_operation_id', '=', order.id), ('type', 'in', ['in_invoice','in_receipt']), ('state', '=', 'posted')])
            # for bill in bills:
            #     bill_amount += bill.amount_total
            #     bills_residual += bill.amount_residual
            #     if bill.invoice_payment_state == 'paid':
            #         bills_paid_amount += bill.amount_total
            #     if bill.invoice_payment_state == 'not_paid':
            #         paid = bill.amount_total - bill.amount_residual
            #         bills_paid_amount += abs(paid)

            # for bill in self.invoice_created_ids:
            #     if (bill.type == 'in_invoice' or bill.type == 'in_receipt') and bill.state == 'posted':
            #         bill_amount += abs(bill.amount_total_signed)
            #         bills_residual += abs(bill.amount_residual_signed)
            #         if bill.invoice_payment_state == 'paid':
            #             bills_paid_amount += abs(bill.amount_total_signed)
            #         if bill.invoice_payment_state == 'not_paid':
            #             paid = abs(bill.amount_total_signed) - \
            #                 abs(bill.amount_residual_signed)
            #             bills_paid_amount += abs(paid)

            order.total_invoiced = invoice_amount
            order.total_bills = bill_amount
            order.margin = invoice_amount - bill_amount
            order.invoice_residual = invoice_residual
            order.invoice_paid_amount = invoice_paid_amount
            order.bills_residual = bills_residual
            order.bills_paid_amount = bills_paid_amount
            order.actual_margin = invoice_paid_amount - bills_paid_amount
            order.fully_paid = True if invoice_residual == 0 else False

    def open_customer_receipts_from_original(self):
        receipt = self.env['account.move'].sudo().search(
            [('freight_operation_id', '=', self.id), ('type', '=', 'out_receipt')])
        return {
            'name': _('Customer Receipts'),
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'views': [(self.env.ref('account.view_move_tree').id, 'tree'),
                      (self.env.ref('account.view_move_form').id, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', [x.id for x in receipt])],
        }

    def open_vendor_receipts_from_original(self):
        receipt = self.env['account.move'].sudo().search(
            [('freight_operation_id', '=', self.id), ('type', '=', 'in_receipt')])
        return {
            'name': _('Vendor Receipts'),
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'views': [(self.env.ref('account.view_move_tree').id, 'tree'),
                      (self.env.ref('account.view_move_form').id, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', [x.id for x in receipt])],
        }

    def get_count_customer_vendor_receipts_from_original(self):
        for rec in self:
            rec.customer_receipt_count = self.env['account.move'].sudo().search_count(
                [('freight_operation_id', '=', rec.id), ('type', '=', 'out_receipt')])

            rec.vendor_receipt_count = self.env['account.move'].sudo().search_count(
                [('freight_operation_id', '=', rec.id), ('type', '=', 'in_receipt')])

    @api.depends('freight_orders')
    def get_values_from_orderTable(self):
        for rec in self:
            sum_container = 0
            sum_weight = 0.0
            sum_volume = 0.0
            if rec.freight_orders:
                for line in rec.freight_orders:
                    sum_weight += line.gross_weight
                    sum_volume += line.volume
                    if line.package.container:
                        sum_container += 1
                rec.no_of_container = sum_container
                rec.order_weight = sum_weight
                rec.order_volume = sum_volume
            else:
                rec.no_of_container = 0
                rec.order_weight = 0.0
                rec.order_volume = 0.0

    def get_count_of_air_delivery_order_report(self):
        for rec in self:
            all_counts = self.env['freight.operation'].search([])
            max_count = max(all_counts.mapped(
                'air_delivery_order_report_count'))
            if not rec.is_printed_before:
                rec.air_delivery_order_report_count = max_count + 1
                rec.is_printed_before = True
                rec.get_sequence_for_air_delivery_order_report()
        return rec.air_delivery_order_report_count, rec.is_printed_before

    def get_sequence_for_air_delivery_order_report(self):
        for rec in self:
            rec.sequence_air_delivery_order_report = 'A' + \
                str(rec.air_delivery_order_report_count).zfill(3)

    def get_sequence_for_bill_of_lading_report(self):
        for rec in self:
            all_counts = self.env['freight.operation'].search([])
            max_count = max(all_counts.mapped('bill_of_lading_report_count'))
            if not rec.is_bill_of_lading_printed_before:
                rec.bill_of_lading_report_count = max_count + 1
                rec.is_bill_of_lading_printed_before = True
                rec.set_sequence_for_bill_of_lading_report()
        return rec.bill_of_lading_report_count, rec.is_bill_of_lading_printed_before

    def set_sequence_for_bill_of_lading_report(self):
        for rec in self:
            rec.sequence_bill_of_lading_report = str(
                rec.bill_of_lading_report_count).zfill(3)

    def get_packages_from_package_line(self):
        lst_packages = []
        sum_of_packages = 0
        for rec in self:
            for package in rec.freight_packages:
                for line in package.freight_item_lines:
                    lst_packages.append(line.qty)
            sum_of_packages = sum(lst_packages)

        return sum_of_packages

    ref_po = fields.Char(string="Reference/PO",)
    mbl = fields.Char(string="MBL",)
    hbl = fields.Char(string="HBL",)
    consignee_po = fields.Char(string="Consignee PO",)
    invoice_created_ids = fields.One2many(
        comodel_name="account.move", inverse_name="freight_operation_id",)
    customer_receipt_count = fields.Float(
        string="Customer Receipts", compute=get_count_customer_vendor_receipts_from_original)
    vendor_receipt_count = fields.Float(
        string="Customer Receipts", compute=get_count_customer_vendor_receipts_from_original)
    no_of_container = fields.Integer(
        string="NO Of Container", compute=get_values_from_orderTable, store=True)
    order_weight = fields.Float(
        string="Weight", compute=get_values_from_orderTable, store=True)
    order_volume = fields.Float(
        string="Volume", compute=get_values_from_orderTable, store=True)
    custom_analytic_acc_id = fields.Many2one(
        comodel_name="account.analytic.account", string="Analytic Account", compute=create_analytic_account_per_operation)
    hawb = fields.Char(string="HAWB No",)
    notify_name = fields.Char(string="Name",)
    notify_address = fields.Char(string="Address",)
    notify_phone = fields.Char(string="Phone",)
    employee_id = fields.Many2one(
        comodel_name="hr.employee", string="Sales Emp", required=True)
    air_delivery_order_report_count = fields.Integer(
        string="Count", store=True, default=0)
    bill_of_lading_report_count = fields.Integer(
        string="bill of lading count", store=True, default=0)
    sequence_air_delivery_order_report = fields.Char(
        string="Sequence", compute=get_sequence_for_air_delivery_order_report, store=True,)
    sequence_bill_of_lading_report = fields.Char(
        string="Sequence bill of lading", compute=set_sequence_for_bill_of_lading_report, store=True,)
    road_no = fields.Char(string="Road No.",)
    land_mbl = fields.Char(string="MBL",)
    land_hbl = fields.Char(string="HBL",)
    obl = fields.Selection(string="OBL", selection=[(
        'original', 'Original'), ('telex', 'Telex'), ('swb', 'SWB'),('copy', 'Copy')], )
    # boolean
    is_notify = fields.Boolean(string="Notify", )
    is_printed_before = fields.Boolean(string="Is_printed_before", store=True)
    is_bill_of_lading_printed_before = fields.Boolean(
        string="Bill of lading printed before", store=True)
    fully_paid = fields.Boolean(
        string="Fully Paid", compute=compute_total_amount)
    customer = fields.Char(string="Customer", compute=_compute_customer_name, store=True)

class Receipts_Customization(models.TransientModel):
    _name = 'rece.rece'
    _description = "Create Receipts"

    def _default_service_ids(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        freight_services = self.env['freight.service'].browse(active_ids)
        return freight_services.ids

    def create_receipt(self):
        self.ensure_one()
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        freight_services = self.env['freight.service'].browse(active_ids)
        account = self.env['account.account']
        line = []
        not_duplicate_currency = []
        for service in self.service_ids:
            not_duplicate_currency.append(service.currency_id.id)
            if service.service_id.property_account_income_id.id:
                income_account = service.service_id.property_account_income_id.id
            elif service.service_id.categ_id.property_account_income_categ_id.id:
                income_account = service.service_id.categ_id.property_account_income_categ_id.id
            else:
                raise UserError(_('Please define income '
                                  'account for this product: "%s" (id:%d).')
                                % (service.service_id.name, service.service_id.id))
            tot = service.qty * service.sale
            tot_cost = service.qty * service.cost
            if self.receipt_type == 'customer':
                if (not service.is_customer_receipt and not service.is_customer_invoice and not service.is_vendor_invoice) or (not service.is_customer_receipt and not service.is_customer_invoice and not service.is_vendor_receipt):
                    service.is_receipt = True
                    service.is_customer_receipt = True
                    if service.currency_id == self.env.ref('base.main_company').currency_id:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': income_account,
                                            'quantity': service.qty,
                                            'credit': service.qty * service.sale,
                                            'price_unit': service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.sale,
                                            'service_id': service.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_receivable_id.id,
                                            'quantity': 1,
                                            'debit': service.qty * service.sale,
                                            'price_unit': -tot,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            }))
                    else:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': income_account,
                                            'quantity': service.qty,
                                            'credit': service.qty * service.sale,
                                            'price_unit': service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.sale,
                                            'service_id': service.id,
                                            'currency_id': service.currency_id.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_receivable_id.id,
                                            'quantity': 1,
                                            'debit': service.qty * service.sale,
                                            'price_unit': -tot,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            'currency_id': service.currency_id.id
                                            }))

            elif self.receipt_type == 'vendor':
                if (not service.is_vendor_receipt and not service.is_customer_receipt and not service.is_vendor_invoice) or (not service.is_vendor_receipt and not service.is_customer_invoice and not service.is_vendor_invoice):
                    service.is_receipt = True
                    service.is_vendor_receipt = True
                    if service.currency_id == self.env.ref('base.main_company').currency_id:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': service.service_id.property_account_expense_id.id or service.service_id.categ_id.property_account_expense_categ_id.id,
                                            'quantity': service.qty,
                                            'price_unit': tot_cost,
                                            'debit': tot_cost,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.cost,
                                            'service_id': service.id,
                                            'analytic_account_id': service.analytic_acc_id.id,
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_payable_id.id,
                                            'quantity': service.qty,
                                            'price_unit': -tot_cost,
                                            'credit': tot_cost,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot_cost,
                                            'partner_id': self.partner_id.id,
                                            }))
                    else:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': service.service_id.property_account_expense_id.id or service.service_id.categ_id.property_account_expense_categ_id.id,
                                            'quantity': service.qty,
                                            'price_unit': tot_cost,
                                            'debit': tot_cost,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.cost,
                                            'service_id': service.id,
                                            'currency_id': service.currency_id.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_payable_id.id,
                                            'quantity': service.qty,
                                            'price_unit': -tot_cost,
                                            'credit': tot_cost,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot_cost,
                                            'partner_id': self.partner_id.id,
                                            'currency_id': service.currency_id.id,
                                            }))
        new_lst = not_duplicate_currency
        for item in not_duplicate_currency:
            for item2 in new_lst:
                if item != item2:
                    raise UserError(
                        'You Cannot Create Invoice Or Receipt with different currencies')
        if self.receipt_type == 'customer' and len(line) != 0:
            invoices = self.env['account.move'].create({
                'type': 'out_receipt',
                'currency_id': service.currency_id.id,
                'partner_id': self.partner_id.id,
                'freight_operation_id': service.shipment_id.id,
                'invoice_line_ids': line,
                'operation_id': service.shipment_id.id,
                'employee_id': service.shipment_id.employee_id.id,
            })
            service.account_move_id = invoices.id

            return {
                'name': _('Customer Receipt'),
                'view_mode': 'form',
                'view_id': self.env.ref('account.view_move_form').id,
                'res_model': 'account.move',
                'context': "{'type':'out_receipt'}",
                'type': 'ir.actions.act_window',
                        'res_id': invoices.id,
            }

        elif self.receipt_type == 'vendor' and len(line) != 0:
            invoices = self.env['account.move'].create({
                'type': 'in_receipt',
                'currency_id': service.currency_id.id,
                'partner_id': self.partner_id.id,
                'freight_operation_id': service.shipment_id.id,
                'invoice_line_ids': line,
                'operation_id': service.shipment_id.id,
            })

            service.account_move_id = invoices.id
            return {
                'name': _('Vendor Receipt'),
                'view_mode': 'form',
                'view_id': self.env.ref('account.view_move_form').id,
                'res_model': 'account.move',
                'context': "{'type':'in_receipt'}",
                'type': 'ir.actions.act_window',
                'res_id': invoices.id,
            }
        else:
            raise UserError(
                'You Cannot Create Invoice Or Receipt For Customer invoice/receipt Or Vendor invoice/receipt')

    partner_id = fields.Many2one('res.partner', 'Partner')
    service_ids = fields.Many2many(
        'freight.service', default=_default_service_ids)
    receipt_type = fields.Selection(
        [('customer', 'Customer'), ('vendor', 'Vendor')], string='Receipt Type')
    type = fields.Selection([('agent', 'Agent'), ('consignee', 'Consignee'), ('shipper', 'Shipper')],
                            string='Receipt to')


class Invoice_inherit(models.Model):
    _inherit = 'account.move'

    def _get_sequence(self):
        self.ensure_one()
        journal = self.journal_id
        if self.type in ('entry', 'out_invoice', 'in_invoice') or not journal.refund_sequence:
            return journal.sequence_id
        if not journal.refund_sequence_id:
            return
        return journal.refund_sequence_id

    @api.onchange('partner_id')
    def get_bank_from_invoices(self):
        lst_bank = []
        lst_all = {}
        for rec in self:
            all_banks = rec.env['bank.invoice'].search([])
            for bank in all_banks:
                lst_bank.append(bank.name)

            for bank_acc in lst_bank:
                domain = [('bank_id.name', '=', bank_acc)]
                record = self.env['bank.acc'].search(domain)
                for acc in record:
                    account = acc.account_num
                    currency = acc.currency_id.name
                    if bank_acc in lst_all:
                        # append the new number to the existing array at this slot
                        lst_all[bank_acc].append([account, currency])
                    else:
                        # create a new array in this slot
                        lst_all[bank_acc] = [[account, currency]]
            return lst_all

    operation_id = fields.Many2one(
        comodel_name="freight.operation", string="Operation", readonly=True)
    employee_id = fields.Many2one(
        comodel_name="hr.employee", string="Sales Emp", readonly=True)


class Account_move_inhert(models.Model):
    _inherit = 'account.move.line'

    service_id = fields.Many2one(
        comodel_name="freight.service", string="Service",)


class RegisterInvoice_inherit(models.TransientModel):
    _inherit = "register.invoice"

    def create_invoice(self):
        self.ensure_one()
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        freight_services = self.env['freight.service'].browse(active_ids)
        line = []
        not_duplicate_currency = []
        for service in self.service_ids:
            not_duplicate_currency.append(service.currency_id.id)
            if service.service_id.property_account_income_id.id:
                income_account = service.service_id.property_account_income_id.id
            elif service.service_id.categ_id.property_account_income_categ_id.id:
                income_account = service.service_id.categ_id.property_account_income_categ_id.id
            else:
                raise UserError(_('Please define income '
                                  'account for this product: "%s" (id:%d).')
                                % (service.service_id.name, service.service_id.id))
            tot = service.qty * service.sale
            if self.invoice_type == 'customer':
                if (not service.is_customer_invoice and not service.is_customer_receipt and not service.is_vendor_invoice) or (not service.is_customer_invoice and not service.is_customer_receipt and not service.is_vendor_receipt):
                    service.is_invoice = True
                    service.is_customer_invoice = True
                    if service.currency_id == self.env.ref('base.main_company').currency_id:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': income_account,
                                            'quantity': service.qty,
                                            'credit': service.qty * service.sale,
                                            'price_unit': service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.sale,
                                            'service_id': service.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_receivable_id.id,
                                            'quantity': 1,
                                            'debit': service.qty * service.sale,
                                            'price_unit': -tot,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            }))
                    else:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': income_account,
                                            'quantity': service.qty,
                                            'credit': service.qty * service.sale,
                                            'price_unit': service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.sale,
                                            'service_id': service.id,
                                            'currency_id': service.currency_id.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_receivable_id.id,
                                            'quantity': 1,
                                            'debit': service.qty * service.sale,
                                            'price_unit': -tot,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            'currency_id': service.currency_id.id
                                            }))
            elif self.invoice_type == 'vendor':
                if (not service.is_vendor_invoice and not service.is_customer_receipt and not service.is_vendor_receipt) or (not service.is_vendor_invoice and not service.is_customer_invoice and not service.is_vendor_receipt):
                    service.is_invoice = True
                    service.is_vendor_invoice = True
                    if service.currency_id == self.env.ref('base.main_company').currency_id:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': service.service_id.property_account_expense_id.id or service.service_id.categ_id.property_account_expense_categ_id.id,
                                            'quantity': service.qty,
                                            'price_unit': service.cost,
                                            'debit': service.qty * service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.cost,
                                            'service_id': service.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_payable_id.id,
                                            'quantity': service.qty,
                                            'price_unit': -tot,
                                            'credit': service.qty * service.sale,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            }))
                    else:
                        line.append((0, 0, {'name': service.name,
                                            'account_id': service.service_id.property_account_expense_id.id or service.service_id.categ_id.property_account_expense_categ_id.id,
                                            'quantity': service.qty,
                                            'price_unit': service.cost,
                                            'debit': service.qty * service.sale,
                                            'product_id': service.service_id.id,
                                            'price_subtotal': service.qty * service.cost,
                                            'service_id': service.id,
                                            'currency_id': service.currency_id.id,
                                            'analytic_account_id': service.analytic_acc_id.id
                                            }))
                        line.append((0, 0, {'account_id': self.partner_id.property_account_payable_id.id,
                                            'quantity': service.qty,
                                            'price_unit': -tot,
                                            'credit': service.qty * service.sale,
                                            'exclude_from_invoice_tab': True,
                                            'price_subtotal': -tot,
                                            'partner_id': self.partner_id.id,
                                            'currency_id': service.currency_id.id
                                            }))
        new_lst = not_duplicate_currency
        for item in not_duplicate_currency:
            for item2 in new_lst:
                if item != item2:
                    raise UserError(
                        'You Cannot Create Invoice Or Receipt with different currencies')

        if self.invoice_type == 'customer' and len(line) != 0:
            invoices = self.env['account.move'].create({
                'type': 'out_invoice',
                'partner_id': self.partner_id.id,
                'currency_id': service.currency_id.id,
                'freight_operation_id': service.shipment_id.id,
                'invoice_line_ids': line,
                'operation_id': service.shipment_id.id,
                'employee_id': service.shipment_id.employee_id.id,
            })
            service.account_move_id = invoices.id

            return {
                'name': _('Customer Invoice'),
                'view_mode': 'form',
                'view_id': self.env.ref('account.view_move_form').id,
                'res_model': 'account.move',
                'context': "{'type':'out_invoice'}",
                'type': 'ir.actions.act_window',
                'res_id': invoices.id,
            }
        elif self.invoice_type == 'vendor' and len(line) != 0:
            invoices = self.env['account.move'].create({
                'type': 'in_invoice',
                'currency_id': service.currency_id.id,
                'partner_id': self.partner_id.id,
                'freight_operation_id': service.shipment_id.id,
                'invoice_line_ids': line,
                'operation_id': service.shipment_id.id,
            })
            service.account_move_id = invoices.id
            return {
                'name': _('Vendor Invoice'),
                'view_mode': 'form',
                'view_id': self.env.ref('account.view_move_form').id,
                'res_model': 'account.move',
                'context': "{'type':'in_invoice'}",
                'type': 'ir.actions.act_window',
                'res_id': invoices.id,
            }
        else:
            raise UserError(
                'You Cannot Create Invoice Or Receipt For Customer invoice/receipt Or Vendor invoice/receipt')


class Freight_service_inherit(models.Model):
    _inherit = 'freight.service'

    @api.depends('account_move_id.state')
    def get_stage_from_invoice(self):
        for rec in self:
            rec.state = rec.account_move_id.state

    def get_analitic_from_operation_by_shipment_id(self):
        for rec in self:
            if rec.shipment_id.custom_analytic_acc_id:
                rec.analytic_acc_id = rec.shipment_id.custom_analytic_acc_id.id

    account_move_id = fields.Many2one(
        comodel_name="account.move", string="Invoice/Receipt", readonly=True)
    is_invoice = fields.Boolean(string="Is Invoiced",)
    is_receipt = fields.Boolean(string="Is Receipt",)
    is_customer_invoice = fields.Boolean(string="Customer Invoice",)
    is_vendor_invoice = fields.Boolean(string="Vendor Invoice",)
    is_customer_receipt = fields.Boolean(string="Customer Receipt",)
    is_vendor_receipt = fields.Boolean(string="Vendor Receipt",)
    state = fields.Selection(string="Stage", selection=[('draft', 'Draft'), (
        'posted', 'Posted'), ('cancel', 'Cancelled')], compute=get_stage_from_invoice)
    analytic_acc_id = fields.Many2one(comodel_name="account.analytic.account",
                                      string="Analytic Account", compute=get_analitic_from_operation_by_shipment_id)


class Create_Bank_Invoice(models.Model):
    _name = 'bank.invoice'
    _rec_name = 'name'
    _description = 'Create Bank Invoice To Print In Invoices'

    name = fields.Char('Bank Name')
    acc_ids = fields.One2many(comodel_name="bank.acc",
                              inverse_name="bank_id", string="Accounts",)


class Bank_accounts(models.Model):
    _name = 'bank.acc'
    _description = 'Create Bank Account To Print In Invoices'

    bank_id = fields.Many2one(comodel_name="bank.invoice", string="Bank",)
    account_num = fields.Char(string="Account Number",)
    currency_id = fields.Many2one(
        comodel_name="res.currency", string="Currency",)


class Freight_Line(models.Model):
    _inherit = 'freight.package.line'

    reference = fields.Char(string="Container No.",)


class Air_Delivery_Order(models.AbstractModel):
    _name = 'report.custom_freight.air_delivery_order_custom_template'
    _description = 'Freight Air Delivery Order'

    def _get_report_values(self, docids, data=None):
        docs = self.env['freight.operation'].browse(docids)
        for doc in docs:
            if doc.transport != 'air':
                raise ValidationError(
                    'You can print this report while transport is air')
            return {
                'doc_ids': docs.ids,
                'doc_model': 'sale.order',
                'docs': docs,
                'proforma': True
            }


class Ocean_bill_of_lading(models.AbstractModel):
    _name = 'report.custom_freight.bill_of_lading_template'
    _description = 'Freight Bill Of lading report'

    def _get_report_values(self, docids, data=None):
        docs = self.env['freight.operation'].browse(docids)
        for doc in docs:
            if doc.transport != 'ocean':
                raise ValidationError(
                    'You can print this report while transport is Ocean')
            return {
                'doc_ids': docs.ids,
                'doc_model': 'freight.operation',
                'docs': docs,
                'proforma': True
            }