from odoo import api, fields, models

class purchase_order_inherit_po_costsheet(models.Model):
    _inherit = 'purchase.order'



class Purchase_order_line_inherit_po_creation_costsheet(models.Model):
    _inherit = 'purchase.order.line'

    cc_qty = fields.Float(string="Cost Sheet QTY",  readonly=True,)