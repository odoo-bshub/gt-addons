# -*- encoding: utf-8 -*-
{
	"name": "Project Management Emails",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'purchase_order_custom','stock','account','account_accountant','project','mail'
	],
	"category": "",
	"complexity": "",
	"description": """
	
	""",
	"data": [
		'security/ir.model.access.csv',
		'security/groups.xml',
		'data/po_confirm_template.xml',
		'data/stock_vilidate_template.xml',
		'data/create_bill_template.xml',
		'data/bill_confirmed_template.xml',
		'data/ask_for_po_temp.xml',
		'views/project_project_inherit.xml',

	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
