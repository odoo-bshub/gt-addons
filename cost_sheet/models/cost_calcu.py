from odoo import api, fields, models
import logging
import psycopg2
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class Import(models.TransientModel):
    _inherit = 'base_import.import'

    def do(self, fields, columns, options, dryrun=False):
        """ Actual execution of the import

        :param fields: import mapping: maps each column to a field,
                       ``False`` for the columns to ignore
        :type fields: list(str|bool)
        :param columns: columns label
        :type columns: list(str|bool)
        :param dict options:
        :param bool dryrun: performs all import operations (and
                            validations) but rollbacks writes, allows
                            getting as much errors as possible without
                            the risk of clobbering the database.
        :returns: A list of errors. If the list is empty the import
                  executed fully and correctly. If the list is
                  non-empty it contains dicts with 3 keys ``type`` the
                  type of error (``error|warning``); ``message`` the
                  error message associated with the error (a string)
                  and ``record`` the data which failed to import (or
                  ``false`` if that data isn't available or provided)
        :rtype: dict(ids: list(int), messages: list({type, message, record}))
        """
        if "cost.calc" == self.res_model:
            if not self._context.get('active_id'):
                raise ValidationError('You must Create/Import From CRM screen Only')
            else:
                self.ensure_one()
                self._cr.execute('SAVEPOINT import')
                columns.append('customer')
                columns.append('related opportunity')
                columns.append('currency')

                try:
                    data, import_fields = self._convert_import_data(fields, options)
                    import_fields.append('partner_id')
                    import_fields.append('opportunity_id')
                    import_fields.append('currency_id')

                    # Parse date and float field
                    data = self._parse_import_data(data, import_fields, options)

                    current_crm_by_active_id = self.env['crm.lead'].sudo().search(
                        [('id', '=', self._context.get('active_id'))])
                    for d in data:
                        if data.index(d) == 0:
                            d.append(current_crm_by_active_id.partner_id.name)
                            d.append(current_crm_by_active_id.name)
                            d.append('USD')
                            len(d)
                        else:
                            d.append('')
                            d.append('')
                            d.append('')



                except ValueError as error:
                    return {
                        'messages': [{
                            'type': 'error',
                            'message': str(error),
                            'record': False,
                        }]
                    }



                _logger.info('importing %d rows...', len(data))

                name_create_enabled_fields = options.pop('name_create_enabled_fields', {})
                import_limit = options.pop('limit', None)
                model = self.env[self.res_model].sudo().with_context(import_file=True,
                                                              name_create_enabled_fields=name_create_enabled_fields,
                                                              _import_limit=import_limit)
                import_result = model.load(import_fields, data)
                _logger.info('done')

                # If transaction aborted, RELEASE SAVEPOINT is going to raise
                # an InternalError (ROLLBACK should work, maybe). Ignore that.
                # TODO: to handle multiple errors, create savepoint around
                #       write and release it in case of write error (after
                #       adding error to errors array) => can keep on trying to
                #       import stuff, and rollback at the end if there is any
                #       error in the results.
                try:
                    if dryrun:
                        self._cr.execute('ROLLBACK TO SAVEPOINT import')
                        # cancel all changes done to the registry/ormcache
                        self.pool.clear_caches()
                        self.pool.reset_changes()
                    else:
                        self._cr.execute('RELEASE SAVEPOINT import')
                except psycopg2.InternalError:
                    pass

                # Insert/Update mapping columns when import complete successfully
                if import_result['ids'] and options.get('headers'):
                    BaseImportMapping = self.env['base_import.mapping']
                    for column_name in columns:
                        if column_name:
                            # Update to latest selected field
                            mapping_domain = [('res_model', '=', self.res_model), ('column_name', '=', column_name)]
                            column_mapping = BaseImportMapping.search(mapping_domain, limit=1)
                            # # if column_mapping:
                            # #     if column_mapping.field_name != fields[index]:
                            # #         column_mapping.field_name = fields[index]
                            # else:
                            BaseImportMapping.sudo().create({
                                    'res_model': self.res_model,
                                    'column_name': column_name,
                                    # 'field_name': fields[index]
                                })
                if 'name' in import_fields:
                    index_of_name = import_fields.index('name')
                    skipped = options.get('skip', 0)
                    # pad front as data doesn't contain anythig for skipped lines
                    r = import_result['name'] = [''] * skipped
                    # only add names for the window being imported
                    r.extend(x[index_of_name] for x in data[:import_limit])
                    # pad back (though that's probably not useful)
                    r.extend([''] * (len(data) - (import_limit or 0)))
                else:
                    import_result['name'] = []

                skip = options.get('skip', 0)
                # convert load's internal nextrow to the imported file's
                if import_result['nextrow']:  # don't update if nextrow = 0 (= no nextrow)
                    import_result['nextrow'] += skip

                # Start Of My Code +++++++++++++++++

                    # create product from importing cost sheet

                dic_of_vendors_in_lines = {}
                list_of_vendors_in_lines_without_duplication = []
                if "cost.calc" == self.res_model:
                    for record in import_result['ids']:
                        current_record = self.env[self.res_model].sudo().browse(record)
                        current_record.opportunity_id.create_cost_calculation_bool = True

                        # set discount and margin from summary and perform it in cost lines
                        dic_discount_margin = {}
                        for summary_line in current_record.summary_line_ids:
                            if summary_line.brand_id.id in dic_discount_margin:
                                dic_discount_margin[summary_line.brand_id.id]['discount'].append(
                                    summary_line.cost_discount)
                                dic_discount_margin[summary_line.brand_id.id]['margin'].append(
                                    summary_line.input_margin)
                            else:
                                dic_discount_margin[summary_line.brand_id.id] = {
                                    'discount': [summary_line.cost_discount],
                                    'margin': [summary_line.input_margin],
                                }
                        print('dic_discount_margin',dic_discount_margin)

                        for brand in dic_discount_margin:
                            for cost_line in current_record.cost_ids:
                                if brand == cost_line.brand_id.id:
                                    cost_line.discount = sum(dic_discount_margin[brand]['discount'])
                                    cost_line.margin = sum(dic_discount_margin[brand]['margin'])

                        # Get Dictionary of total by brand and set it's value in summary lines
                    dic_cost_lines = {}
                    for line in current_record.cost_ids:
                        if line.brand_id.id in dic_cost_lines:
                            dic_cost_lines[line.brand_id.id]['ext_list'].append(line.ext_list)
                            dic_cost_lines[line.brand_id.id]['ext_d_cost'].append(line.ext_d_cost)
                            dic_cost_lines[line.brand_id.id]['ext_selling'].append(line.ext_selling)
                            dic_cost_lines[line.brand_id.id]['ext_sar'].append(line.ext_sar)
                            dic_cost_lines[line.brand_id.id]['description'].append(line.description)
                        else:
                            dic_cost_lines[line.brand_id.id] = {'ext_list': [line.ext_list],
                                                                'ext_d_cost': [line.ext_d_cost],
                                                                'ext_selling': [line.ext_selling],
                                                                'ext_sar': [line.ext_sar],
                                                                'description': [line.description],
                                                                }

                    for summ_line in current_record.summary_line_ids:
                        for brand_item in dic_cost_lines:
                            if summ_line.brand_id.id == brand_item:

                                summ_line.list = sum(dic_cost_lines[brand_item]['ext_list'])
                                summ_line.cost_dollar = sum(dic_cost_lines[brand_item]['ext_d_cost'])
                                summ_line.selling_dollar = sum(dic_cost_lines[brand_item]['ext_selling'])
                                summ_line.selling_sar = sum(dic_cost_lines[brand_item]['ext_sar'])

                    all_products = self.env['product.product'].search([]).mapped('default_code')
                    if len(data[0]) == 9:
                        for list_data in data:
                                # product not exsist
                                if list_data[1] not in all_products:
                                    not_exist_product = self.env['product.product'].search([('name', '=', list_data[1])])
                                    not_exist_product.sudo().update({
                                            'name': list_data[2],
                                            'default_code': list_data[1],
                                            'brand_id': self.env['brand.config'].search([('name', '=', list_data[5])]).id,
                                            'categ_id': 5,
                                            'uom_id': 1,
                                            'uom_po_id': 1,
                                            'type': 'product',
                                        })

                    else:
                        for list_data in data:
                            # product not exsist
                            if list_data[2] not in all_products:
                                not_exist_product = self.env['product.product'].search([('name', '=', list_data[2])])
                                not_exist_product.sudo().update({
                                    'name': list_data[3],
                                    'default_code': list_data[2],
                                    'brand_id': self.env['brand.config'].search([('name', '=', list_data[6])]).id,
                                    'categ_id': 5,
                                    'uom_id': 1,
                                    'uom_po_id': 1,
                                    'type': 'product',
                                })



                        # find if product with this internal reference is exsist or not
                        # if product_line.product_id.default_code in all_products:
                        #     print(product_line.product_id.name,'2')

                        # self.env['product.product'].sudo().create({
                        #     'name':product_line.description,
                        #     'default_code':product_line.
                        # })


        else:
            """ Actual execution of the import

                    :param fields: import mapping: maps each column to a field,
                                   ``False`` for the columns to ignore
                    :type fields: list(str|bool)
                    :param columns: columns label
                    :type columns: list(str|bool)
                    :param dict options:
                    :param bool dryrun: performs all import operations (and
                                        validations) but rollbacks writes, allows
                                        getting as much errors as possible without
                                        the risk of clobbering the database.
                    :returns: A list of errors. If the list is empty the import
                              executed fully and correctly. If the list is
                              non-empty it contains dicts with 3 keys ``type`` the
                              type of error (``error|warning``); ``message`` the
                              error message associated with the error (a string)
                              and ``record`` the data which failed to import (or
                              ``false`` if that data isn't available or provided)
                    :rtype: dict(ids: list(int), messages: list({type, message, record}))
                    """
            self.ensure_one()
            self._cr.execute('SAVEPOINT import')

            try:
                data, import_fields = self._convert_import_data(fields, options)
                # Parse date and float field
                data = self._parse_import_data(data, import_fields, options)
            except ValueError as error:
                return {
                    'messages': [{
                        'type': 'error',
                        'message': str(error),
                        'record': False,
                    }]
                }

            _logger.info('importing %d rows...', len(data))

            name_create_enabled_fields = options.pop('name_create_enabled_fields', {})
            import_limit = options.pop('limit', None)
            model = self.env[self.res_model].with_context(import_file=True,
                                                          name_create_enabled_fields=name_create_enabled_fields,
                                                          _import_limit=import_limit)
            import_result = model.load(import_fields, data)
            _logger.info('done')

            # If transaction aborted, RELEASE SAVEPOINT is going to raise
            # an InternalError (ROLLBACK should work, maybe). Ignore that.
            # TODO: to handle multiple errors, create savepoint around
            #       write and release it in case of write error (after
            #       adding error to errors array) => can keep on trying to
            #       import stuff, and rollback at the end if there is any
            #       error in the results.
            try:
                if dryrun:
                    self._cr.execute('ROLLBACK TO SAVEPOINT import')
                    # cancel all changes done to the registry/ormcache
                    self.pool.clear_caches()
                    self.pool.reset_changes()
                else:
                    self._cr.execute('RELEASE SAVEPOINT import')
            except psycopg2.InternalError:
                pass

            # Insert/Update mapping columns when import complete successfully
            if import_result['ids'] and options.get('headers'):
                BaseImportMapping = self.env['base_import.mapping']
                for index, column_name in enumerate(columns):
                    if column_name:
                        # Update to latest selected field
                        mapping_domain = [('res_model', '=', self.res_model), ('column_name', '=', column_name)]
                        column_mapping = BaseImportMapping.search(mapping_domain, limit=1)
                        if column_mapping:
                            if column_mapping.field_name != fields[index]:
                                column_mapping.field_name = fields[index]
                        else:
                            BaseImportMapping.create({
                                'res_model': self.res_model,
                                'column_name': column_name,
                                'field_name': fields[index]
                            })
            if 'name' in import_fields:
                index_of_name = import_fields.index('name')
                skipped = options.get('skip', 0)
                # pad front as data doesn't contain anythig for skipped lines
                r = import_result['name'] = [''] * skipped
                # only add names for the window being imported
                r.extend(x[index_of_name] for x in data[:import_limit])
                # pad back (though that's probably not useful)
                r.extend([''] * (len(data) - (import_limit or 0)))
            else:
                import_result['name'] = []

            skip = options.get('skip', 0)
            # convert load's internal nextrow to the imported file's
            if import_result['nextrow']:  # don't update if nextrow = 0 (= no nextrow)
                import_result['nextrow'] += skip


            #my code
            if "purchase.order" == self.res_model:
                    for record in import_result['ids']:
                        current_record = self.env[self.res_model].browse(record)
                        for po_line in current_record.order_line:
                            if po_line.product_id.brand_id:
                                po_line.brand_id = po_line.product_id.brand_id.id

        return import_result


class Cost_Calc(models.Model):
    _name = 'cost.calc'

    @api.model
    def create(self, values):
        duplicate_record = self.search([('opportunity_id', '=', values['opportunity_id'])])
        if duplicate_record:
            raise ValidationError('Cost Calculation already exist for the opportunity')
        else:
            res = super(Cost_Calc, self).create(values)
            res.opportunity_id.created_cost_calculation_id = res.id
            res.show_summary = True
            res.opportunity_id.create_cost_calculation_bool = True
        return res

    # def copy(self):
    #     self.on_change_cost_ids_set_total_ids_by_partner()
    #     return super(Cost_Calc, self).copy()

    # @api.model
    # def create(self, values):
    #     dic_of_vendors_in_lines = {}
    #     list_of_vendors_in_lines_without_duplication = []
    #     res = super(Cost_Calc, self).create(values)
    #     if res.total_ids:
    #         for line in res.cost_ids:
    #             for tot_line in res.total_ids:
    #                 if line.brand_id == tot_line.brand_id:
    #                     if line.brand_id.id in dic_of_vendors_in_lines:
    #                         dic_of_vendors_in_lines[line.brand_id.id]['cost_many2many'].append(line.id)
    #                     else:
    #                         dic_of_vendors_in_lines[line.brand_id.id] = {'cost_many2many': [line.id],
    #                                                                      'discount': line.discount,
    #                                                                      'margin': line.margin,
    #                                                                      'list': tot_line.total_ext_list,
    #                                                                      'cost_dollar': tot_line.total_ext_d_cost,
    #                                                                      'selling_dollar': tot_line.total_ext_selling,
    #                                                                      'selling_sar': tot_line.total_ext_sar
    #                                                                      }
    #
    #     for key in dic_of_vendors_in_lines:
    #         list_of_vendors_in_lines_without_duplication.append((0, 0, {'brand_id': key,
    #                                                                     'cost_line_ids': dic_of_vendors_in_lines[key][
    #                                                                         'cost_many2many'],
    #                                                                     'cost_discount': dic_of_vendors_in_lines[key][
    #                                                                         'discount'],
    #                                                                     'input_margin': dic_of_vendors_in_lines[key][
    #                                                                         'margin'],
    #                                                                     'list': dic_of_vendors_in_lines[key]['list'],
    #                                                                     'cost_dollar': dic_of_vendors_in_lines[key][
    #                                                                         'cost_dollar'],
    #                                                                     'selling_dollar': dic_of_vendors_in_lines[key][
    #                                                                         'selling_dollar'],
    #                                                                     'selling_sar': dic_of_vendors_in_lines[key][
    #                                                                         'selling_sar']
    #                                                                     }))
    #
    #     new_summury = self.env['summary.summary'].sudo().create({
    #         'name': 'Summary For ' + res.partner_id.name or 'Empty Customer',
    #         'partner_id': res.partner_id.id or False,
    #         'project_id': res.project_id.id or False,
    #         'currency_id': res.currency_id.id or False,
    #         'cost_calculation_id': res.id or False,
    #         'summary_line_ids': list_of_vendors_in_lines_without_duplication or False
    #     })
    #     return res

    def create_summary(self):
        dic_of_vendors_in_lines = {}
        list_of_vendors_in_lines_without_duplication = []
        if self.total_ids:
            for line in self.cost_ids:
                for tot_line in self.total_ids:
                    if line.brand_id == tot_line.brand_id:
                        if line.brand_id.id in dic_of_vendors_in_lines:
                            dic_of_vendors_in_lines[line.brand_id.id]['cost_many2many'].append(line.id)
                        else:
                            dic_of_vendors_in_lines[line.brand_id.id] = {'cost_many2many': [line.id],
                                                                         'discount': line.discount,
                                                                         'margin': line.margin,
                                                                         'list': tot_line.total_ext_list,
                                                                         'cost_dollar': tot_line.total_ext_d_cost,
                                                                         'selling_dollar': tot_line.total_ext_selling,
                                                                         'selling_sar': tot_line.total_ext_sar,
                                                                         'overall_margin': (
                                                                                                   1 - tot_line.total_ext_d_cost / tot_line.total_ext_selling) * 100
                                                                         }

        for key in dic_of_vendors_in_lines:
            list_of_vendors_in_lines_without_duplication.append((0, 0, {'brand_id': key,
                                                                        'cost_line_ids': [
                                                                            (6, 0, dic_of_vendors_in_lines[key][
                                                                                'cost_many2many'])],
                                                                        'cost_discount': dic_of_vendors_in_lines[key][
                                                                            'discount'],
                                                                        'input_margin': dic_of_vendors_in_lines[key][
                                                                            'margin'],
                                                                        'list': dic_of_vendors_in_lines[key]['list'],
                                                                        'cost_dollar': dic_of_vendors_in_lines[key][
                                                                            'cost_dollar'],
                                                                        'selling_dollar': dic_of_vendors_in_lines[key][
                                                                            'selling_dollar'],
                                                                        'selling_sar': dic_of_vendors_in_lines[key][
                                                                            'selling_sar'],
                                                                        'overall_margin': dic_of_vendors_in_lines[key][
                                                                            'overall_margin']
                                                                        }))

        self.write({
            'summary_line_ids': list_of_vendors_in_lines_without_duplication or False,
            'show_summary': False
        })

        # new_summury = self.env['summary.summary'].sudo().create({
        #     'name': 'Summary For ' + self.partner_id.name or 'Empty Customer',
        #     'partner_id': self.partner_id.id or False,
        #     'project_id': self.project_id.id or False,
        #     'currency_id': self.currency_id.id or False,
        #     'cost_calculation_id': self.id or False,
        #     'summary_line_ids': list_of_vendors_in_lines_without_duplication or False
        # })

    @api.depends('cost_ids')
    @api.onchange('cost_ids')
    def on_change_cost_ids_(self):
        for rec in self:

            # #get difference between two lists of brands
            # new_brand_must_be_inserted_in_summary_lines = set(lst_brand) - set(rec.summary_line_ids.mapped('brand_id.id'))
            #
            # brand_must_be_removed_from_summary_lines = set(rec.summary_line_ids.mapped('brand_id.id')) - set(lst_brand)
            # print(brand_must_be_removed_from_summary_lines)
            #
            # if list(new_brand_must_be_inserted_in_summary_lines):
            #     for brand in list(new_brand_must_be_inserted_in_summary_lines):
            #         rec.write({
            #             'summary_line_ids': [(0, 0, {
            #                 'brand_id': brand,
            #             })]
            #         })
            # if list(brand_must_be_removed_from_summary_lines):
            #     for brand_remove in list(brand_must_be_removed_from_summary_lines):
            #         print('yes')
            #         rm_record  = rec.env['summary.summary.line'].search([('brand_id','=',brand_remove)])
            #         rm_record.sudo().unlink()

            # New Brand in cost_ids reflect in summary
            rec.summary_line_ids = False
            lst_brand = []
            for line_cost in rec.cost_ids:
                if line_cost.brand_id:
                    lst_brand.append(line_cost.brand_id.id)
            not_duplicated_brands = set(lst_brand)
            for brand in list(not_duplicated_brands):
                rec.write({
                    'summary_line_ids': [(0, 0, {
                        'brand_id': brand,
                    })]
                })

            #get data of Cost_ids
            dic_lines = {}
            for line in rec.cost_ids:
                if line.brand_id or line.ext_list or line.ext_d_cost or line.ext_selling or line.ext_sar:
                    if line.brand_id.id in dic_lines:
                        dic_lines[line.brand_id.id]['ext_list'].append(line.ext_list)
                        dic_lines[line.brand_id.id]['ext_d_cost'].append(line.ext_d_cost)
                        dic_lines[line.brand_id.id]['ext_selling'].append(line.ext_selling)
                        dic_lines[line.brand_id.id]['ext_sar'].append(line.ext_sar)
                        dic_lines[line.brand_id.id]['discount'].append(line.discount)
                        dic_lines[line.brand_id.id]['margin'].append(line.margin)
                    else:
                        dic_lines[line.brand_id.id] = {'ext_list': [line.ext_list],
                                                       'ext_d_cost': [line.ext_d_cost],
                                                       'ext_selling': [line.ext_selling],
                                                       'ext_sar': [line.ext_sar],
                                                       'discount': [line.discount],
                                                       'margin': [line.margin],

                                                       }

            # update the value of summary line ids from cost lines
            for summary_line in rec.summary_line_ids:
                for brand_key in dic_lines:
                    if summary_line.brand_id.id == brand_key:
                        # set total_list in summary lines
                        summary_line.update({
                            'list': sum(dic_lines[brand_key]['ext_list']),
                            'cost_dollar': sum(dic_lines[brand_key]['ext_d_cost']),
                            'selling_dollar': sum(dic_lines[brand_key]['ext_selling']),
                            'selling_sar': sum(dic_lines[brand_key]['ext_sar']),
                            'cost_discount': max(dic_lines[brand_key]['discount']),
                            'input_margin': max(dic_lines[brand_key]['margin']),
                        })

            # set values in total_ids
            # for value in dic_lines:
            #     self.write({
            #         'total_ids': [(0, 0, {
            #             'brand_id': self.env['brand.config'].browse(value).id,
            #             'total_ext_list': sum(dic_lines[value]['ext_list']),
            #             'total_ext_d_cost': sum(dic_lines[value]['ext_d_cost']),
            #             'total_ext_selling': sum(dic_lines[value]['ext_selling']),
            #             'total_ext_sar': sum(dic_lines[value]['ext_sar']),
            #         })]
            #     })
            #
            # if rec.state in ['draft']:
            #     # update values in summary from total_ids
            #     # summary = self.env['summary.summary'].search([('cost_calculation_id', '=', self.id)],
            #     #                                              limit=1).summary_line_ids
            #     summary = self.summary_line_ids
            #
            #     for summary_line in summary:
            #         for total_line in rec.total_ids:
            #             if summary_line.brand_id == total_line.brand_id:
            #                 # set total_list in summary lines
            #                 summary_line.update({
            #                     'list': total_line.total_ext_list,
            #                     'cost_dollar': total_line.total_ext_d_cost,
            #                     'selling_dollar': total_line.total_ext_selling,
            #                     'selling_sar': total_line.total_ext_sar,
            #                 })

    @api.depends('summary_line_ids')
    @api.onchange('summary_line_ids')
    def on_change_summary_summary_set_discount_and_margin_by_partner(self):
        for rec in self:
            if rec.summary_line_ids:
                for line in rec.summary_line_ids:
                    if line.cost_discount or line.input_margin:
                        # if line.cost_line_ids:
                        #     for cost_record in line.cost_line_ids.ids:
                        #         self.env['cost.calc.line'].search([('id', '=', cost_record)]).update({
                        #             'discount': line.cost_discount,
                        #         })
                        if self.cost_ids:
                            for calc in self.cost_ids:
                                if line.brand_id == calc.brand_id:
                                    calc.update({
                                        'discount': line.cost_discount,
                                        'margin': line.input_margin,
                                    })

            dic_cost_lines = {}
            for line_cost in self.cost_ids:
                if line_cost.brand_id.id in dic_cost_lines:
                    dic_cost_lines[line_cost.brand_id.id]['ext_list'].append(line_cost.ext_list)
                    dic_cost_lines[line_cost.brand_id.id]['ext_d_cost'].append(line_cost.ext_d_cost)
                    dic_cost_lines[line_cost.brand_id.id]['ext_selling'].append(line_cost.ext_selling)
                    dic_cost_lines[line_cost.brand_id.id]['ext_sar'].append(line_cost.ext_sar)
                else:
                    dic_cost_lines[line_cost.brand_id.id] = {'ext_list': [line_cost.ext_list],
                                                             'ext_d_cost': [line_cost.ext_d_cost],
                                                             'ext_selling': [line_cost.ext_selling],
                                                             'ext_sar': [line_cost.ext_sar],
                                                             }
            for summ_line in self.summary_line_ids:
                for brand_item in dic_cost_lines:
                    if summ_line.brand_id.id == brand_item:
                        summ_line.list = sum(dic_cost_lines[brand_item]['ext_list'])
                        summ_line.cost_dollar = sum(dic_cost_lines[brand_item]['ext_d_cost'])
                        summ_line.selling_dollar = sum(dic_cost_lines[brand_item]['ext_selling'])
                        summ_line.selling_sar = sum(dic_cost_lines[brand_item]['ext_sar'])

            # self.on_change_cost_ids_set_total_ids_by_partner()

    def create_quotation(self):
        for rec in self:
            line = []
            if rec.cost_ids:
                rec.state = 'qoute_created'
                for product in rec.cost_ids:
                    line.append((0, 0, {'brand_id': product.brand_id.id,
                                        'product_id': product.product_id.id,
                                        'name': product.description,
                                        'part_num': product.part_num,
                                        'product_uom_qty': product.qty,
                                        'price_unit': product.unit_sar,
                                        'disc2': product.discount,
                                        'margin_percent': product.margin,
                                        'is_created_from_cost_calculation': True,
                                        }))
                Qutation = self.env['sale.order'].create({
                    'partner_id': rec.partner_id.id,
                    'order_line': line,
                    'cost_calculation_id': self.id,
                    'company_id': self.company_id.id,
                    'opportunity_id': self.opportunity_id.id,
                    'analytic_account_id': self.project_id.analytic_account_id.id,
                })
                return Qutation
            else:
                raise ValidationError("You must fill cost calculation table before creating Quotation")

    def create_financial_proposal(self):
        for rec in self:
            line = []
            if rec.cost_ids:
                for product in rec.cost_ids:
                    line.append((0, 0, {
                        'product_id': product.product_id.id,
                        'uom_id': product.product_id.uom_id.id,
                        'quant': product.qty,
                        'total_without_compute_quantity': product.unit_sar,
                    }))
                fin = self.env['fin.pro'].create({
                    'quant_price_ids': line,
                    'opportunity_id': self.opportunity_id.id,
                })
                fin.fill_values_of_pay_table_from_quantity_table()

    def action_open_related_summary(self):
        action = self.env.ref('cost_sheet.summary_summary_action_view').read()[0]
        action['context'] = {}
        action['domain'] = [('cost_calculation_id', '=', self.id)]
        return action

    def action_open_related_quotation(self):
        action = self.env.ref('sale.action_quotations_with_onboarding').read()[0]
        action['context'] = {}
        action['domain'] = [('cost_calculation_id', '=', self.id)]
        return action

    def get_summary_count(self):
        for rec in self:
            count = rec.env['summary.summary'].search_count([('cost_calculation_id', '=', self.id)])
            rec.summary_count = count or 0

    def get_quotation_count(self):
        for rec in self:
            count = rec.env['sale.order'].search_count([('cost_calculation_id', '=', self.id)])
            rec.quotation_count = count or 0

    @api.onchange('summary_line_ids', 'summary_line_ids.list', 'summary_line_ids.cost_dollar',
                  'summary_line_ids.selling_dollar', 'summary_line_ids.selling_sar', 'summary_line_ids.cost_discount',
                  'summary_line_ids.input_margin')
    @api.depends('summary_line_ids', 'summary_line_ids.list', 'summary_line_ids.cost_dollar',
                 'summary_line_ids.selling_dollar', 'summary_line_ids.selling_sar', 'summary_line_ids.cost_discount',
                 'summary_line_ids.input_margin')
    def get_totals_of_table(self):
        for rec in self:
            lst_list = []
            lst_cost_dollar = []
            lst_selling_dollar = []
            lst_selling_sar = []
            if rec.summary_line_ids:
                for line in rec.summary_line_ids:
                    lst_list.append(line.list)
                    lst_cost_dollar.append(line.cost_dollar)
                    lst_selling_dollar.append(line.selling_dollar)
                    lst_selling_sar.append(line.selling_sar)
                rec.total_list = sum(lst_list)
                rec.total_cost_dollar = sum(lst_cost_dollar)
                rec.total_selling_dollar = sum(lst_selling_dollar)
                rec.total_selling_sar = sum(lst_selling_sar)
                rec.total_cost_sar = rec.total_cost_dollar * 3.75
                rec.gt_margin = rec.total_selling_sar - rec.total_cost_sar
            else:
                rec.total_list = 0.0
                rec.total_cost_dollar = 0.0
                rec.total_selling_dollar = 0.0
                rec.total_selling_sar = 0.0
                rec.total_cost_sar = 0.0
                rec.gt_margin = 0.0

    @api.depends('total_cost_dollar', 'total_selling_dollar', 'summary_line_ids')
    @api.onchange('total_cost_dollar', 'total_selling_dollar', 'summary_line_ids')
    def get_overall_total_margin(self):
        for rec in self:
            if rec.total_selling_dollar:
                rec.overall_margin_total = (1 - rec.total_cost_dollar / rec.total_selling_dollar) * 100

            else:
                rec.overall_margin_total = 0.0

    state = fields.Selection(string="State", selection=[('draft', 'Draft'), ('qoute_created', 'Created Quotation'), ],
                             default='draft', )
    partner_id = fields.Many2one(comodel_name="res.partner", string="Customer", required=False)
    project_id = fields.Many2one(comodel_name="project.project", string="Project")
    currency_id = fields.Many2one(comodel_name="res.currency", string="Currency", readonly=True, )
    company_id = fields.Many2one(comodel_name="res.company", string="", required=False,
                                 default=lambda self: self.env.company)
    cost_ids = fields.One2many(comodel_name="cost.calc.line", inverse_name="cost_calcu_id", string="Cost Calu Lines",
                               copy=True)
    # total_ids = fields.One2many(comodel_name="total.ext", inverse_name="total_ext_id", string="Totals By Partner",
    #                             readonly=True, copy=True)
    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Related Opportunity", required=False)

    # summary
    summary_line_ids = fields.One2many(comodel_name="summary.summary.line", inverse_name="cost_calculation_id",
                                       string="", )

    # smart buttons
    summary_count = fields.Integer(string="Summary", compute=get_summary_count, )
    quotation_count = fields.Integer(string="Qutation", compute=get_quotation_count)

    # boolean fields
    show_summary = fields.Boolean(string="", )

    # summary_page fields
    total_list = fields.Float(string="Total List", compute=get_totals_of_table, store=True)
    total_cost_dollar = fields.Float(string="Total Cost Dollar", compute=get_totals_of_table, store=True)
    total_cost_sar = fields.Float(string="Total Cost SAR", compute=get_totals_of_table, store=True)
    total_selling_dollar = fields.Float(string="Total Selling Dollar", compute=get_totals_of_table, store=True)
    total_selling_sar = fields.Float(string="Total Selling SAR", compute=get_totals_of_table, store=True)
    gt_margin = fields.Float(string="GT Margin", compute=get_totals_of_table, store=True)

    overall_margin_total = fields.Float(string="Total Overall Margin", readonly=True, compute=get_overall_total_margin,
                                        store=True
                                        )


class Cost_Calcu_line(models.Model):
    _name = 'cost.calc.line'
    _rec_name = 'id'

    def _get_invoice_line_sequence(self, new=0, old=0):
        """
        Method intended to be overridden in third-party module if we want to prevent the resequencing
        of invoice lines.

        :param int new:   the new line sequence
        :param int old:   the old line sequence

        :return:          the sequence of the SO line, by default the new one.
        """
        return new or old

    name = fields.Char(string="", required=False, )
    description = fields.Char(string="Description", required=False, readonly=False)
    sequence = fields.Integer(string='Sequence', default=10)
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id', readonly=True)
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], readonly=False, help="Technical field for UX purpose.")

    @api.depends('unit_list', 'qty')
    def get_ext_list(self):
        for rec in self:
            if rec.unit_list or rec.qty:
                rec.ext_list = rec.unit_list * rec.qty
            else:
                rec.ext_list = 0.0

    @api.depends('brand_id')
    def on_change_partner_get_discount_magrin_from_summary(self):
        for rec in self:
            if rec.brand_id:
                # get value from summary.summary.line
                summary = self.env['summary.summary'].search([], limit=1)
                for line in summary.summary_line_ids:
                    if line.brand_id == rec.brand_id:
                        rec.discount = line.cost_discount
                        rec.margin = line.input_margin
            else:
                rec.discount = 0.0
                rec.margin = 0.0

    @api.depends('unit_list', 'discount')
    def get_unit_d_cost(self):
        for rec in self:
            if rec.unit_list or rec.discount:
                rec.unit_d_cost = rec.unit_list * (1 - rec.discount / 100)
            else:
                rec.unit_d_cost = 0.0

    @api.depends('qty', 'unit_d_cost')
    def get_ext_d_cost(self):
        for rec in self:
            if rec.qty or rec.unit_d_cost:
                rec.ext_d_cost = rec.qty * rec.unit_d_cost
            else:
                rec.ext_d_cost = 0.0

    @api.depends('unit_d_cost', 'margin')
    def get_unit_selling(self):
        for rec in self:
            if rec.unit_d_cost or rec.margin:
                rec.unit_selling = rec.unit_d_cost / (1 - rec.margin / 100)
            else:
                rec.unit_selling = 0.0

    @api.depends('unit_selling', 'selling_qty')
    def get_ext_selling(self):
        for rec in self:
            if rec.unit_selling or rec.selling_qty:
                rec.ext_selling = rec.unit_selling * rec.selling_qty
            else:
                rec.ext_selling = 0.0

    @api.depends('unit_selling', 'cost_calcu_id.currency_id')
    @api.onchange('unit_selling', 'cost_calcu_id.currency_id')
    def get_unit_sar(self):
        for rec in self:
            if rec.unit_selling and rec.cost_calcu_id.currency_id:
                rec.unit_sar = rec.unit_selling / rec.cost_calcu_id.currency_id.rate
            else:
                rec.unit_sar = 0.0

    @api.depends('unit_sar', 'qty')
    def get_ext_sar(self):
        for rec in self:
            if rec.unit_sar or rec.qty:
                rec.ext_sar = rec.unit_sar * rec.qty
            else:
                rec.ext_sar = 0.0

    @api.onchange('product_id')
    def on_change_partner_id_get_brand(self):
        for rec in self:
            brands = self.env['product.product'].search([('id', '=', rec.product_id.id)]).brand_id
            if brands:
                rec.brand_id = brands.id
            else:
                rec.brand_id = False

            # return {'domain': {'product_id': [('id', 'in', product_list)]}}

    @api.onchange('product_id', 'brand_id')
    @api.depends('product_id', 'brand_id')
    def onchange_product_id_set_value_of_part_number(self):
        for rec in self:
            if rec.product_id:
                rec.part_num = rec.product_id.default_code
            else:
                rec.part_num = False

    # def unlink(self):
    #     for rec in self:
    #         if self.cost_calcu_id.state in ['draft']:
    #             summary = self.env['summary.summary'].search([], limit=1).summary_line_ids.search(
    #                 [('partner_id', '=', rec.partner_id.id)])
    #
    #             summary.update({
    #                 'list': 0.0,
    #                 'cost_dollar': 0.0,
    #                 'selling_dollar': 0.0,
    #                 'selling_sar': 0.0,
    #             })
    #     return super(Cost_Calcu_line, self).unlink()

    # @api.onchange('product_id')
    # def onchange_product_id_set_brand(self):
    #     for rec in self:
    #         if rec.product_id:
    #             rec.brand_id = rec.product_id.brand_id

    cost_calcu_id = fields.Many2one(comodel_name="cost.calc", string="Cost Calc ID", )

    brand_id = fields.Many2one(comodel_name="brand.config", string="Vendor/Brand", )
    product_id = fields.Many2one(comodel_name="product.product", string="Part NO.", )
    part_num = fields.Char(string="Product", compute=onchange_product_id_set_value_of_part_number, store=True)
    unit_list = fields.Float(string="U/List", )
    qty = fields.Float(string="C Qty", )
    ext_list = fields.Float(string="Ext List", compute=get_ext_list, store=True)  # unit_list * qty
    discount = fields.Float(string="Disc.", readonly=True)
    unit_d_cost = fields.Float(string="U/ D Cost", compute=get_unit_d_cost, store=True)  # unit_list * (1 - discount)
    ext_d_cost = fields.Float(string="Ext D Cost", compute=get_ext_d_cost, store=True)  # qty * unit_d_cost
    margin = fields.Float(string="Marg.", readonly=True)
    unit_selling = fields.Float(string="U/Sell", compute=get_unit_selling, store=True)  # unit_d_cost /(1 - margin)
    selling_qty = fields.Float(string="Sell Qty", readonly=True, related='qty')
    ext_selling = fields.Float(string="Ext Sell", compute=get_ext_selling, store=True)  # unit_selling * selling_qty
    unit_sar = fields.Float(string="U/SAR", compute=get_unit_sar, store=True)  # unit_selling * rate_of_currency
    ext_sar = fields.Float(string="Ext/SAR", compute=get_ext_sar, store=True)  # unit_sar * qty

# class Totals(models.Model):
#     _name = 'total.ext'
#
#     total_ext_id = fields.Many2one(comodel_name="cost.calc", string="Cost Calcu ID", )
#
#     brand_id = fields.Many2one(comodel_name="brand.config", string="Vendor/Brand", )
#     # Totals
#     total_ext_list = fields.Float(string="Total List", )
#     total_ext_d_cost = fields.Float(string="Total Ext D Cost", )
#     total_ext_selling = fields.Float(string="Total Ext Selling", )
#     total_ext_sar = fields.Float(string="Total Ext SAR", )
