from odoo import api, fields, models

class Product_Product_Inherit(models.Model):
    _inherit = 'product.product'

    custom_description = fields.Char(string="Description",related='product_tmpl_id.custom_description')


class Product_Template_Inherit(models.Model):
    _inherit = 'product.template'

    custom_description = fields.Char(string="Description",)

class Stock_quant_Inherit(models.Model):
    _inherit = 'stock.quant'

    custom_description = fields.Char(string="Custom Description",store=True,readonly=True,related='product_id.custom_description')


