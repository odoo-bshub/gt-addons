# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from json import dumps

import json
import re
import calendar
from datetime import datetime

from odoo.exceptions import UserError, ValidationError


class Target(models.Model):
    _inherit = 'hr.employee'

    monthly_target = fields.Float(string="Monthly Target")

    is_sale = fields.Boolean(string="is sales", compute='_is_sale_person')

    total_calculated = fields.Float(
        string="Total Calculated",  required=False, readonly=True, compute="_compute_totals", )
    total_earned = fields.Float(
        string="Total Earned",  required=False, readonly=True, compute="_compute_totals", )
    total_paid = fields.Float(
        string="Total Paid",  required=False, readonly=True, compute="_get_total_paid", )
    total_remained = fields.Float(
        string="Total Remained",  required=False, readonly=True, compute="_compute_total_remained")
    total_unpaid_commission = fields.Float(
        string="Total Unpaid Commission",  required=False, readonly=True, compute="_compute_totals",)

    def _compute_totals(self):
        emp_total_calculated = 0
        emp_total_earned = 0
        emp_total_unpaid_commission = 0
        for rec in self:
            employee_commissions = rec.env['commission.detail'].search(
                [('employee_id', '=', rec.id)])

            if employee_commissions:
                for comm in employee_commissions:
                    emp_total_calculated += comm.calculated_commission
                    emp_total_earned += comm.earned_commission
                    emp_total_unpaid_commission += comm.unpaid_commission

        rec.total_calculated = emp_total_calculated
        rec.total_earned = emp_total_earned
        rec.total_unpaid_commission = emp_total_unpaid_commission

    def _get_total_paid(self):
        emp_total_paid = 0
        for rec in self:
            employee_payslips = rec.env['hr.payslip'].search(
                [('employee_id', '=', rec.id), ('state', '=', 'done')])

            if employee_payslips:
                for payslip in employee_payslips:
                    bonus = payslip.line_ids.filtered(
                        lambda x: x.code == 'BON')

                    emp_total_paid += bonus.total
        rec.total_paid = emp_total_paid

    @api.onchange('total_earned', 'total_paid')
    def _compute_total_remained(self):
        self.total_remained = self.total_earned - self.total_paid

    def open_commission_details(self):
        return {
            'name': _('Commission Status'),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'domain': [('employee_id', '=', self.id)],
            'res_model': 'commission.detail',
        }

    # check the employee is sales or not
    @api.onchange('department_id')
    def _is_sale_person(self):
        if self.department_id.name.__eq__('Sales Dept'):
            self.is_sale = True
        else:
            self.is_sale = False


class CommissionDetails(models.Model):
    _name = 'commission.detail'

    employee_id = fields.Many2one('hr.employee', string='Employee', )
    month_year = fields.Char(
        string="Month/Year", required=False, compute="_compute_all_fields", store=True)
    target = fields.Float(string="Target", required=False,
                          related="employee_id.monthly_target", store=True)
    sales_amount = fields.Float(string="Sales", required=False, store=True)
    sales_untaxed = fields.Float(
        string="Sales Untaxed", required=False, store=True)
    sales_percent = fields.Float(
        string="Sales Percent", required=False, compute="_compute_all_fields")
    calculated_commission = fields.Float(
        string="Calculated Comm.", required=False, compute="_compute_all_fields", store=True)
    earned_commission = fields.Float(
        string="Earned Comm.", required=False,)
    paid_amount = fields.Float(
        string="Paid Amount", required=False,)

    paid_commission = fields.Float(
        string="Paid Comm.", required=False, )
    unpaid_commission = fields.Float(
        string="Unpaid Comm.", required=False,)
    calculate_flag = fields.Boolean(
        string="Calculate", required=False, default=True)

    total_cost = fields.Float(
        string="Total Cost", required=False, )

    net_sales = fields.Float(
        string="Net", compute="_compute_all_fields", store=True)

    def _compute_all_fields(self):
        for rec in self:
            # note we used total_cost is negative amount we need
            rec.net_sales = rec.sales_untaxed + rec.total_cost
            if rec.target > 0:
                rec.sales_percent = (rec.net_sales / rec.target) * 100
                if 0 <= rec.sales_percent <= 60:
                    rec.calculated_commission = 0.0
                elif 61 <= rec.sales_percent <= 80:
                    rec.calculated_commission = 0.07 * \
                        (rec.sales_untaxed + rec.total_cost)
                elif 81 <= rec.sales_percent < 150:
                    rec.calculated_commission = 0.1 * \
                        (rec.sales_untaxed + rec.total_cost)
                elif rec.sales_percent >= 150:
                    # 1- first step is to calculate commission of 150 as normal 10%
                    main_commssion_amount = rec.target * 1.5 * 0.1
                    # 2- second step is to calculate the amount above 150% of target
                    current_net_above_150 = rec.net_sales - (rec.target * 1.5)
                    # 3- third step is to calculate the extra commission (0.15 * amount above 150% of target)
                    extra_commission_amount = current_net_above_150 * 0.15
                    # 4- last step is to calculate the sum of both commissions
                    rec.calculated_commission = main_commssion_amount + extra_commission_amount

    def create_records(self):
        employees = self.env['hr.employee'].search(
            [('monthly_target', '>', 0)])
        month_year = str(datetime.today().month)+'/'+str(datetime.today().year)
        for employee in employees:
            comission_exist = self.env['commission.detail'].search(
                [('month_year', '=', month_year), ('employee_id', '=', employee.id)])
            if not comission_exist:
                self.env['commission.detail'].create({
                    'target': employee.monthly_target,
                    'employee_id': employee.id,
                    'month_year': month_year,
                })
            # else:

    def create_previous_records(self):
        employees = self.env['hr.employee'].search(
            [('monthly_target', '>', 0)])
        current_opt_emp = []
        # print('employee', employees)
        for employee in employees:
            employee_operations_month_1 = self.env['freight.operation'].search(
                [('datetime', '>=', datetime.strptime('jan 1 2021 12:00AM', '%b %d %Y %I:%M%p').date()),
                 ('datetime', '<=', datetime.strptime('jan 31 2021 11:59PM', '%b %d %Y %I:%M%p').date()), ('employee_id', '=', employee.id)])
            self.compute_previous(
                employee_operations_month_1, employee, '1/2021')

            employee_operations_month_2 = self.env['freight.operation'].search(
                [('datetime', '>=', datetime.strptime('feb 1 2021 12:00AM', '%b %d %Y %I:%M%p').date()),
                 ('datetime', '<=', datetime.strptime('feb 28 2021 11:59PM', '%b %d %Y %I:%M%p').date()), ('employee_id', '=', employee.id)])
            self.compute_previous(
                employee_operations_month_2, employee, '2/2021')

            employee_operations_month_3 = self.env['freight.operation'].search(
                [('datetime', '>=', datetime.strptime('mar 1 2021 12:00AM', '%b %d %Y %I:%M%p').date()),
                 ('datetime', '<=', datetime.strptime('mar 31 2021 11:59PM', '%b %d %Y %I:%M%p').date()), ('employee_id', '=', employee.id)])
            self.compute_previous(
                employee_operations_month_3, employee, '3/2021')

            employee_operations_month_4 = self.env['freight.operation'].search(
                [('datetime', '>=', datetime.strptime('apr 1 2021 12:00AM', '%b %d %Y %I:%M%p').date()),
                 ('datetime', '<=', datetime.strptime('apr 30 2021 11:59PM', '%b %d %Y %I:%M%p').date()), ('employee_id', '=', employee.id)])
            self.compute_previous(
                employee_operations_month_4, employee, '4/2021')

            employee_operations_month_5 = self.env['freight.operation'].search(
                [('datetime', '>=', datetime.strptime('may 1 2021 12:00AM', '%b %d %Y %I:%M%p').date()),
                 ('datetime', '<=', datetime.strptime('may 31 2021 11:59PM', '%b %d %Y %I:%M%p').date()), ('employee_id', '=', employee.id)])
            self.compute_previous(
                employee_operations_month_5, employee, '5/2021')

    def compute_previous(self, opts, emp, month_year):
        total_invoice_amount = 0.0
        total_invoice_tax_excluded = 0.0
        total_invoice_residual = 0.0
        total_invoice_paid_amount = 0.0
        all_operations_total_cost = 0.0

        for opt in opts:
            for tt in opt.invoice_created_ids:
                if tt.name.startswith('INV'):
                    total_invoice_amount += tt.amount_total_signed  # sales_amount
                    total_invoice_tax_excluded += tt.amount_untaxed_signed  # sales_untaxed
                    total_invoice_residual += tt.amount_residual_signed
                else:
                    all_operations_total_cost += tt.amount_untaxed_signed  # total_cost
        total_invoice_paid_amount += total_invoice_amount - total_invoice_residual

        commission = self.env['commission.detail'].search(
            [('employee_id.id', '=', emp.id), ('month_year', '=', month_year)], limit=1, order='id desc')
        if not commission:
            self.env['commission.detail'].create({
                'target': emp.monthly_target,
                'employee_id': emp.id,
                'month_year': month_year,
                'sales_amount': total_invoice_amount,
                'sales_untaxed': total_invoice_tax_excluded,
                'paid_amount': total_invoice_paid_amount,
                # 'calculated_commission': calculated_commission,
                'total_cost': all_operations_total_cost
            })


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    def post(self):
        res = super(AccountPayment, self).post()
        current_payment = 0
        done_operations = []

        invoice_id = self.env['account.move'].search(
            [('name', '=', self.communication)])

        if (type(invoice_id.invoice_date) == bool):
            return res

        month_year = str(invoice_id.invoice_date.month) + \
            '/' + str(invoice_id.invoice_date.year)

        date_range = calendar.monthrange(
            invoice_id.invoice_date.year, invoice_id.invoice_date.month)
        start_date = str(invoice_id.invoice_date.year) + "-" + \
            str(invoice_id.invoice_date.month)+"-1"
        end_date = str(invoice_id.invoice_date.year) + "-" + \
            str(invoice_id.invoice_date.month)+"-"+str(date_range[1])

        # getting all operations in same month of current payment invoice
        all_operations = self.env['freight.operation'].search(
            [('employee_id.id', '=', invoice_id.employee_id.id),
             ('datetime', '>=', start_date),
             ('datetime', '<=', end_date)])

        # currency_id (payment currency)
        current_payment = self.amount * self.currency_id.rate

        total_invoice_amount = 0.0
        total_invoice_tax_excluded = 0.0
        total_invoice_residual = 0.0
        total_invoice_paid_amount = 0.0
        total_earned_commission = 0.0

        for t in all_operations:
            operation_invoice_amount = 0.0
            operation_invoice_tax_ex_amount = 0.0
            operation_invoice_residual = 0.0
            operation_invoice_paid_amount = 0.0

            all_invoices = self.env['account.move'].search(
                [('operation_id.id', '=', t.id), ('type', '=', 'out_invoice'), ('state', '=', 'posted')])

            for invoice in all_invoices:
                operation_invoice_amount += invoice.amount_total_signed
                operation_invoice_tax_ex_amount += invoice.amount_untaxed_signed
                operation_invoice_residual += invoice.amount_residual_signed

                if invoice.invoice_payment_state == 'paid':
                    operation_invoice_paid_amount += invoice.amount_total_signed
                elif invoice.invoice_payment_state == 'not_paid':
                    paid = invoice.amount_total_signed - invoice.amount_residual_signed  # +
                    operation_invoice_paid_amount += abs(paid)

            if operation_invoice_amount - operation_invoice_paid_amount == 0:
                done_operations.append({
                    'id': t.id,
                    'operation_invoice_tax_ex_amount': operation_invoice_tax_ex_amount,
                })

            total_invoice_paid_amount += operation_invoice_paid_amount
            total_invoice_residual += operation_invoice_residual
            total_invoice_amount += operation_invoice_amount
            total_invoice_tax_excluded += operation_invoice_tax_ex_amount

        commission = self.env['commission.detail'].search(
            [('employee_id.id', '=', invoice_id.employee_id.id), ('month_year', '=', month_year)], limit=1, order='id desc')

        if commission:
            commission.sales_amount = total_invoice_amount
            commission.sales_untaxed = total_invoice_tax_excluded
            commission.paid_amount = total_invoice_paid_amount
            commission._compute_all_fields()

            if commission.calculated_commission > 0:
                if done_operations:
                    for done in done_operations:
                        total_earned_commission += (done['operation_invoice_tax_ex_amount'] /
                                                    total_invoice_tax_excluded) * commission.calculated_commission

                commission.earned_commission = total_earned_commission
                commission.unpaid_commission = commission.earned_commission - \
                    commission.paid_commission
                # Rashad - visit this later to target only payslips of current month not the invoice month
                # Rashad - visit this later to add the invoice payment to bonus of this month if not paid yet
                # current_month_payslip = self.env['hr.payslip'].search([
                #     ('date_from', '>=', start_date), ('date_from', '<=', end_date), ('employee_id.id', '=', invoice_id.employee_id.id)])

                # if current_month_payslip :
                #     for line in current_month_payslip.line_ids:
                #         if line.code == 'BON':
                #             commission.paid_commission = line.total
                #     commission.unpaid_commission = commission.earned_commission - \
                #         commission.paid_commission

        return res


class HRPayslipInherit(models.Model):
    _inherit = "hr.payslip"

    @api.model
    def write(self, values):
        res = super(HRPayslipInherit, self).write(values)
        if self.state == 'done':
            commissions = self.env['commission.detail'].search(
                [('employee_id.id', '=', self.contract_id.employee_id.id), ('unpaid_commission', '>', 0)],)
            print(commissions)
            for rec in commissions:
                rec.paid_commission += rec.unpaid_commission
                rec.unpaid_commission = 0
        return res


class AccountMove(models.Model):
    _inherit = 'account.move'

    def action_post(self):
        res = super(AccountMove, self).action_post()
        for move in self:
            if move.type != 'out_invoice' or move.type != 'in_invoice':
                continue

            month_year = str(move.operation_id.create_date.month) + \
                '/' + str(move.operation_id.create_date.year)
            commissions = move.env['commission.detail'].search(
                [('employee_id.id', '=', move.operation_id.employee_id.id), ('month_year', '=', month_year)])
            if move.type == 'out_invoice':
                if commissions:
                    # print("move.amount_total_signed", move.amount_total_signed)
                    commissions.sales_amount += move.amount_total_signed
                    commissions.sales_untaxed += move.amount_untaxed_signed
                    # commissions.total_cost = all_operations_total_cost
                else:
                    move.env['commission.detail'].create({
                        'employee_id': move.operation_id.employee_id.id,
                        'month_year': month_year,
                        'sales_amount': move.amount_total,
                        'sales_untaxed': move.amount_untaxed_signed,
                    })
            if move.type == 'in_invoice':
                # print("move.amount_total_signed", move.amount_total_signed)
                if commissions:
                    commissions.total_cost += move.amount_untaxed_signed
                else:
                    move.env['commission.detail'].create({
                        'employee_id': move.operation_id.employee_id.id,
                        'month_year': month_year,
                        'total_cost': move.amount_untaxed_signed,
                    })

        return res


class FreightChanger(models.Model):
    _inherit = 'freight.operation'

    sales_target = fields.Float(
        string="Sales Monthly Target", required=False, related='employee_id.monthly_target')
