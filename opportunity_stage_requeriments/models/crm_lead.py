from odoo import api, models, _, fields
from odoo.exceptions import UserError,ValidationError


class Lead(models.Model):
    _inherit = 'crm.lead'

    @api.depends('stage_id', 'is_propose_with_risk')
    def set_stage_probability(self):
        for rec in self:
            if rec.is_propose_with_risk and rec.stage_id.id == 6:
                rec.probability = 25

            elif rec.stage_id.probability and not rec.is_propose_with_risk:
                rec.probability = rec.stage_id.probability
            elif rec.stage_id.probability:
                rec.probability = rec.stage_id.probability

    def action_open_attachments(self):
        self.ensure_one()
        self.is_attach = True
        return {
            'name': _('Digital Attachments'),
            'domain': [('res_model', '=', self._name),
                       ('res_id', '=', self.id), ],
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'context': "{'default_res_model': '%s','default_stage_attach_id': '%s','default_res_id': %d, "
                       "'default_product_downloadable': True}" % (
                self._name,self.stage_id.name, self.id),
            'help': """
                    <p class="o_view_nocontent_smiling_face">Add attachments for this digital product</p>
                    <p>The attached files are the ones that will be purchased and sent to the customer.</p>
                    """,
        }

    def action_open_attachments_won(self):
        self.ensure_one()
        self.is_attach_contract = True
        return {
            'name': _('Digital Attachments'),
            'domain': [('res_model', '=', self._name),
                       ('res_id', '=', self.id), ],
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'context': "{'default_res_model': '%s','default_stage_attach_id': '%s','default_res_id': %d, "
                       "'default_product_downloadable': True}" % (
                self._name,self.stage_id.name, self.id),
            'help': """
                       <p class="o_view_nocontent_smiling_face">Add attachments for this digital product</p>
                       <p>The attached files are the ones that will be purchased and sent to the customer.</p>
                       """,
        }

    def manger_qualify(self):
        template_id = self.env.ref(
            'opportunity_stage_requeriments'
            '.lead_sale_person_need_permission_mail'
            '').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(self.id, force_send=True)

    @api.depends('pre_sales')
    def send_mail_pre_sales(self):
        if self.pre_sales:
            self.ensure_one()
            template_id = self.env.ref(
                'opportunity_stage_requeriments.message_user_assigned_pre_sales').id
            ctx = {
                'default_model': 'crm.lead',
                'default_res_id': self.id,
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'custom_layout': 'mail.mail_notification_light',
            }
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'target': 'new',
                'context': ctx,
            }

    @api.depends('is_engaged')
    def send_mail_vendor(self):
        if self.is_engaged == True:
            self.ensure_one()
            template_id = self.env.ref(
                'opportunity_stage_requeriments.lead_vendor_mail_template').id
            ctx = {
                'default_model': 'crm.lead',
                'default_res_id': self.id,
                # 'default_stage_attach_id': self.id.stage_id.name,
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'custom_layout': 'mail.mail_notification_light',
            }
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'target': 'new',
                'context': ctx,
            }

    @api.depends('pre_sales')
    def action_notify(self):
        for rec in self:
            body_html = self.env.ref(
                'opportunity_stage_requeriments.lead_vendor_mail_template'
                '')._render_field('body_html', rec.ids, compute_lang=True)[
                rec.id]
            rec.message_notify(
                body=body_html,
                partner_ids=[rec.pre_sales.user_id.id],
                subtype_xmlid='mail.mt_comment',
                email_layout_xmlid='mail.mail_notification_light',
            )
        # for rec in self:
        #     rec.pre_sales.user_id.notify_success("")

    def managing_director_approve_template_email_to(self):
        email_list=[]
        users=self.env['res.users'].search([])
        for user in users:
            if user.has_group('opportunity_stage_requeriments.managing_dr_group'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
                else:
                    raise UserError('One or more Managing Directors have not email address')
        return ",".join(email_list)

    def business_unit_approve_template_email_to(self):
        email_list=[]
        users=self.env['res.users'].search([])
        for user in users:
            if user.has_group('opportunity_stage_requeriments.business_unit_group'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
                else:
                    raise UserError('One or more Sales Managers have not email address')
        return ",".join(email_list)

    def sales_admin_approve_template_email_to(self):
        email_list=[]
        users=self.env['res.users'].search([])
        for user in users:
            if user.has_group('crm_approval.group_data_correct'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
                else:
                    raise UserError('One or more Sales Admin have not email address')
        return ",".join(email_list)


    @api.constrains('stage_id')
    def _onchange_stage_id_check(self):
        """
        Check that the opportunity can change from one stage to another
        depending on if it has data of the requirements that each one requires.

        :raises: UserError
        """
        if self.stage_id.new_prospect:
            if self.is_new != True and self.type_new in ['full'] :
                err_msg = _("""
                You can not change status if the opportunity lacks:
                - Must select All check Box is Selected .
                            """)
                raise UserError(_(err_msg))

        if self.stage_id.new_prospect:
            if self.is_new_rfp != True and self.type_new in ['rfp'] :
                err_msg = _("""
                You can not change status if the opportunity lacks:
                - Must select All check Box is Selected .
                            """)
                raise UserError(_(err_msg))

        if self.stage_id.approval:
            if self.is_clicked_approve_sales_manager != True and self.type_new in ['full'] :
                err_msg_amount = _("""
                You can not change status if the opportunity lacks:
                - Some Approval.
                                   """)
                raise UserError(_(err_msg_amount))

        if self.stage_id.qualify:
            if self.is_qualify != True and self.type_new in ['full','rfp'] :
                err_msg_amount = _("""
                You can not change status if the opportunity lacks:
                - Must select All check Box is Selected .
                                   """)
                raise UserError(_(err_msg_amount))

        if self.stage_id.disqualify:
            if self.is_disqualify != True and self.type_new == 'full':
                err_msg_amount = _("""
                You can not change status if the opportunity lacks:
                - Must select All check Box is Selected .
                                   """)
                raise UserError(_(err_msg_amount))

        if self.stage_id.develop:
            if self.is_develop != True and self.type_new == 'full':
                err_msg_amount = _("""
                You can not change status if the opportunity lacks these points:
                - Must select All check Box is Selected .
                - Must upload or create new cost sheet related to this opportunity. 
                                   """)
                raise UserError(_(err_msg_amount))


        if self.stage_id.develop:
            if self.is_develop_rfp != True and self.type_new == 'rfp' :
                err_msg_amount = _("""
                You can not change status to Propose if the opportunity lacks:
                - Must select All check Box is Selected .
                - Must upload or create new cost sheet related to this opportunity. 

                                   """)
                raise UserError(_(err_msg_amount))

        if self.stage_id.proposal:
            # if self.type_new == 'full':
            attach = self.env['ir.attachment'].search(
                [('res_model', '=', 'crm.lead'),
                 ('res_id', '=', self.id)])
            if not attach:
                err_attachment = _("""
                                You can not change status if the opportunity lacks:
                                 - attached Financial or Technical Proposal 
                                 document.
                                """)
                raise UserError(_(err_attachment))

        if self.stage_id.contract:
            # if self.type_new == 'full':
            attach_won = self.env['ir.attachment'].search(
                [('res_model', '=', 'crm.lead'),
                 ('res_id', '=', self.id),('stage_attach_id','=','Negotiation')])
            if not attach_won :
                err_attachment = _("""
                You can not change status if the opportunity lacks:
                - attached PO Contract or LOI Attachment 
                document.
                                         """)
                raise UserError(_(err_attachment))

            self.project_manager_id = self.env['res.users'].search([('login','=','y.albarazi@gt-ict.com')],limit=1).id


        if self.stage_id.negotiation:
            if not self.is_cust_engaged:
                err_attachment = _("""
                You can not change status if the opportunity lacks:
                - Must select All check Box is Selected .

                                         """)
                raise UserError(_(err_attachment))

        if self.stage_id.handover:
            if not self.is_hand_over:
                err_msg_amount = _("""
                You can not change status to Propose if the opportunity lacks:
                - Must contain Hand Over Task .
                                   """)
                raise UserError(_(err_msg_amount))


    @api.depends('probability', 'expected_revenue')
    def compute_weight_value(self):
        for rec in self:
            if rec.weight or not rec.weight:
                rec.weight = (rec.probability * rec.expected_revenue) / 100

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def get_constrain_new_stage(self):
        for rec in self:
            if rec.is_new or not rec.is_new:
                if rec.is_budget == True and rec.is_frame == True and \
                        rec.is_project == True \
                        and rec.is_solution == True and \
                        rec.state_qualify == 'qualify' and rec.is_approved_data_from_sales_admin == True:
                    rec.is_new = True

    is_new = fields.Boolean(string="", compute=get_constrain_new_stage)

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def get_constrain_new_stage_rfp(self):
        for rec in self:
            if rec.is_new_rfp or not rec.is_new_rfp:
                if rec.state_qualify == 'qualify':
                    rec.is_new_rfp = True

    is_new_rfp = fields.Boolean(string="", compute=get_constrain_new_stage_rfp)

    @api.onchange('stage_id', 'is_aware', 'is_engaged', 'is_dist', 'is_deal',
                  'is_deal_id')
    @api.depends('stage_id', 'is_aware', 'is_engaged', 'is_dist', 'is_deal',
                 'is_deal_id')
    def get_constrain_qualify_stage(self):
        for rec in self:
            if rec.is_qualify or not rec.is_qualify:
                if rec.is_aware == True and rec.is_engaged == True and rec.is_dist == True \
                        and rec.is_deal == True and rec.is_deal_id == True:
                    rec.is_qualify = True

    is_qualify = fields.Boolean(string="", compute=get_constrain_qualify_stage)

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def get_constrain_develop_stage(self):
        for rec in self:
            if rec.is_develop or not rec.is_develop:
                if rec.is_cust_req == True and rec.is_propose == True and rec.is_verify == True \
                        and rec.is_sheet == True and self.cost_calc_count >= 1:
                    print(self.cost_calc_count)
                    rec.is_develop = True

    is_develop = fields.Boolean(string="", compute=get_constrain_develop_stage)

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def get_constrain_develop_stage_rfp(self):
        for rec in self:
            if rec.is_develop_rfp or not rec.is_develop_rfp:
                if rec.is_cust_req == True and rec.is_propose == True and rec.is_etimed == True \
                        and rec.is_sheet == True and self.cost_calc_count >= 1:

                    rec.is_develop_rfp = True
    is_develop_rfp = fields.Boolean(string="", compute=get_constrain_develop_stage_rfp)

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def get_constrain_disqualify_stage(self):
        for rec in self:
            if rec.is_disqualify or not rec.is_disqualify:
                if rec.is_budget == True and rec.is_frame == True and rec.is_project == True and rec.is_solution == True and rec.state_qualify == 'disqualify' and rec.reason:
                    rec.is_disqualify = True

    is_disqualify = fields.Boolean(string="",
                                   compute=get_constrain_disqualify_stage)

    def set_state_to_ready_for_approve(self):
        for rec in self:
            if rec.state_qualify == 'disqualify' and rec.reason:
                if rec.type_new == 'full':
                    if rec.is_budget == True and rec.is_frame == True and rec.is_project == True and rec.is_solution == True:
                        rec.stage_id = rec.stage_id.search([('name','=','Approval')]).id

                        template_id = self.env.ref('opportunity_stage_requeriments.template_mail_approve_presales_activity').id
                        template = self.env['mail.template'].browse(template_id)
                        template.sudo().send_mail(self.id, force_send=True)
                    else:
                        err_msg_amount = _("""
                                        You can not change status if the opportunity lacks:
                                        - Must select All check Box is Selected .
                                                           """)
                        raise UserError(_(err_msg_amount))
                else:
                    rec.stage_id = rec.stage_id.search([('name', '=', 'Approval')]).id
                    template_id = self.env.ref(
                        'opportunity_stage_requeriments.template_mail_approve_presales_activity').id
                    template = self.env['mail.template'].browse(template_id)
                    template.sudo().send_mail(self.id, force_send=True)

            else:
                err_msg_amount = _("""
                                     You can not change status if the opportunity lacks:
                                     - Must select Disqualify and write reason of disqualify.
                                                                           """)
                raise UserError(_(err_msg_amount))

    @api.onchange('stage_id')
    @api.depends('stage_id')
    def check_the_state_approval(self):
        for rec in self:
            if rec.stage_id.id == 10 :
                rec.is_stage_approval = True
            else:
                rec.is_stage_approval = False

    def get_login_user(self):
        for rec in self:
            rec.login_user_id = self.env.user
            if rec.login_user_id == rec.team_leader_id:
                rec.is_login_user_same_team_leader = True
            else:
                rec.is_login_user_same_team_leader = False

            if rec.is_stage_approval and rec.is_approved_by_presales and rec.is_login_user_same_team_leader:
                rec.is_appear_button_sales_manager_approve = True
            else:
                rec.is_appear_button_sales_manager_approve = False

    @api.onchange('team_id')
    def on_change_team_id_appear_sales_manager_button(self):
        for rec in self:
            if rec.is_stage_approval and rec.is_approved_by_presales and rec.is_login_user_same_team_leader:
                rec.is_appear_button_sales_manager_approve = True
            else:
                rec.is_appear_button_sales_manager_approve = False

    def approve_for_presales(self):
        for rec in self:
            rec.is_approved_by_presales = True
            rec.current_approve = 'Sales Manager Approval'
            template_id = self.env.ref('opportunity_stage_requeriments.sales_manager_approval_mail_template').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)
            if rec.team_id.user_id :
                rec.team_leader_id = rec.team_id.user_id
            else:
                err_msg_amount = _("""
                                    You should set team leader for sales team
                                                                                           """)
                raise UserError(_(err_msg_amount))
            if rec.is_stage_approval and rec.is_approved_by_presales and rec.is_login_user_same_team_leader:
                rec.is_appear_button_sales_manager_approve = True


    def approve_for_sales_manager(self):
        for rec in self:
            rec.is_clicked_approve_sales_manager= True
            rec.stage_id = 2
            template_id = self.env.ref(
                'opportunity_stage_requeriments.template_mail_approve_sales_manager').id
            template = self.env['mail.template'].browse(template_id)
            template.sudo().send_mail(self.id, force_send=True)
            rec.current_approve = ''


    #When SEND FOR APPROVAL Button will appear
    @api.onchange('state_qualify')
    def on_change_state_qualify_appear_button(self):
        for rec in self:
            if rec.state_qualify == 'disqualify' and rec.stage_id.id in [1] :
                rec.is_state_disqualify = True
            else:
                rec.is_state_disqualify = False


    def presales_approve_template_email_to(self):
        email_list=[]
        users=self.env['res.users'].search([])
        for user in users:
            if user.has_group('opportunity_stage_requeriments.group_presales_team'):
                if user.partner_id.email:
                    email_list.append(user.partner_id.email)
                else:
                    raise UserError('One or more presales persons have not email address')
        print(email_list)
        return ",".join(email_list)

    probability = fields.Float(
        'Probability', group_operator="avg", copy=False, default=5,
        compute='set_stage_probability', readonly=True, )

    attachment_ids = fields.Many2many(
        'ir.attachment', string='Proposal Financial or Technical',
        help="Financial Or Technical "
             "Proposal")

    def set_domain(self):
        lst =[]
        users = self.env['res.users'].search([])
        for user in users:
            if user.has_group('project.group_project_manager'):
                lst.append(user.id)
        return [('id','in',lst)]

    @api.constrains('budget','is_budget')
    def _constraint_methods_for_budget(self):
        for rec in self:
            if rec.is_budget and rec.budget <= 0 :
                raise ValidationError('Budget Must be greater than 0')

    def action_create_task(self):
        """ button to run action """
        if not self.is_hand_over  :
            raise ValidationError(
                "You Must Check all the Handover task requirements before you can create handover task")
        else:
            self.ensure_one()
            action = \
                self.env.ref('project.action_view_task').read()[
                    0]
            action['context'] = {
                'default_name': 'HandOver : ' + self.name or False,
                'default_opportunity_id': self.id or
                                          False,
                'default_partner_id': self.partner_id.id or False,
                'search_default_my_tasks': 1 or False,
                'default_count': self.count,
                'default_user_id': self.project_manager_id.id,
                'default_is_created_task': True,
                # 'default_project_id': self.custom_project_id.id or False,
                'default_project_id': self.project_created_from_crm_id.id or False,
            }
            action['views'] = [(
                self.env.ref('project.view_task_form2').id,
                'form'
            )]
            # self.update({
            #     'stage_id': 4
            # })
            print(self.stage_id.name)
            print(self.next_stage_id.name)
            # print(self._context.get('active_id'))
            template_id = self.env.ref(
                'abs_opportunity_task.template_mail_for_project_handover_task').id
            template = self.env['mail.template'].browse(template_id)
            template.send_mail(self.id, force_send=True)

            if self.overall_margin < 20.0 :
                self.is_dm_approval_required = True
                self.is_bum_approval_required = False
                self.is_sales_admin_approval_required = False
                template_id = self.env.ref('opportunity_stage_requeriments.template_mail_for_managing_director_approval').id
                template = self.env['mail.template'].browse(template_id)
                template.sudo().send_mail(self.id, force_send=True)

            elif 20.0 <= self.overall_margin <= 25.0:
                self.is_bum_approval_required = True
                self.is_dm_approval_required = False
                self.is_sales_admin_approval_required = False
                template_id = self.env.ref(
                    'opportunity_stage_requeriments.template_mail_for_business_unit_approval').id
                template = self.env['mail.template'].browse(template_id)
                template.sudo().send_mail(self.id, force_send=True)

            # elif self.overall_margin > 25.0:
            #     self.is_sales_admin_approval_required = True
            #     self.is_bum_approval_required = False
            #     self.is_dm_approval_required = False
            #     template_id = self.env.ref(
            #         'opportunity_stage_requeriments.template_mail_for_sales_admin_approval').id
            #     template = self.env['mail.template'].browse(template_id)
            #     template.sudo().send_mail(self.id, force_send=True)

            return action


    is_attach = fields.Boolean(string="", )
    is_attach_contract = fields.Boolean(string="", )

    pre_sales = fields.Many2one(comodel_name="res.users", string="Pre-Sales",
                                required=False, )
    pre_sales_manager = fields.Many2one(comodel_name="res.users", string="PreSales Manager",
                                required=False, )

    pre_sales_second = fields.Many2one(comodel_name="res.users",
                                       string="2nd Pre-Sales",
                                       required=False, )

    sectoer_id = fields.Many2one(comodel_name="sectoers.sectoers",
                                 string="Sectors", required=False, )

    solutions_id = fields.Many2one(comodel_name="solution.solution",
                                   string="Solutions", )

    collection_value = fields.Monetary(string="Collection Value",
                                       required=False, )
    collection_date = fields.Date(string="Collection Date", required=False, )
    currency_id = fields.Many2one('res.currency', readonly=True,
                                  related='company_id.currency_id', )
    competitor = fields.Char(string="Competitor", required=False, )
    weight = fields.Monetary(string="Weighted Value",
                             compute=compute_weight_value)

    """" fields required for first stage"""

    is_budget = fields.Boolean(string="- Customer has Budget", )
    is_frame = fields.Boolean(string="- Customer has Time Frame ", )
    is_project = fields.Boolean(string="- Customer has Pain ", )
    is_solution = fields.Boolean(string="- Customer has Requirements", )
    # is_permission = fields.Boolean(string="- Processed to Permission", )

    """" fields required for second stage"""

    is_aware = fields.Boolean(string="- Vendor has Aware of the opportunity", )
    is_engaged = fields.Boolean(
        string="- Vendor hasn't Engaged with another partner ", )
    is_dist = fields.Boolean(string="- Dist. Aware of Opportunity ", )
    is_deal = fields.Boolean(string="- Deal Registration", )
    is_deal_id = fields.Boolean(string="- Send Deal ID", )
    registration = fields.Text(string="Registration ID", required=False, )

    """" fields required for third stage"""

    is_cust_req = fields.Boolean(string="- Study Customer Requirement ", )
    is_propose = fields.Boolean(string="- Propose Solution  ", )
    is_verify = fields.Boolean(string="- Verify Solution with Customer  ", )
    is_sheet = fields.Boolean(string="- Generate Initial Cost Sheet", )
    is_etimed = fields.Boolean(string="- Upload Technical & Financial Offer on Etimed.", )

    """" fields required for third stage"""

    attachments_ids = fields.Many2many(
        'ir.attachment', 'attachment_id', string='Contract', \
        help="PO/Contract/LOI attachment")

    """" fields required for four stage"""

    is_cust_engaged = fields.Boolean(string="- Engaged with Customer", )

    """" fields required for lost stage"""
    lost_ids = fields.Many2many(comodel_name="ir.attachment",
                                relation="lost_attachment",
                                string="Lost Report", )

    budget = fields.Monetary(string=" Budget Value", required=False, )
    date_frame = fields.Date(string="Date", required=False, )
    pain = fields.Text(string="Describe Pain", required=False, )
    require = fields.Text(string="Set Requirements", required=False, )

    due_date = fields.Date(string="Submission Due Date", required=True, )
    ref_number = fields.Char(string="Reference Number", required=False, )
    ref_fee = fields.Monetary(string=" RFP Fee",required=False, )

    project_manager_id = fields.Many2one(comodel_name="res.users",
                                         string="Project Manager",
                                         required=False, domain=set_domain)

    is_propose_with_risk = fields.Boolean(string="Propose With Risk", )

    scope = fields.Selection(string="Project Scope",
                             selection=[('supply', 'Supply Only'),
                                        (
                                        'sup-install', 'Supply & Installation'),
                                        ('install', 'Installation'),
                                        ], required=True, default='supply' )
    summary = fields.Text(string="Scope Summary", )

    reason = fields.Text(string="Disqualify Reason", )
    state_qualify = fields.Selection(string="", selection=[('qualify','Qualify'),
                                                           ('disqualify','Disqualify'),],default='qualify', )

    login_user_id = fields.Many2one(comodel_name="res.users", string="Login user", readonly=True,compute=get_login_user,default=lambda self : self.env.user)

    team_leader_id = fields.Many2one(comodel_name="res.users",string="Sales Team Leader",related='team_id.user_id')
    is_stage_approval = fields.Boolean(string="",compute= check_the_state_approval,store=True)
    is_state_disqualify = fields.Boolean(string="",)
    is_approved_by_presales = fields.Boolean(string="",)
    is_login_user_same_team_leader = fields.Boolean(string="", compute=get_login_user,store=True, )
    is_appear_button_sales_manager_approve = fields.Boolean(string="",)
    is_clicked_approve_sales_manager = fields.Boolean(string="",)

    def approve_for_direct_manager_on_margin(self):
        self.is_dm_approved = True

    def approve_for_bum_on_margin(self):
        self.is_bum_approved = True

    def approve_for_sales_admin_on_margin(self):
        self.is_sales_admin_approved = True

    is_dm_approval_required = fields.Boolean(string="",readonly=True  )
    is_dm_approved = fields.Boolean(string="",  readonly=True)
    is_bum_approval_required = fields.Boolean(string="",  readonly=True)
    is_bum_approved = fields.Boolean(string="", readonly=True )
    is_sales_admin_approval_required= fields.Boolean(string="", readonly=True )
    is_sales_admin_approved = fields.Boolean(string="",readonly=True  )

    def create_project(self):
        """ button to run action """
        self.ensure_one()
        action = \
            self.env.ref('project.open_view_project_all').sudo().read()[
                0]
        action['context'] = {
            'default_name': self.name or False,
            'default_user_id': self.project_manager_id.id,
            # 'default_user_id': self.project_manager_id.id,

        }
        action['views'] = [(
            self.env.ref('project.edit_project').id,
            'form'
        )]
        return action

    @api.model
    def get_email_to(self):
        email_list = []
        user_group = self.env.ref(
            "opportunity_stage_requeriments.group_financial_team")
        email_list = [
            usr.partner_id.email for usr in user_group.users if
            usr.partner_id.email]
        print(email_list)
        return ",".join(email_list)

    # @api.model
    def get_email_to_disqualify(self):
        email_list = []
        user_group = self.env.ref("sales_team.group_sale_manager")
        email_list = [
            usr.partner_id.email for usr in user_group.users if
            usr.partner_id.email]
        print(email_list)
        return ",".join(email_list)


    def presales_after_data_correct_approve_template_email_to(self):
        # email_list_gp_1 = []
        # email_list_gp_2 = []
        # user_group_1 = self.env.ref("sales_team.group_sale_manager")
        # user_group_2 = self.env.ref("opportunity_stage_requeriments.group_presales_team")
        # email_list_gp_1 = [
        #     usr.partner_id.email for usr in user_group_1.users if
        #     usr.partner_id.email]
        # email_list_gp_2 = [
        #     usr.partner_id.email for usr in user_group_2.users if
        #     usr.partner_id.email]
        # total_emails = email_list_gp_1 + email_list_gp_2
        # not_dublicated = set(total_emails)
        # ready_to_send = list(not_dublicated)
        # print(ready_to_send)
        # return ",".join(ready_to_send)
        email_list = []
        if self.pre_sales.partner_id.email:
            email_list = [
                self.pre_sales.partner_id.email]
        print(email_list)
        return email_list

    def get_message_log(self):
        lst_message = []
        for rec in self:
            all_messages = self.env['mail.message'].sudo().search(
                [('res_id', '=', rec.id),
                 ('model', '=', 'crm.lead')])
            for message in all_messages:
                if message.subtype_id.id == 2:
                    lst_message.append({'author': message.author_id.name,
                                        'message_body': message.body,
                                        'created_on': message.date, })
        print(lst_message)
        for sa in lst_message:
            note = self.env['note.note'].sudo().create(
                {
                    'memo': dict(self._fields['message_body'].selection).get(
                        self.message_body),
                }
            )

        return lst_message

    type_new = fields.Selection(string="Opportunity Type", selection=[('full','Full Solution'),
                                                                      ('rfp','RFP'),],
                                default='full', required=True )

    def set_count_opportunity(self):
        for rec in self:
            if rec.count or not rec.count or rec.count_contract or not rec.count_contract:
                attach = self.env['ir.attachment'].search_count(
                    [('res_model', '=', 'crm.lead'),
                     ('res_id', '=', self.id)])
                rec.count=attach
                rec.count_contract=attach

    count = fields.Integer(required=False, compute=set_count_opportunity)
    count_contract = fields.Integer(required=False, compute=set_count_opportunity)




