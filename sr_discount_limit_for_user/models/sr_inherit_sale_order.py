# -*- coding: utf-8 -*-
##############################################################################
#
#    This module uses OpenERP, Open Source Management Solution Framework.
#    Copyright (C) 2017-Today Sitaram
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from odoo import api, models, _
from odoo.exceptions import ValidationError


class srSaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.onchange('discount')
    def check_discount_limit(self):
        if self.env.user.discount_limit != 0.00 and self.discount > self.env.user.discount_limit:
            message = "You can only assign maximum " + str(self.env.user.discount_limit) + "% Discount \nContact your administrator for more details"
            raise ValidationError(_(message))
    
    @api.constrains('discount')
    def check_discount_limit_constrains(self):
        for record in self:
            if self.env.user.discount_limit != 0.00 and record.discount > self.env.user.discount_limit:
                message = "You can only assign maximum " + str(self.env.user.discount_limit) + "% Discount \nContact your administrator for more details"
                raise ValidationError(_(message))

