from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CoverLetter(models.Model):
    _name = 'cover.letter'
    _rec_name = "name"

    total = fields.Char(string="total",  related="opportunity_id.text_amount_mony" ,store=True)
    # total = fields.Char(string="total",  compute="set_default_value" )
    name = fields.Char(string="Code", required=False, )
    opportunity_id = fields.Many2one(comodel_name="crm.lead", readonly=True)

    date = fields.Date(string="Date", required=False, )
    partner_id = fields.Many2one(comodel_name="res.partner", string="Commission", readonly=True, )


    def set_default_value(self):
        my_record= self.env['fin.pro'].search([('opportunity_id','=',self._context.get('active_id'))])
        # print(my_record.text_amount)
        result=f"""
        <div style="text-align: right;"><br><font style="font-size: 14px;">  
        إشارة إلى المنافسة أعاله، يسرنا أن نتقدم بعرضنا األصلي لعدد اربعة من البنود بمبلغ إجمالي وقدره بالريال السعودي {my_record.text_amount}</font><br><font style="font-size: 14px;">شامل قيمة الضريية المضافة إذا كانت خاضعة للقيمة المضافة</font><font style="font-size: 14px;"><br>
        </font><font style="font-size: 14px;"><br></font><u><font style="font-size: 14px;">كما تجدون برفقه األوراق الرسمية المطلوبة أدناه</font></u><u><font style="font-size: 14px;"> </font></u><u><font style="font-size: 14px;"><br></font>
        </u><u><font style="font-size: 14px;"><br></font></u><font style="font-size: 14px;">شهادة منشأت تفيد أن المنشأة ضمن المنشأت الصغيرة والمتوسطة</font>
        <font style="font-size: 14px;"> </font><font style="font-size: 14px;"><br></font><font style="font-size: 14px;">صورة السجل التجاري صورة من شهادة المؤسسة العامة للتأمينات االجتماعية سارية المفعول</font><font style="font-size: 14px;"><br>
        </font><font style="font-size: 14px;"> صورة من شهادة مكتب العمل تفيد تحقيق نس بة السعودة سارية المفعول </font>
        <font style="font-size: 14px;"><br></font><font style="font-size: 14px;">صورة من شهادة المقاولين السعوديين سارية المفعول</font><font style="font-size: 14px;"><br></font><font style="font-size: 14px;"><br></font>
        <div style="text-align: left;"><font style="font-size: 14px;">,,,,,,,,</font><font style="font-size: 14px;">والسلام عليكم ورحمة وبركاته</font>
        <font style="font-size: 14px;"><br><br></font><font style="font-size: 14px;"> شركة التقنيات العالمية للتجارة والمقاوالات </font>
        <font style="font-size: 14px;"><br></font><br><div style="text-align: left;"><font style="font-size: 14px;">         
        :  التوقيع </font><br></div></div> </div>
        """
        return result

    cover = fields.Html(string="Cover Letter Description",
                        default=set_default_value
                        )

class CRM(models.Model):
    _inherit = 'crm.lead'

    text_amount_mony = fields.Char(string="total", compute="get_total", )

    def get_total(self):
        for rec in self:
            list = self.env['fin.pro'].search([('opportunity_id', '=', rec.id)])
            if list :
                for line in list:
                    if rec.text_amount_mony or not rec.text_amount_mony:
                        rec.text_amount_mony=line.text_amount
            else:
                rec.text_amount_mony = ' '

    def open_cover_letter_action(self):
        """ button to run action """
        self.ensure_one()
        action = \
            self.env.ref('report_cover_letter.cover_letter_action').read()[
                0]
        action['domain'] = [('opportunity_id', '=', self.id)]
        action['context'] = {
            'default_opportunity_id': self.id or False,
            'default_partner_id':
                self.partner_id and
                self.partner_id.id or
                False,
        }
        action['views'] = [(
            self.env.ref('report_cover_letter.cover_letter_view_form').id,
            'form'
        )]
        return action

    def action_all_cover_letter(self):
        self.ensure_one()
        return {
            'name': 'Cover letter',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'cover.letter',
            'domain': [('opportunity_id', '=',
                        self.id)],
        }

    def count_cover_letter(self):
        for rec in self:
            if rec.cover_count or not rec.cover_count:
                rec.cover_count=self.env['cover.letter'].search_count([('opportunity_id','=',rec.id)])
        return

    cover_count = fields.Integer(string="", compute=count_cover_letter, )
