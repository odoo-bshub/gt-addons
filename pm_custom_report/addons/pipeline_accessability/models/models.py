from odoo import api, fields, models

class CRM_PIPELINE(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        user = self.env.user
        if not user.has_group('sales_team.group_sale_manager'):
            domain.append('|')
            domain.append('|')
            domain.append('|')
            domain.append('|')
            domain.append(('user_id','=',user.id))
            domain.append(('pre_sales','=',user.id))
            domain.append(('pre_sales_second','=',user.id))
            domain.append(('project_manager_id','=',user.id))
            domain.append(('team_id.user_id','=',user.id))
        return super(CRM_PIPELINE, self).search_read(domain=domain,fields=fields,offset=offset,limit=limit,order=order)

