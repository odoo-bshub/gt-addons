# -*- encoding: utf-8 -*-
{
	"name": "Unupdated Opportunities",
	"version": "14.0",
	"author": "Ahmed Hegazy ,Bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm',
	],
	"category": "",
	"complexity": "",
	"description": """
	    put a flag at un-updated opportunities

	""",
	"data": [
		'security/ir.model.access.csv',
		'views/configuration.xml'
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
