# -*- encoding: utf-8 -*-
{
	"name": "CRM FEEDBACK",
	"version": "14.0",
	"author": "bshub",
	"website": "http://www.bshub.com",
	"sequence": 5,
	"depends": [
		'crm',
	],
	"category": "sales",
	"complexity": "easy",
	"description": """
	This module allow to add Updates on CRN screen. 
	""",
	"data": [
		# 'security/ir.model.access.csv',
		'views/crm.xml',
	],
	"demo": [
	],
	"test": [
	],
	"auto_install": False,
	"installable": True,
	"application": False,
    'images': ['static/description/banner.png'],
	'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
