.. class:: text-left


HelpDesk Type

Usage
=====

* add new fields of opportunity type and tender specification amount

Credits
-------

.. |copy| unicode:: U+000A9 .. COPYRIGHT SIGN
.. |tm| unicode:: U+2122 .. TRADEMARK SIGN

- `Samah Kandil <s.kandil@bshub.com>`_ |copy|
  `Elite <http://www.eliteg.com>`_ |tm| 2020
