# -*- coding: utf-8 -*-
{
    'name': "Purchase Order For Cost sheet",

    'summary': """ Purchase Order For Cost sheet """,

    'description': """ """,

    'author': "s.kandil@bshub.com",
    'website': " ",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['crm','purchase','sale_management','sale'],

    # always loaded
    'data': [
        'security/group.xml',
        'data/pmo_purchase_approve_temp.xml',
        'data/md_purchase_approve_temp.xml',
        'views/views.xml',
        'views/report.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}
