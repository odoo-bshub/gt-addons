from odoo import api, fields, models

class Customer_id_field_res_partner(models.Model):
    _inherit = 'res.partner'

    customer_id_custom_1 = fields.Char(string="Customer ID",readonly=True,related='customer_code')
