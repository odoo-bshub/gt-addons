# pylint: disable=missing-docstring, manifest-required-author
{
    'name': 'Bshub Log Note',
    'summary': 'Log Note ',
    'author': "samah kandil, BsHub",
    'website': "http://www.eltieg.com",
    'version': '14.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'mail',
        'crm',

    ],
    'data': [
        'views/note.xml',
    ],
    'auto_install': False,
}
