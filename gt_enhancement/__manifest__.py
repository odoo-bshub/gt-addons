# -*- coding: utf-8 -*-
{
    'name': "CRM GT Enhancement",

    'summary': """ GT Enhancement""",

    'description': """ """,

    'author': "Etmana",
    'website': " ",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'CRM',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','crm','opportunity_stage_requeriments'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/security.xml',
        'data/pre_sales_mail.xml',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/emails.xml',
    ],
}
