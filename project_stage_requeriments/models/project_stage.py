from odoo import api, fields, models, _


class ProjectStatus(models.Model):
    _inherit = "project.status"

    initiate = fields.Boolean('Initiation phase ',
                              help=_('This stage require Initiation phase .'))

    planning = fields.Boolean('Planning phase',
                              help=_('This stage require Planning phase '))

    execution = fields.Boolean('execution Phase',
                               help=_('This stage requires execution Phase '))

    finance = fields.Boolean('Financing and closing ',
                             help=_('This stage requires Financing and closing'))
