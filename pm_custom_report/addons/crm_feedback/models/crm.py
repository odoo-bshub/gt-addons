from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError, RedirectWarning


class CrmFeedback(models.Model):
    _inherit = 'crm.lead'
